#include <stdio.h>
#include <stdlib.h>

#define LIST_INIT_SIZE 10 //100
#define LISTINCREMENT 10

enum status {FAIL, SUCCESS} ;
//默认的是一元多项式
//目标是多元多项式

//单一项的结构定义
typedef struct LNode 
{
	int coefficient ;//每项的系数
	int index ;//每项的指数
	struct LNode *pNext ;
} LNode, *pLNode ;


//每个项的头结点的线性表
typedef struct Polynomial_Linear_List
{
	int nowPoly ;
	int lenght ; //已建立的多项式个数
	int listsize ;//表的大小
	pLNode *headNodeList ;// 注意 这里存放的是pLode类型 那他指向的是pLnode，他是pLonde的指针
} Poly_Head_List, *pPoly_Head_List ;


void menu(void) ;

////////////////多项式的基本操作
//1 多项式头结点线性表已经在主函数中建立了
//初始化长度为0；

//2 初始化头结点线性表
void initialize_Head_List_P(pPoly_Head_List) ;

//3 显示头结点列表如果满了增加大小
void Show_Polynomial_List_P(pPoly_Head_List) ;

//4 判断头结点列表是否已满
void If_Full_AddMemory_P(pPoly_Head_List) ; 

//5 新建一个多项式头
//结点的coefficient 域储存多项式的大小
//index 域储该多项式编号1为起始号
int Create_Polynomial_P(pPoly_Head_List) ;

//6输入多项式内容
void Input_Polynomial_Interface_P(pPoly_Head_List) ;

//7显示多项式
void Show_Polynomial_P(pPoly_Head_List, int) ;

//8函数7的交互界面
void Show_Polynomial_Interface_P(pPoly_Head_List) ;

//9自定义显示多项式
void Show_Now_Polynomial_Interface_P(pPoly_Head_List) ;

//10 获得头结点1为第一个
pLNode Get_Head_Node_P(pPoly_Head_List, int) ;

//11 化简多项式（合并指数相同的项,并排序）
//   先排序在化简效率会高一些，内嵌排序函数
void To_Easy_Polynomial_P(pPoly_Head_List, int) ;

//12 化简当前多项式的交互界面
void To_Easy_Now_Polynomial_Interface_P(pPoly_Head_List) ;

//13 自定义化简多项式交互界面
void To_Easy_Set_Polynomial_Interface_P(pPoly_Head_List) ;

//14 多项式的顺序排序
void Sequentialv_Sort_Polynomial_P(pPoly_Head_List, int) ;

//15 排序当前多项式
void Sequentialv_Sort_Now_Polynoial_Interface_P(pPoly_Head_List) ;

//16 自定义排序多项式
void Sequentialv_Sort_Set_Polynoial_Interface_P(pPoly_Head_List) ;

//17 删除多项式
//非经典线性表德尔删除方式，因为有多项式编号问题所以我采用
//在线性表中间的删除节点赋值为空
int Delet_Set_Polynoial_P(pPoly_Head_List, int) ;

//18函数17的交互界面
void Delet_Set_Polynoial_Interface_P(pPoly_Head_List) ;


//19检查是否有给定编号的多项式从1开始
int If_Have_Set_Polynoial_P(pPoly_Head_List, int) ;

//20 选定当前多项式
void Set_Now_Polynoial_P(pPoly_Head_List, int) ;

//21 函数20的交互界面
void Set_Now_Polynoial_Interface_P(pPoly_Head_List) ;

//22 多项式得到相加
//基于两个多项式的相加结果为新的一个多项式
//不开辟新多项式，相加结果覆盖到第一个多项式
void Add_Polynoial_P(pPoly_Head_List, int, int) ;

//23 函数22的交互界面
void Add_Polynoial_Interface_P(pPoly_Head_List) ;

//24 找出一个多项式的尾节点给出头结点
pLNode Find_End_Node_Polynoial_P(pLNode) ;

//25 多项式相减
////基于两个多项式的相减结果为新的一个多项式
//不开辟新多项式，相减结果覆盖到第一个多项式
void Sub_Polynoial_P(pPoly_Head_List, int, int) ;

//26函数25的交互界面
void Sub_Polynoial_Interface_P(pPoly_Head_List) ;

//27 多项式相乘的原子操作
//输入乘数的一个节点的前一个的地址 将他-》next与被乘多项式的每项分别相乘
//链在一起
void Mul_polynoial_Atom_Operation_P(pPoly_Head_List, int, int) ;

//28 函数27的交互界面
void Mul_Polynoial_Interface_P(pPoly_Head_List) ;





int main()
{
	int select ;
	Poly_Head_List headList ;//建立多项式头结点线性表
	initialize_Head_List_P(&headList) ;


	while(1)
	{
		fflush(stdin) ;
		menu() ;
		scanf("%d", &select) ;

		switch(select)
		{
		case 0:
			return 0 ;

		case 1:
			Input_Polynomial_Interface_P(&headList) ;
			break ;

		case 2:
			Show_Now_Polynomial_Interface_P(&headList) ;
			break ;

		case 3:
			Show_Polynomial_List_P(&headList) ;
			break ;

		case 4:
			Show_Polynomial_Interface_P(&headList) ;
			break ;

		case 5:
			Sequentialv_Sort_Now_Polynoial_Interface_P(&headList) ;
			break ;

		case 6:
			Sequentialv_Sort_Set_Polynoial_Interface_P(&headList) ;
			break ;

		case 7:
			To_Easy_Now_Polynomial_Interface_P(&headList) ;
			break ;

		case 8:
			To_Easy_Set_Polynomial_Interface_P(&headList) ;
			break ;

		case 9:
			Delet_Set_Polynoial_Interface_P(&headList) ;
			break ;

		case 10:
			Set_Now_Polynoial_Interface_P(&headList) ;
			break ;

		case 11:
			Add_Polynoial_Interface_P(&headList) ;
			break ;

		case 12:
			Sub_Polynoial_Interface_P(&headList) ;
			break ;

		case 13:
			Mul_Polynoial_Interface_P(&headList) ;
			break ;

		default:
			printf("\n无效输入，请从新输入\n") ;
			fflush(stdin) ;
			break ;
		}
	}
}

void menu(void)
{
	printf("\n***************************************************\n") ;
	printf(  "* 1 新建一多项式             2 显示当前多项式     *\n") ;
	printf(  "* 3 显示所有多项式           4 自定义显示多项式   *\n") ;
	printf(  "* 5 排序当前多项式           6 自定义排序多项式   *\n") ;
	printf(  "* 7 化简当前多项式           8 自定义化简多项式   *\n") ;
	printf(  "* 9 自定义删除多项式        10 自定义当前多项式   *\n") ;
	printf(  "*11 自定义多项式相加        12 自定义多项式想减   *\n") ;
	printf(  "*13 自定义多项式相乘                              *\n") ;
	printf(  "*                    0 退出                       *\n") ;
	printf(  "***************************************************\n") ;
	printf("请选择：") ;
}


void initialize_Head_List_P(pPoly_Head_List headList) 
{
	headList->nowPoly = 0 ;
	headList->lenght = 0 ;
	headList->listsize = LIST_INIT_SIZE ;
	if( (headList->headNodeList = (pLNode*)malloc(LIST_INIT_SIZE * sizeof(pLNode))) == (pLNode*)NULL)
	{
		printf("\n内存分配失败！") ;
		exit (1) ;
	}
}


void Show_Polynomial_List_P(pPoly_Head_List headList)
{
	int index ;
	int lenght = headList->lenght ;
	printf("共建立多项式的个数为： %d\n", headList->lenght) ;
	printf("头结点线性表的大小为： %d\n", headList->listsize) ;

	if(headList->lenght == 0)
	{
		;
	}
	else
	{
		for(index = 0; index < lenght; index++)
		{	
			if(Get_Head_Node_P(headList, index+1) == (pLNode)NULL)
			{
				lenght++ ;
			}
			else
			{
				Show_Polynomial_P(headList, index+1) ;
			}
		}
	}
} 


void If_Full_AddMemory_P(pPoly_Head_List headList) 
{
	pLNode* newMemory ;
	int i ;

	if(headList->lenght == headList->listsize)
	{
		if( (newMemory = (pLNode*)malloc((headList->listsize + LISTINCREMENT) * sizeof(pLNode))) == (pLNode*)NULL)
		{
			printf("\n内存分配失败！") ;
			exit (1) ;
		}
		headList->listsize = headList->listsize + LISTINCREMENT ;
			
		for(i = 0; i < headList->lenght; i++)
		{
			newMemory[i] = headList->headNodeList[i] ;
		}
		free(headList->headNodeList) ;
		headList->headNodeList = newMemory ;		
	}
}



int Create_Polynomial_P(pPoly_Head_List headList) 
{
	int i ;
	int index = -1;//标记多项式头表里空的表头的index
	pLNode headPoly ;
	
	for(i = 0; i < headList->lenght; i++)
	{
		if((headPoly = Get_Head_Node_P(headList, i+1)) == (pLNode) NULL)
		{
			index = i ;
			break ;
		}
	}
	
	if((headPoly = (pLNode)malloc(sizeof(LNode))) == (pLNode)NULL)
	{
			printf("\n内存分配失败！") ;
			exit (1) ;
	}

	if(index == -1)
	{
		headList->nowPoly = headList->lenght+1 ;//将现在标记指向新建的多项式
		If_Full_AddMemory_P(headList) ;
		headList->lenght++ ;

		i = headList->lenght ;	
		headList->headNodeList[i-1] = headPoly ;//
		headPoly->coefficient = 0 ;
		headPoly->index = headList->lenght ;
		return (headList->lenght) ;
	}
	else
	{
		headList->lenght++ ;
		headList->headNodeList[index] = headPoly ;
		headPoly->coefficient = 0 ;
		headPoly->index = index+1 ;
		return index+1 ;
	}
}


void Input_Polynomial_Interface_P(pPoly_Head_List headList) 
{
	int coefficient ;//每项的系数
	int index ; 
	int polyNum ;
	pLNode pPre ;
	pLNode nowNode ;
	pLNode headNode ;

	polyNum = Create_Polynomial_P(headList) ;

	headNode = pPre = headList->headNodeList[polyNum-1] ;
	printf("\n该多项式编号为：%d\n", polyNum) ; 


	printf("请输入每项的系数和指数以CTRL+E结束。 \n") ;

	while(scanf("%d %d", &coefficient, &index))
	{
		if( ( nowNode = (pLNode)malloc(sizeof(LNode))) == (pLNode)NULL)
		{
			printf("\n内存分配失败！") ;
			exit (1) ;
		}
		nowNode->coefficient = coefficient ;
		nowNode->index = index ;
		pPre->pNext = nowNode ;
		pPre = nowNode ;
		headNode->coefficient++ ;
	}
	nowNode->pNext = (pLNode)NULL ;//最后一个的指针域为空
	fflush(stdin) ;
}


void Show_Polynomial_P(pPoly_Head_List headList, int index)
{
	pLNode nowNode ;

	if(index < 1)
	{
		printf("\n没有此多项式\n") ;
	}
	else
	{

		nowNode = Get_Head_Node_P(headList, index) ;
		if(nowNode == (pLNode)NULL)
		{
			printf("\n没有此多项式\n") ;
		}
		//nowNode = headList->headNodeList[index-1] ; //找到头结点
		else
		{
			if(nowNode->coefficient == 0)
			{
				printf("\n多项式无数据\n") ;
			}
			else
			{
				printf("\n多项式编号：%d\n", nowNode->index) ;
				printf("项数为：%d\n", nowNode->coefficient) ;
				nowNode = nowNode->pNext ;
				while(nowNode != (pLNode)NULL)
				{
					printf("%dX%d + ", nowNode->coefficient, nowNode->index) ;
					nowNode = nowNode->pNext ;
				}
			}
		}
	}
}


void Show_Polynomial_Interface_P(pPoly_Head_List headList) 
{
	int index ;

	printf("请输入要显示的多项式编号: ") ;
	scanf("%d" ,&index) ;
	
	if(index < 1 || index > headList->lenght)
	{
		printf("\n没有此多项式\n") ;
	}
	else
	{
		Show_Polynomial_P(headList, index) ;
	}
}

void Show_Now_Polynomial_Interface_P(pPoly_Head_List headList) 
{
	int index = headList->nowPoly ;

	if(index == 0)
	{
		printf("\n无选定多项式或没有多项式\n") ;
	}
	else
	{
		Show_Polynomial_P(headList, index) ;
	}
}


void Sequentialv_Sort_Polynomial_P(pPoly_Head_List headList, int index) 
{	
	int nowNodeNextIndex ;
	int nowNodeNextCoeff ;
	int setNodeNextIndex ;
	int setNodeNextCoeff ;
	int swap ;
	pLNode swapNode ;
	pLNode nowNode ; //被比较的项的前一项！
	pLNode headNode = Get_Head_Node_P(headList, index) ;//初始为头结点比较的前一项
	pLNode setNode = headNode ;
	
	if(index > headList->lenght || index < 1)
	{
		printf("\n无此头结点\n") ;
	}
	else
	{
		while(setNode->pNext != (pLNode)NULL)
			{
				swapNode = setNode->pNext ;
				swap = swapNode->index ;
				setNodeNextIndex = setNode->pNext->index ;
				setNodeNextCoeff = setNode->pNext->coefficient ;
				nowNode = setNode->pNext ;

				while(nowNode != (pLNode)NULL)
					{
						if(nowNode->pNext == (pLNode)NULL)
						{
							break ;
						}
						nowNodeNextIndex = nowNode->pNext->index ;
						nowNodeNextCoeff = nowNode->pNext->coefficient ;

						if(swap > nowNodeNextIndex)
						{
							swapNode = nowNode->pNext ;
							swap = swapNode->index ;
						}
						nowNode = nowNode->pNext ;
					}
					//跳出内循环
					if(setNodeNextIndex > swap)
					{
						swap = setNode->pNext->index ;
						setNode->pNext->index = swapNode->index ;
						swapNode->index = swap ;
						swap = setNode->pNext->coefficient ;
						setNode->pNext->coefficient = swapNode->coefficient ;
						swapNode->coefficient = swap ;
					}
					setNode = setNode->pNext ;
				}
	}
}



pLNode Get_Head_Node_P(pPoly_Head_List headList, int index) 
{
	if(index < 1)
	{
		printf("\n无此头结点\n") ;
		return (pLNode)NULL ;
	}
	else
	{
		return headList->headNodeList[index-1] ;
	}
}

void Sequentialv_Sort_Now_Polynoial_Interface_P(pPoly_Head_List headList) 
{
	int index = headList->nowPoly ;

	if(index == 0)
	{
		printf("\n无多项\n") ;
	}
	else
	{
		Sequentialv_Sort_Polynomial_P(headList, index) ;
		printf("\n排序完成\n") ;
	}
}


void Sequentialv_Sort_Set_Polynoial_Interface_P(pPoly_Head_List headList)
{
	int index ;

	printf("\n请输入排序多项式编号： ") ;
	scanf("%d", &index) ;
	if(index <1 || index > headList->lenght)
	{
		printf("\n无多项\n") ;
	}
	else
	{
		Sequentialv_Sort_Polynomial_P(headList, index) ;
		printf("\n排序完成\n") ;
	}
}

//这个是基于排序的化简，效率高 大O为lenght的线性
void To_Easy_Polynomial_P(pPoly_Head_List headList, int index) 
{
	int nowNodeIndex ;
	int nowNodeCoeff ;
	int setNodeIndex ;
	int setNodeCoeff ;
	pLNode setHeadNode = Get_Head_Node_P(headList, index) ;
	pLNode setNode  ; 
	pLNode nowNode  ;
	pLNode setNodeFront = setHeadNode ;
	pLNode nowNodeFront  ;


	if(index < 1 || index > headList->lenght)
	{
			printf("\n无多项\n") ;
	}
	else
	{
		Sequentialv_Sort_Polynomial_P(headList, index) ;//先排序
		while(setNodeFront->pNext != (pLNode)NULL)
		{	
			setNode = setNodeFront->pNext ;
			setNodeIndex = setNode->index ;
			setNodeCoeff = setNode->coefficient ;
			nowNodeFront = setNodeFront->pNext ;

			if(nowNodeFront->pNext == (pLNode)NULL)
				break ;
			nowNode = nowNodeFront->pNext ;
			nowNodeIndex = nowNode->index ;
			nowNodeCoeff = nowNode->coefficient ;
			

			while(setNodeIndex == nowNodeIndex)
			{
				setNode->coefficient += nowNode->coefficient ;
				setNode->pNext = nowNode->pNext ;
				free(nowNode) ;
				setHeadNode->coefficient-- ;//多项式长度减一


				nowNode = nowNodeFront->pNext ;//向下移动一个在和now比较
				nowNodeIndex = nowNode->index ;
				nowNodeCoeff = nowNode->coefficient ;

			}
			if(setNodeFront->pNext->coefficient == 0)
			{
				setNodeFront->pNext = setNode->pNext ;
				free(setNode) ;
				setHeadNode->coefficient-- ;//多项式长度减一
			}
			else
			{
				setNodeFront = nowNodeFront ;
			}
			
		}
	}

}

void To_Easy_Now_Polynomial_Interface_P(pPoly_Head_List headList) 
{
	int index = headList->nowPoly ;
	
	if(index < 1 || index > headList->lenght)
	{
			printf("\n无多项\n") ;
	}
	else
	{
		To_Easy_Polynomial_P(headList, index) ;
	}
}

void To_Easy_Set_Polynomial_Interface_P(pPoly_Head_List headList) 
{
	int index ;

	printf("\n请输入化简多项式的编号：\n") ;
	scanf("%d", &index) ;

	
	if(index < 1 || index > headList->lenght)
	{
			printf("\n无多项式\n") ;
	}
	else
	{
		To_Easy_Polynomial_P(headList, index) ;
	}
}

int If_Have_Set_Polynoial_P(pPoly_Head_List headList, int index)
{
	int i ;
	int lenght = headList->lenght;
	if(index < 1)
	{
		return FAIL ;
	}
	else
	{
		for(i = 1; i <= lenght; i++)
		{	
			if(Get_Head_Node_P(headList, i) == (pLNode)NULL)
			{
				lenght++ ;
			}
			if(index == i)
				return SUCCESS ;
		}
		return FAIL ;
	}
} 

int Delet_Set_Polynoial_P(pPoly_Head_List headList, int index) 
{
	if(index < 1 )
	{
		printf("\n无此多项式，无法删除\n") ;
		return FAIL ;
	}
	else
	{
		if(If_Have_Set_Polynoial_P(headList, index) == FAIL)
		{
			printf("\n无此多项式，无法删除\n") ;
			return FAIL ;
		}
		else
		{
			headList->headNodeList[index-1] = (pLNode)NULL ;
			headList->lenght-- ;
			printf("成功删除！\n") ;
			return SUCCESS ;
		}
	}
}


void Delet_Set_Polynoial_Interface_P(pPoly_Head_List headList) 
{
	int index ;

	printf("\n请输入删除多项式的编号：") ;
	scanf("%d", &index) ;

	if(If_Have_Set_Polynoial_P(headList, index) == FAIL)
	{
		printf("\n无此多项式\n") ;
	}
	else
	{
		Delet_Set_Polynoial_P(headList, index) ;
	}
}

void Set_Now_Polynoial_P(pPoly_Head_List headList, int index)
{
	if(index < 1 || If_Have_Set_Polynoial_P(headList, index) == FAIL)
	{
		printf("没有此多项式!\n") ;
	}
	else
		headList->nowPoly = index ;
} 

void Set_Now_Polynoial_Interface_P(pPoly_Head_List headList)
{
	int index ;

	printf("请输出多项式编号： ") ;
	scanf("%d", &index) ;

	Set_Now_Polynoial_P(headList, index) ;
} 

pLNode Find_End_Node_Polynoial_P(pLNode headNode) 
{
	pLNode nowNode = headNode ;

	if(nowNode == (pLNode)NULL)
		return (pLNode)NULL ;
	else
	{
		while(nowNode->pNext != (pLNode)NULL)
		{
			nowNode = nowNode->pNext ;
		}
		return nowNode ;
	}
}


void Add_Polynoial_P(pPoly_Head_List headList, int fIndex, int sIndex) 
{
	pLNode fEndNode ;
	pLNode fHeadNode ; 
	pLNode sNowNode ;
	pLNode addNode ;

	if( If_Have_Set_Polynoial_P(headList, fIndex) != SUCCESS ||
		If_Have_Set_Polynoial_P(headList, sIndex) != SUCCESS)
	{
		printf("\n相加多项式不存在！\n") ;
	}
	else
	{
		//初始化节点
		fHeadNode = Get_Head_Node_P(headList, fIndex) ;
		fEndNode = Find_End_Node_Polynoial_P(headList->headNodeList[fIndex-1]) ;
		sNowNode = headList->headNodeList[sIndex-1]->pNext ;
		
		while(sNowNode != (pLNode)NULL)
		{

			if( ( addNode = (pLNode)malloc(sizeof(LNode))) == (pLNode)NULL)
			{
			printf("\n内存分配失败！") ;
			exit (1) ;
			}
			addNode->coefficient = sNowNode->coefficient ;
			addNode->index = sNowNode->index ;
			fEndNode->pNext = addNode ;//链上新节点
			fHeadNode->coefficient++ ;

			fEndNode = addNode ;
			sNowNode = sNowNode->pNext ;
		}
		fEndNode->pNext = (pLNode)NULL ;//连接没问题

		//连上后化简，相当于相加
		To_Easy_Polynomial_P(headList, fIndex) ;
	}

}

void Add_Polynoial_Interface_P(pPoly_Head_List headList) 
{
	int fIndex = -1, sIndex = -1 ;

	printf("\n请输入相加的多项式编号：") ;
	scanf("%d", &fIndex) ;

	while(scanf("%d", &sIndex))
	{
		
		Add_Polynoial_P(headList, fIndex, sIndex) ;
	}
}

void Sub_Polynoial_P(pPoly_Head_List headList, int fIndex, int sIndex) 
{
	pLNode fEndNode ;
	pLNode fHeadNode ; 
	pLNode sNowNode ;
	pLNode addNode ;

	if( If_Have_Set_Polynoial_P(headList, fIndex) != SUCCESS ||
		If_Have_Set_Polynoial_P(headList, sIndex) != SUCCESS)
	{
		printf("\n相加多项式不存在！\n") ;
	}
	else
	{
		//初始化节点
		fHeadNode = Get_Head_Node_P(headList, fIndex) ;
		fEndNode = Find_End_Node_Polynoial_P(headList->headNodeList[fIndex-1]) ;
		sNowNode = headList->headNodeList[sIndex-1]->pNext ;
		
		while(sNowNode != (pLNode)NULL)
		{

			if( ( addNode = (pLNode)malloc(sizeof(LNode))) == (pLNode)NULL)
			{
			printf("\n内存分配失败！") ;
			exit (1) ;
			}
			addNode->coefficient = -(sNowNode->coefficient) ;//转成负数
			addNode->index = sNowNode->index ;
			fEndNode->pNext = addNode ;//链上新节点
			fHeadNode->coefficient++ ;

			fEndNode = addNode ;
			sNowNode = sNowNode->pNext ;
		}
		fEndNode->pNext = (pLNode)NULL ;//连接没问题

		//连上后化简，相当于相减
		To_Easy_Polynomial_P(headList, fIndex) ;
	}
}


void Sub_Polynoial_Interface_P(pPoly_Head_List headList) 
{
	int fIndex = -1, sIndex = -1 ;

	printf("\n请输入相减的多项式编号：") ;
	scanf("%d", &fIndex) ;

	while(scanf("%d", &sIndex))
	{	
		Sub_Polynoial_P(headList, fIndex, sIndex) ;
	}
}


void Mul_Polynoial_Atom_Operation_P(pPoly_Head_List headList, int fIndex, int sIndex)
{
	pLNode fHeadNode ;
	pLNode sHeadNode ;
	pLNode fNowNode ; //指向nowNode
	pLNode sNowNode ;
	pLNode nextNode ; //乘完后链接的地址
	pLNode newNode ;
	int fNodeCoeff ;
	int fNodeIndex ;


	if( If_Have_Set_Polynoial_P(headList, fIndex) == FAIL || 
		If_Have_Set_Polynoial_P(headList, sIndex )== FAIL )
	{
		printf("\n无效多项式！\n") ;
	}
	else
	{
		fHeadNode = Get_Head_Node_P(headList, fIndex) ;
		sHeadNode = Get_Head_Node_P(headList, sIndex) ;
		fNowNode = fHeadNode ;
	
		while(fNowNode->pNext != (pLNode)NULL)
		{
			sNowNode = sHeadNode->pNext ;//首项
			nextNode = fNowNode->pNext->pNext ;
			fNodeCoeff = fNowNode->pNext->coefficient ;
			fNodeIndex = fNowNode->pNext->index ;

			while(sNowNode != (pLNode)NULL )
			{
				
				if((newNode = (pLNode)malloc(sizeof(LNode))) == (pLNode)NULL)
				{
					printf("\n内存分配失败！") ;
					exit (1) ;
				}
				newNode->coefficient = fNodeCoeff * sNowNode->coefficient ;
				newNode->index = fNodeIndex + sNowNode->index ;
				fNowNode->pNext = newNode ;
				fHeadNode->coefficient++ ;
				fNowNode = newNode ;
				sNowNode = sNowNode->pNext ;
			}

			fNowNode->pNext = nextNode ;//和原来的节点链上
			//fNowNode = newNode ;
		}
		//化简结果
		To_Easy_Polynomial_P(headList, fIndex) ;
	}
}

void Mul_Polynoial_Interface_P(pPoly_Head_List headList)
{
	int fIndex = -1, sIndex = -1 ;

	printf("\n请输入相乘的多项式编号：") ;
	scanf("%d", &fIndex) ;

	while(scanf("%d", &sIndex))
	{	
		Mul_Polynoial_Atom_Operation_P(headList, fIndex, sIndex) ;
	}
}
 
/*

//不是基于排序的化简函数
void Sequentialv_Sort_Polynomial_P(pPoly_Head_List headList, int index) 
{	
	int nowNodeNextIndex ;
	int nowNodeNextCoeff ;
	int setNodeNextIndex ;
	int setNodeNextCoeff ;
	pLNode delNode ;
	pLNode nowNode ; //被比较的项的前一项！
	pLNode headNode = Get_Head_Node_P(headList, index) ;//初始为头结点比较的前一项
	pLNode setNode = headNode ;
	
	if(index > headList->lenght || index < 1)
	{
		printf("\n无此头结点\n") ;
	}
	else
	{
		while(setNode->pNext != (pLNode)NULL)
			{
				setNodeNextIndex = setNode->pNext->index ;
				setNodeNextCoeff = setNode->pNext->coefficient ;
				nowNode = setNode->pNext ;

				while(nowNode != (pLNode)NULL)
					{
						if(nowNode->pNext == (pLNode)NULL)
						{
							break ;
						}
						nowNodeNextIndex = nowNode->pNext->index ;
						nowNodeNextCoeff = nowNode->pNext->coefficient ;

						if(setNodeNextIndex == nowNodeNextIndex)
						{
							setNode->pNext->coefficient += nowNodeNextCoeff ;
							//删除这个指数一样的项
							delNode = nowNode->pNext ;
							if(delNode->pNext == (pLNode)NULL)
							{
								nowNode->pNext = (pLNode)NULL ;
							}
							else
							{
								nowNode->pNext = delNode->pNext ;//
							}
							free(delNode) ;
							headNode->coefficient -- ;
							//出现系数相加为零的情况
							if(setNode->pNext->coefficient == (int)0)
							{
								delNode = setNode->pNext ;
								setNode->pNext = delNode->pNext ;
								free(delNode) ;
								headNode->coefficient-- ;
							}
						}
						nowNode = nowNode->pNext ;
					}
					//跳出内循环
					setNode = setNode->pNext ;
				}
	}
}
  */