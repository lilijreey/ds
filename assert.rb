
def Assert(exp)
    if (!exp)
        raise "Assert failed"
    end
end

def AssertEq(a,b)
    if (a != b)
        raise "Assert failed: a:#{a} != b:#{b}\n"
    end
end

def AssertNot(exp)
    if (exp)
    raise "AssertNot failed line"
    end
end
