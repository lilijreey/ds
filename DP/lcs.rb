#!/usr/bin/env ruby

# 最长递增子序列
# 最长公共子序列
# 最长公共递增子序列

$cache = {}
$cache_str = {}

def lis(ary)
  # 设d[i] 以第i个元素结尾的最长公共子串长度
  # d[i+1] = d[i] + 1 if ary[i+1] > ary[i]
  #          d[i] if ary[i+1] == ary[i]

  return [] if ary.empty?

  m = [-1] * ary.length
  d = m.dup
  m[0] = ary[0]
  d[0] = 1
  
  i = 1
  while i < ary.length
    if ary[i+1] > m[i]
      d[i+1] = 1 + d[i]
      m[i+1] = ary[i+1]
    else
      d[i+1] = d[i]
      m[i+1] = m[i]
    end
    i+=1
  end

  m.max
end
def lcs(str1, str2)
  ## 推倒
  #设lcs(l.n,r.n) 为l,r 的最长公共子序列,全部情况
  #lcs(l.n+1, r.n+1) = 1 + lcs(l.n, r.n)  如果 l.n+1 == r.n+1
  # 如果不等
  #                  = max(lcs(l.n, r.n+1), lcs(l.n+1, r.n))
  
  ret = []
  return ret if str1.empty? or str2.empty?
  #puts "lcs"
  #p str1
  #p str2
  if $cache[[str1.length, str2.length]] 
    return $cache_str[[str1.length, str2.length]]
  end

  $cache[[str1.length, str2.length]] =
  if str1[-1] == str2[-1]
    ret << [str1.length-1, str2.length-1]
    n = lcs(str1[0..-2], str2[0..-2])
    ret += n
    1 + n.length
  else
    n1 = lcs(str1, str2[0..-2])
    n2 = lcs(str1[0..-2],str2)
    if n1.length > n2.length
      ret += n1
      n1.length
    elsif n1.length == n2.length
      # 多种情况
      if n1 == n2
        ret += n1
      else
        ret += [:tt,n1,n2]
      end
      n1.length
    else
      ret += n2
      n2.length
    end
  end

  $cache_str[[str1.length, str2.length]] = ret
  ret

end


def expansion(str)
  ss = [[]]
  i = 0
  while i < ret.length
    if ret[i] == :tt
      last1 = expansion(ret[i+1])
      last2 = expansion(ret[i+2])


    end
  end
end

def lcs_all(str1, str2)
  ret = lcs(str1, str2)

  ss = []
  i = 0
  while i < ret.length
    if ret[i] == :tt
      ss = ss * 2
      ss.map! {|l|
       ll + xx(ret[i+1])
      }
    end
  end
end

def max_envelopes(envelopes)
  envelopes.uniq!
  $cache.clear
  $cache_str.clear
      # sort + LCS

  ss = []
  envelopes.each_with_index {|e, i|
    ss << [e[0], e[1], i]
  }

  wfirst = ss.sort {|l, r|
    n = l[0] <=> r[0]
    if n == 0
      l[1] <=> r[1]
    else
      n
    end

  }.map {|(e,_,i)| i}

  hfirst = ss.sort {|l, r|
    n = l[1] <=> r[1]
    if n == 0
      l[0] <=> r[0]
    else
      n
    end
  }.map {|(_,e,i)| i}

  l = lcs(wfirst, hfirst)
  pp wfirst
  pp hfirst
  pp l
  seq = 
    l.reverse.map {|(i,j)|
      envelopes[wfirst[i]]
  }
  pp seq

  ## delete w == w, h ==h
  maxh = 0
  maxw = 0
  n = 0
  #pp seq
  seq.each {|(w,h)|
    if w > maxw and h > maxh
      maxw = w
      maxh = h
      #pp w,h
      n+=1
    end
  }

  n

end

def test()
  str1 = "ABCBDAB"
  str2 = "BDCABA"
  ret = ""
end
