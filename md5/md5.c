#if 0

md5 算法
=====================

原理
================
1. 输入数据以512bit/64B 来分组, 每个组内又分为16个32bit/4B
2. 填充数据时期字节长度使得填充后的长度为 512b * N + 448b (N>=0)
  2.1 填充规则
     * 一个1,其余都是0,
     * 不管原始数据是否已经对齐448,都会进行填充, 填充至少1个bit
       最多512个
3. 填充长度
     把原始数据的bit数以64位大小补齐到最后, 使得整个数据是512b的整备数
     数字为小端序

4. 初始化四个32bit的buffer. 
    A=0x01234567，B=0x89ABCDEF，C=0xFEDCBA98，D=0x76543210
5. 对每512(64B)数据块,在划分为16个段,每段4B
   对每个段进行如下四种计算
   16次 FF(a,b,c,d,mj,s,ti)
   16次 GG(a,b,c,d,mj,s,ti)
   16次 HH(a,b,c,d,mj,s,ti)
   16次 II(a,b,c,d,mj,s,ti)

   得到新的a,b,c,d
   加上之前的a,b,c,d,作为下一轮的a,b,c,d

5. 按照地址从低到高打印,a,b,c,d的值就是md5
    

2. 输出128bit

## 实现参考
https://openwall.info/wiki/people/solar/software/public-domain-source-code/md5
https://tools.ietf.org/html/rfc1321

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

//初始化种子
uint32_t seed[] = { 
    0x67452301,
    0xefcdab89,
    0x98badcfe,
    0x10325476,
};

static int bc;


//4个基础运算
#define F(X, Y, Z) ((X & Y) | (~X & Z))
#define G(X, Y, Z) ((X & Z) | (Y & ~Z))
#define H(X, Y, Z) (X ^ Y ^ Z)
#define I(X, Y, Z) (Y ^ (X | ~Z))

//T 表
static const unsigned int T[] = {
#if 0
生成规则如下
(1 .. 64).map do 
 ((2**32) * Math.sin(i).abs).floor
end
#endif
   0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee, //0
   0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501, //4
   0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be, //8
   0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821, //12
   0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa, //16
   0xd62f105d,  0x2441453, 0xd8a1e681, 0xe7d3fbc8, //20
   0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed, //24
   0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a, //28
   0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c, //32
   0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70, //36
   0x289b7ec6, 0xeaa127fa, 0xd4ef3085,  0x4881d05, //40
   0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665, //44
   0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039, //48
   0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1, //52
   0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1, //56
   0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391  //60
};


//循环左移表
static const int SHIFT_S[64] = {
#if 0
    //可以通过一个更小的表生成
    S_B= [7, 12, 17, 22, 5, 9, 14, 20, 4, 11, 16, 23, 6, 10, 15, 21]                                                                                  
    n = (0..63).map do |i|                                                                                                                            
       S_B[(i >> 4) << 2 | (i&3)]                                                                                                                      
     end  
#endif
    7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
    5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20, 5,  9, 14, 20,
    4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
    6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21
};


//512 block, 16个分组,选择子
static const int K_T[64] =
{
#if 0
生成算法
int K(int n)
{
    int r = 0;
    if (n <= 15) r = n;
    else if (n <= 31) r = 5 * n + 1;
    else if (n <= 47) r = 3 * n + 5;
    else r = 7 * n;
    return r % 16;
}
#endif
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 
	1, 6, 11, 0, 5, 10, 15, 4, 9, 14, 3, 8, 13, 2, 7, 12, 
	5, 8, 11, 14, 1, 4, 7, 10, 13, 0, 3, 6, 9, 12, 15, 2, 
	0, 7, 14, 5, 12, 3, 10, 1, 8, 15, 6, 13, 4, 11, 2, 9,
};

//循环左移
#define left_shift_cycl(x, c) ((x<<c) | (x >> (32-c)))

//在原始算法上,展开了计算过程速度更快,但是代码更多,
//下面使用一种速度较慢但是代码更少的方式
//https://rosettacode.org/wiki/MD5/Implementation#Go
void calc512(uint32_t abcd[4], char *block)
{
#if 0
    计算公式 a = b + ((a+F(b,c,d) + X[k] + T[i]) << s)
        [a,b,c,d, k, s, i]
        i: 从0-64 
        s: SHIFT_S 中的数字
        k:
#endif

    uint32_t a1=abcd[0],ta;
    uint32_t b1=abcd[1],tb;
    uint32_t c1=abcd[2],tc;
    uint32_t d1=abcd[3],td;
    int k,s,f;
    uint32_t *X = (uint32_t*)block;
    for (int i=0; i<64; ++i) {
        /*printf("byte :%d\n", i);*/
        switch (i >> 4 ) {
        case 0: {               
            f = F(b1,c1,d1);    
            k = i;              
            break;              
        }                       
        case 1: {               
            f = G(b1,c1,d1);    
            k = (i*5 + 1) & 0xF;
            break;              
        }                       
        case 2: {               
            f = H(b1,c1,d1);    
            k = (i*3 + 5) & 0xF;
            break;              
        }                       
        case 3: {               
            f = I(b1,c1,d1);    
            k = (i*7 ) & 0xF;   
        }                       
        }//end switch
        /*assert(k == K_T[i]);*/
        //计算公式 a = b + ((a+F(b,c,d) + X[k] + T[i]) << s)
        
        /*printf("f1:%x\n",f);*/
        f += X[k];
        /*printf("+X f1:%x\n",f);*/
        f += T[i];
        /*printf("+T f1:%x\n",f);*/
        a1 += f ;
        a1 = left_shift_cycl(a1, SHIFT_S[i]);
        a1 += b1;
        ta = a1;
        tb = b1;
        tc = c1;
        td = d1;

        /*printf("f:%x k:%x X:%x T:%x, s:%x\n", */
        /*       f, k, X[k], T[i], SHIFT_S[i]); */

        /*printf("a:%x b:%x c:%x d:%x\n",       */
        /*       ta,tb,tc,td);                  */

        a1 = td;
        b1 = ta;
        c1 = tb;
        d1 = tc;

    }
    abcd[0] +=a1;
    abcd[1] +=b1;
    abcd[2] +=c1;
    abcd[3] +=d1;
    printf("%i out 0x%x, 0x%x, 0x%x, 0x%x\n", 
           bc++,
           abcd[0],
           abcd[1],
           abcd[2],
           abcd[3]);
}

#if 1
int main(int argc, char *argv[])
{
    if (argc != 2) {
        printf("Usage: %s <file>", argv[0]);
        exit(1);
    }

    const char *fname = argv[1];

    FILE *f = fopen(fname, "rb");
    if (!f) {
        printf("open %s failed", fname);
        exit(1);
    }


    char buf[64];
#define buf_sz (sizeof(buf))
    int n,m;
    int file_sz=0;

    uint32_t abcd[] = {
    0x67452301,
    0xefcdab89,
    0x98badcfe,
    0x10325476,
    };


    while((m = fread(buf, 1, buf_sz, f))) {
        /*printf("read file size:%d\n",m);*/
        file_sz += m;
        if (m != buf_sz) {
            n = m;
            break;
        }
        calc512(abcd, buf);
    }

    /*printf("last len:%d\n",n);*/
    //先填充至少1一个bit,然后在判断
    buf[n++] = 0x80;
    //补齐448 bit
    if (n <= 56) {
        for (int i=n; i <56; ++i) buf[i] = 0;
        *(uint64_t*)(buf+56) = 8 * file_sz;
        printf("file size:%d m:%d fit size:%d\n", file_sz, n, 56-n);
        calc512(abcd, buf);
    } else { //> 56
        for (int i=n; i<buf_sz; ++i) buf[i] = 0;
        calc512(abcd, buf);
        printf("n:%d > 56\n", n);

        for(int i=0; i < 56; ++i) buf[i] =0;
        *(uint64_t*)(buf+56) = 8 * file_sz;
        printf("file size:%d m:%d fit size:%d\n", file_sz, n, 56+64-n);
        calc512(abcd, buf);
    }

    /*printf("file size:%d\n", file_sz);*/

    fclose(f);
    const unsigned char *diget = (char*)abcd;
    for (int i=0; i < 16; ++i)
    {
        /*printf("%2x", 2);*/
        printf("%02x",diget[i]);
    }
    printf("\n");

    ///////////////////////////////////////////
    return 0;
}
#endif
