
main:     file format elf64-x86-64


Disassembly of section .init:

0000000000000628 <_init>:
 628:	48 83 ec 08          	sub    $0x8,%rsp
 62c:	48 8b 05 b5 19 20 00 	mov    0x2019b5(%rip),%rax        # 201fe8 <__gmon_start__>
 633:	48 85 c0             	test   %rax,%rax
 636:	74 02                	je     63a <_init+0x12>
 638:	ff d0                	callq  *%rax
 63a:	48 83 c4 08          	add    $0x8,%rsp
 63e:	c3                   	retq   

Disassembly of section .plt:

0000000000000640 <.plt>:
 640:	ff 35 5a 19 20 00    	pushq  0x20195a(%rip)        # 201fa0 <_GLOBAL_OFFSET_TABLE_+0x8>
 646:	ff 25 5c 19 20 00    	jmpq   *0x20195c(%rip)        # 201fa8 <_GLOBAL_OFFSET_TABLE_+0x10>
 64c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000000650 <strlen@plt>:
 650:	ff 25 5a 19 20 00    	jmpq   *0x20195a(%rip)        # 201fb0 <strlen@GLIBC_2.2.5>
 656:	68 00 00 00 00       	pushq  $0x0
 65b:	e9 e0 ff ff ff       	jmpq   640 <.plt>

0000000000000660 <__stack_chk_fail@plt>:
 660:	ff 25 52 19 20 00    	jmpq   *0x201952(%rip)        # 201fb8 <__stack_chk_fail@GLIBC_2.4>
 666:	68 01 00 00 00       	pushq  $0x1
 66b:	e9 d0 ff ff ff       	jmpq   640 <.plt>

0000000000000670 <__strncpy_chk@plt>:
 670:	ff 25 4a 19 20 00    	jmpq   *0x20194a(%rip)        # 201fc0 <__strncpy_chk@GLIBC_2.3.4>
 676:	68 02 00 00 00       	pushq  $0x2
 67b:	e9 c0 ff ff ff       	jmpq   640 <.plt>

0000000000000680 <__printf_chk@plt>:
 680:	ff 25 42 19 20 00    	jmpq   *0x201942(%rip)        # 201fc8 <__printf_chk@GLIBC_2.3.4>
 686:	68 03 00 00 00       	pushq  $0x3
 68b:	e9 b0 ff ff ff       	jmpq   640 <.plt>

0000000000000690 <__sprintf_chk@plt>:
 690:	ff 25 3a 19 20 00    	jmpq   *0x20193a(%rip)        # 201fd0 <__sprintf_chk@GLIBC_2.3.4>
 696:	68 04 00 00 00       	pushq  $0x4
 69b:	e9 a0 ff ff ff       	jmpq   640 <.plt>

Disassembly of section .plt.got:

00000000000006a0 <__cxa_finalize@plt>:
 6a0:	ff 25 52 19 20 00    	jmpq   *0x201952(%rip)        # 201ff8 <__cxa_finalize@GLIBC_2.2.5>
 6a6:	66 90                	xchg   %ax,%ax

Disassembly of section .text:

00000000000006b0 <main>:
#include <stdio.h>
#include <string.h>
#include "md5calc.h"

int main()
{
     6b0:	53                   	push   %rbx
     6b1:	48 83 ec 70          	sub    $0x70,%rsp
    char input[] = "1234567";
    char md5V[80];

    MD5_String(input, md5V);
     6b5:	48 8d 5c 24 10       	lea    0x10(%rsp),%rbx
     6ba:	48 8d 7c 24 08       	lea    0x8(%rsp),%rdi
{
     6bf:	64 48 8b 04 25 28 00 	mov    %fs:0x28,%rax
     6c6:	00 00 
     6c8:	48 89 44 24 68       	mov    %rax,0x68(%rsp)
     6cd:	31 c0                	xor    %eax,%eax
    MD5_String(input, md5V);
     6cf:	48 89 de             	mov    %rbx,%rsi
    char input[] = "1234567";
     6d2:	48 b8 31 32 33 34 35 	movabs $0x37363534333231,%rax
     6d9:	36 37 00 
     6dc:	48 89 44 24 08       	mov    %rax,0x8(%rsp)
    MD5_String(input, md5V);
     6e1:	e8 ca 0d 00 00       	callq  14b0 <MD5_String>
}

__fortify_function int
printf (const char *__restrict __fmt, ...)
{
  return __printf_chk (__USE_FORTIFY_LEVEL - 1, __fmt, __va_arg_pack ());
     6e6:	48 8d 35 07 0f 00 00 	lea    0xf07(%rip),%rsi        # 15f4 <_IO_stdin_used+0x4>
     6ed:	48 89 da             	mov    %rbx,%rdx
     6f0:	bf 01 00 00 00       	mov    $0x1,%edi
     6f5:	31 c0                	xor    %eax,%eax
     6f7:	e8 84 ff ff ff       	callq  680 <__printf_chk@plt>
    printf("md5:%s\n", md5V);
    printf("md5-length:%lu\n", strlen(md5V));
     6fc:	48 89 da             	mov    %rbx,%rdx
     6ff:	8b 0a                	mov    (%rdx),%ecx
     701:	48 83 c2 04          	add    $0x4,%rdx
     705:	8d 81 ff fe fe fe    	lea    -0x1010101(%rcx),%eax
     70b:	f7 d1                	not    %ecx
     70d:	21 c8                	and    %ecx,%eax
     70f:	25 80 80 80 80       	and    $0x80808080,%eax
     714:	74 e9                	je     6ff <main+0x4f>
     716:	89 c1                	mov    %eax,%ecx
     718:	bf 01 00 00 00       	mov    $0x1,%edi
     71d:	c1 e9 10             	shr    $0x10,%ecx
     720:	a9 80 80 00 00       	test   $0x8080,%eax
     725:	0f 44 c1             	cmove  %ecx,%eax
     728:	48 8d 4a 02          	lea    0x2(%rdx),%rcx
     72c:	89 c6                	mov    %eax,%esi
     72e:	48 0f 44 d1          	cmove  %rcx,%rdx
     732:	40 00 c6             	add    %al,%sil
     735:	48 8d 35 c0 0e 00 00 	lea    0xec0(%rip),%rsi        # 15fc <_IO_stdin_used+0xc>
     73c:	48 83 da 03          	sbb    $0x3,%rdx
     740:	31 c0                	xor    %eax,%eax
     742:	48 29 da             	sub    %rbx,%rdx
     745:	e8 36 ff ff ff       	callq  680 <__printf_chk@plt>
    return 0;

}
     74a:	31 c0                	xor    %eax,%eax
     74c:	48 8b 74 24 68       	mov    0x68(%rsp),%rsi
     751:	64 48 33 34 25 28 00 	xor    %fs:0x28,%rsi
     758:	00 00 
     75a:	75 06                	jne    762 <main+0xb2>
     75c:	48 83 c4 70          	add    $0x70,%rsp
     760:	5b                   	pop    %rbx
     761:	c3                   	retq   
     762:	e8 f9 fe ff ff       	callq  660 <__stack_chk_fail@plt>
     767:	66 0f 1f 84 00 00 00 	nopw   0x0(%rax,%rax,1)
     76e:	00 00 

0000000000000770 <_start>:
     770:	31 ed                	xor    %ebp,%ebp
     772:	49 89 d1             	mov    %rdx,%r9
     775:	5e                   	pop    %rsi
     776:	48 89 e2             	mov    %rsp,%rdx
     779:	48 83 e4 f0          	and    $0xfffffffffffffff0,%rsp
     77d:	50                   	push   %rax
     77e:	54                   	push   %rsp
     77f:	4c 8d 05 5a 0e 00 00 	lea    0xe5a(%rip),%r8        # 15e0 <__libc_csu_fini>
     786:	48 8d 0d e3 0d 00 00 	lea    0xde3(%rip),%rcx        # 1570 <__libc_csu_init>
     78d:	48 8d 3d 1c ff ff ff 	lea    -0xe4(%rip),%rdi        # 6b0 <main>
     794:	ff 15 46 18 20 00    	callq  *0x201846(%rip)        # 201fe0 <__libc_start_main@GLIBC_2.2.5>
     79a:	f4                   	hlt    
     79b:	0f 1f 44 00 00       	nopl   0x0(%rax,%rax,1)

00000000000007a0 <deregister_tm_clones>:
     7a0:	48 8d 3d 69 18 20 00 	lea    0x201869(%rip),%rdi        # 202010 <__TMC_END__>
     7a7:	55                   	push   %rbp
     7a8:	48 8d 05 61 18 20 00 	lea    0x201861(%rip),%rax        # 202010 <__TMC_END__>
     7af:	48 39 f8             	cmp    %rdi,%rax
     7b2:	48 89 e5             	mov    %rsp,%rbp
     7b5:	74 19                	je     7d0 <deregister_tm_clones+0x30>
     7b7:	48 8b 05 1a 18 20 00 	mov    0x20181a(%rip),%rax        # 201fd8 <_ITM_deregisterTMCloneTable>
     7be:	48 85 c0             	test   %rax,%rax
     7c1:	74 0d                	je     7d0 <deregister_tm_clones+0x30>
     7c3:	5d                   	pop    %rbp
     7c4:	ff e0                	jmpq   *%rax
     7c6:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
     7cd:	00 00 00 
     7d0:	5d                   	pop    %rbp
     7d1:	c3                   	retq   
     7d2:	0f 1f 40 00          	nopl   0x0(%rax)
     7d6:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
     7dd:	00 00 00 

00000000000007e0 <register_tm_clones>:
     7e0:	48 8d 3d 29 18 20 00 	lea    0x201829(%rip),%rdi        # 202010 <__TMC_END__>
     7e7:	48 8d 35 22 18 20 00 	lea    0x201822(%rip),%rsi        # 202010 <__TMC_END__>
     7ee:	55                   	push   %rbp
     7ef:	48 29 fe             	sub    %rdi,%rsi
     7f2:	48 89 e5             	mov    %rsp,%rbp
     7f5:	48 c1 fe 03          	sar    $0x3,%rsi
     7f9:	48 89 f0             	mov    %rsi,%rax
     7fc:	48 c1 e8 3f          	shr    $0x3f,%rax
     800:	48 01 c6             	add    %rax,%rsi
     803:	48 d1 fe             	sar    %rsi
     806:	74 18                	je     820 <register_tm_clones+0x40>
     808:	48 8b 05 e1 17 20 00 	mov    0x2017e1(%rip),%rax        # 201ff0 <_ITM_registerTMCloneTable>
     80f:	48 85 c0             	test   %rax,%rax
     812:	74 0c                	je     820 <register_tm_clones+0x40>
     814:	5d                   	pop    %rbp
     815:	ff e0                	jmpq   *%rax
     817:	66 0f 1f 84 00 00 00 	nopw   0x0(%rax,%rax,1)
     81e:	00 00 
     820:	5d                   	pop    %rbp
     821:	c3                   	retq   
     822:	0f 1f 40 00          	nopl   0x0(%rax)
     826:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
     82d:	00 00 00 

0000000000000830 <__do_global_dtors_aux>:
     830:	80 3d d9 17 20 00 00 	cmpb   $0x0,0x2017d9(%rip)        # 202010 <__TMC_END__>
     837:	75 2f                	jne    868 <__do_global_dtors_aux+0x38>
     839:	48 83 3d b7 17 20 00 	cmpq   $0x0,0x2017b7(%rip)        # 201ff8 <__cxa_finalize@GLIBC_2.2.5>
     840:	00 
     841:	55                   	push   %rbp
     842:	48 89 e5             	mov    %rsp,%rbp
     845:	74 0c                	je     853 <__do_global_dtors_aux+0x23>
     847:	48 8b 3d ba 17 20 00 	mov    0x2017ba(%rip),%rdi        # 202008 <__dso_handle>
     84e:	e8 4d fe ff ff       	callq  6a0 <__cxa_finalize@plt>
     853:	e8 48 ff ff ff       	callq  7a0 <deregister_tm_clones>
     858:	c6 05 b1 17 20 00 01 	movb   $0x1,0x2017b1(%rip)        # 202010 <__TMC_END__>
     85f:	5d                   	pop    %rbp
     860:	c3                   	retq   
     861:	0f 1f 80 00 00 00 00 	nopl   0x0(%rax)
     868:	f3 c3                	repz retq 
     86a:	66 0f 1f 44 00 00    	nopw   0x0(%rax,%rax,1)

0000000000000870 <frame_dummy>:
     870:	55                   	push   %rbp
     871:	48 89 e5             	mov    %rsp,%rbp
     874:	5d                   	pop    %rbp
     875:	e9 66 ff ff ff       	jmpq   7e0 <register_tm_clones>
     87a:	66 0f 1f 44 00 00    	nopw   0x0(%rax,%rax,1)

0000000000000880 <MD5_Round_Calculate>:
	*a = Round(*a, b, I(b,c,d), k, s, i);
}

static void MD5_Round_Calculate(const unsigned char *block,
	unsigned int *A2, unsigned int *B2, unsigned int *C2, unsigned int *D2)
{
     880:	41 57                	push   %r15
     882:	41 56                	push   %r14
     884:	41 55                	push   %r13
     886:	41 54                	push   %r12
     888:	55                   	push   %rbp
     889:	53                   	push   %rbx
     88a:	48 83 ec 68          	sub    $0x68,%rsp
	//create X It is since it is required.
	unsigned int X[16]; //512bit 64byte
	int j,k;

	//Save A as AA, B as BB, C as CC, and and D as DD (saving of A, B, C, and D)
	unsigned int A=*A2, B=*B2, C=*C2, D=*D2;
     88e:	44 8b 19             	mov    (%rcx),%r11d
     891:	44 8b 12             	mov    (%rdx),%r10d
	//It is a large region variable reluctantly because of calculation of a round. . . for Round1...4
	pX = X;

	//Copy block(padding_message) i into X
	for (j=0,k=0; j<64; j+=4,k++)
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     894:	66 0f 6f 1d c4 0d 00 	movdqa 0xdc4(%rip),%xmm3        # 1660 <_IO_stdin_used+0x70>
     89b:	00 
{
     89c:	64 48 8b 1c 25 28 00 	mov    %fs:0x28,%rbx
     8a3:	00 00 
     8a5:	48 89 5c 24 58       	mov    %rbx,0x58(%rsp)
     8aa:	31 db                	xor    %ebx,%ebx
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     8ac:	f3 0f 6f 17          	movdqu (%rdi),%xmm2
     8b0:	66 0f 6f cb          	movdqa %xmm3,%xmm1
     8b4:	66 0f 6f f3          	movdqa %xmm3,%xmm6
     8b8:	f3 0f 6f 6f 10       	movdqu 0x10(%rdi),%xmm5
     8bd:	66 0f db ca          	pand   %xmm2,%xmm1
     8c1:	66 0f 71 d2 08       	psrlw  $0x8,%xmm2
     8c6:	66 0f 6f fb          	movdqa %xmm3,%xmm7
     8ca:	66 0f db f5          	pand   %xmm5,%xmm6
     8ce:	66 0f 71 d5 08       	psrlw  $0x8,%xmm5
	unsigned int A=*A2, B=*B2, C=*C2, D=*D2;
     8d3:	41 8b 18             	mov    (%r8),%ebx
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     8d6:	f3 0f 6f 67 20       	movdqu 0x20(%rdi),%xmm4
     8db:	66 0f 67 d5          	packuswb %xmm5,%xmm2
     8df:	66 0f 6f eb          	movdqa %xmm3,%xmm5
     8e3:	f3 0f 6f 47 30       	movdqu 0x30(%rdi),%xmm0
   return (X & Y) | (~X & Z);
     8e8:	44 89 df             	mov    %r11d,%edi
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     8eb:	66 0f 67 ce          	packuswb %xmm6,%xmm1
   return (X & Y) | (~X & Z);
     8ef:	31 df                	xor    %ebx,%edi
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     8f1:	66 0f 6f f3          	movdqa %xmm3,%xmm6
   return (X & Y) | (~X & Z);
     8f5:	89 f8                	mov    %edi,%eax
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     8f7:	66 0f db ec          	pand   %xmm4,%xmm5
     8fb:	66 0f 71 d4 08       	psrlw  $0x8,%xmm4
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     900:	8b 3e                	mov    (%rsi),%edi
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     902:	66 0f db f0          	pand   %xmm0,%xmm6
     906:	66 0f 71 d0 08       	psrlw  $0x8,%xmm0
   return (X & Y) | (~X & Z);
     90b:	44 21 d0             	and    %r10d,%eax
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     90e:	66 0f db f9          	pand   %xmm1,%xmm7
     912:	66 0f 71 d1 08       	psrlw  $0x8,%xmm1
	unsigned int A=*A2, B=*B2, C=*C2, D=*D2;
     917:	48 89 14 24          	mov    %rdx,(%rsp)
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     91b:	66 0f 67 e0          	packuswb %xmm0,%xmm4
   return (X & Y) | (~X & Z);
     91f:	31 d8                	xor    %ebx,%eax
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     921:	66 0f 67 ee          	packuswb %xmm6,%xmm5
	unsigned int A=*A2, B=*B2, C=*C2, D=*D2;
     925:	48 89 4c 24 08       	mov    %rcx,0x8(%rsp)
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     92a:	66 0f 6f f3          	movdqa %xmm3,%xmm6
     92e:	66 0f db f5          	pand   %xmm5,%xmm6
     932:	66 0f 71 d5 08       	psrlw  $0x8,%xmm5
     937:	66 0f 67 cd          	packuswb %xmm5,%xmm1
     93b:	66 0f 67 fe          	packuswb %xmm6,%xmm7
     93f:	66 0f 6f f3          	movdqa %xmm3,%xmm6
     943:	66 0f db dc          	pand   %xmm4,%xmm3
     947:	66 0f 71 d4 08       	psrlw  $0x8,%xmm4
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
			| ( ((unsigned int )block[j+2]) << 16 )
     94c:	66 0f 6f e9          	movdqa %xmm1,%xmm5
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     950:	66 0f db f2          	pand   %xmm2,%xmm6
     954:	66 0f 71 d2 08       	psrlw  $0x8,%xmm2
     959:	66 44 0f 6f cf       	movdqa %xmm7,%xmm9
     95e:	66 0f 67 d4          	packuswb %xmm4,%xmm2
     962:	66 0f 67 f3          	packuswb %xmm3,%xmm6
     966:	66 0f ef db          	pxor   %xmm3,%xmm3
			| ( ((unsigned int )block[j+3]) << 24 );
     96a:	66 44 0f 6f c2       	movdqa %xmm2,%xmm8
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     96f:	66 44 0f 6f d6       	movdqa %xmm6,%xmm10
			| ( ((unsigned int )block[j+2]) << 16 )
     974:	66 0f 60 eb          	punpcklbw %xmm3,%xmm5
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     978:	66 44 0f 60 d3       	punpcklbw %xmm3,%xmm10
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     97d:	66 44 0f 60 cb       	punpcklbw %xmm3,%xmm9
     982:	66 0f 68 fb          	punpckhbw %xmm3,%xmm7
			| ( ((unsigned int )block[j+2]) << 16 )
     986:	66 0f 6f c5          	movdqa %xmm5,%xmm0
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     98a:	66 0f 68 f3          	punpckhbw %xmm3,%xmm6
			| ( ((unsigned int )block[j+2]) << 16 )
     98e:	66 0f 68 cb          	punpckhbw %xmm3,%xmm1
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     992:	66 41 0f 6f e2       	movdqa %xmm10,%xmm4
			| ( ((unsigned int )block[j+3]) << 24 );
     997:	66 44 0f 60 c3       	punpcklbw %xmm3,%xmm8
     99c:	66 0f 68 d3          	punpckhbw %xmm3,%xmm2
			| ( ((unsigned int )block[j+2]) << 16 )
     9a0:	66 0f ef db          	pxor   %xmm3,%xmm3
     9a4:	66 0f 61 c3          	punpcklwd %xmm3,%xmm0
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     9a8:	66 0f 61 e3          	punpcklwd %xmm3,%xmm4
     9ac:	66 44 0f 69 d3       	punpckhwd %xmm3,%xmm10
			| ( ((unsigned int )block[j+2]) << 16 )
     9b1:	66 0f 72 f0 10       	pslld  $0x10,%xmm0
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     9b6:	66 0f 72 f4 08       	pslld  $0x8,%xmm4
			| ( ((unsigned int )block[j+2]) << 16 )
     9bb:	66 0f eb c4          	por    %xmm4,%xmm0
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     9bf:	66 41 0f 6f e1       	movdqa %xmm9,%xmm4
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     9c4:	66 41 0f 72 f2 08    	pslld  $0x8,%xmm10
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     9ca:	66 44 0f 69 cb       	punpckhwd %xmm3,%xmm9
     9cf:	66 0f 61 e3          	punpcklwd %xmm3,%xmm4
			| ( ((unsigned int )block[j+2]) << 16 )
     9d3:	66 0f eb c4          	por    %xmm4,%xmm0
			| ( ((unsigned int )block[j+3]) << 24 );
     9d7:	66 41 0f 6f e0       	movdqa %xmm8,%xmm4
     9dc:	66 44 0f 69 c3       	punpckhwd %xmm3,%xmm8
     9e1:	66 0f 61 e3          	punpcklwd %xmm3,%xmm4
     9e5:	66 41 0f 72 f0 18    	pslld  $0x18,%xmm8
     9eb:	66 0f 72 f4 18       	pslld  $0x18,%xmm4
     9f0:	66 0f eb c4          	por    %xmm4,%xmm0
			| ( ((unsigned int )block[j+2]) << 16 )
     9f4:	66 0f 6f e5          	movdqa %xmm5,%xmm4
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     9f8:	66 41 0f 7e c6       	movd   %xmm0,%r14d
			| ( ((unsigned int )block[j+2]) << 16 )
     9fd:	66 0f 69 e3          	punpckhwd %xmm3,%xmm4
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     a01:	0f 29 44 24 10       	movaps %xmm0,0x10(%rsp)
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     a06:	42 8d 94 37 78 a4 6a 	lea    -0x28955b88(%rdi,%r14,1),%edx
     a0d:	d7 
     a0e:	8b 7c 24 14          	mov    0x14(%rsp),%edi
     a12:	44 8b 7c 24 1c       	mov    0x1c(%rsp),%r15d
			| ( ((unsigned int )block[j+2]) << 16 )
     a17:	66 0f 72 f4 10       	pslld  $0x10,%xmm4
     a1c:	66 41 0f eb e2       	por    %xmm10,%xmm4
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     a21:	01 d0                	add    %edx,%eax
   return (X & Y) | (~X & Z);
     a23:	44 89 d2             	mov    %r10d,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     a26:	c1 c0 07             	rol    $0x7,%eax
   return (X & Y) | (~X & Z);
     a29:	44 31 da             	xor    %r11d,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     a2c:	8d 8c 3b 56 b7 c7 e8 	lea    -0x173848aa(%rbx,%rdi,1),%ecx
     a33:	44 01 d0             	add    %r10d,%eax
     a36:	8b 7c 24 18          	mov    0x18(%rsp),%edi
     a3a:	47 8d 8c 3a ee ce bd 	lea    -0x3e423112(%r10,%r15,1),%r9d
     a41:	c1 
   return (X & Y) | (~X & Z);
     a42:	21 c2                	and    %eax,%edx
			| ( ((unsigned int )block[j+2]) << 16 )
     a44:	66 41 0f eb e1       	por    %xmm9,%xmm4
   return (X & Y) | (~X & Z);
     a49:	44 31 da             	xor    %r11d,%edx
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     a4c:	66 0f 6f c6          	movdqa %xmm6,%xmm0
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     a50:	01 d1                	add    %edx,%ecx
   return (X & Y) | (~X & Z);
     a52:	44 89 d2             	mov    %r10d,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     a55:	41 8d bc 3b db 70 20 	lea    0x242070db(%r11,%rdi,1),%edi
     a5c:	24 
     a5d:	c1 c1 0c             	rol    $0xc,%ecx
   return (X & Y) | (~X & Z);
     a60:	31 c2                	xor    %eax,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     a62:	01 c1                	add    %eax,%ecx
			| ( ((unsigned int )block[j+3]) << 24 );
     a64:	66 41 0f eb e0       	por    %xmm8,%xmm4
   return (X & Y) | (~X & Z);
     a69:	21 ca                	and    %ecx,%edx
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     a6b:	66 0f 61 c3          	punpcklwd %xmm3,%xmm0
   return (X & Y) | (~X & Z);
     a6f:	44 31 d2             	xor    %r10d,%edx
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     a72:	66 0f 69 f3          	punpckhwd %xmm3,%xmm6
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     a76:	01 d7                	add    %edx,%edi
   return (X & Y) | (~X & Z);
     a78:	89 ca                	mov    %ecx,%edx
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     a7a:	66 0f 7e e5          	movd   %xmm4,%ebp
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     a7e:	c1 cf 0f             	ror    $0xf,%edi
   return (X & Y) | (~X & Z);
     a81:	31 c2                	xor    %eax,%edx
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     a83:	66 0f 72 f0 08       	pslld  $0x8,%xmm0
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     a88:	01 cf                	add    %ecx,%edi
			| ( ((unsigned int )block[j+1]) << 8 ) // A function called Decode as used in the field of RFC
     a8a:	66 0f 72 f6 08       	pslld  $0x8,%xmm6
   return (X & Y) | (~X & Z);
     a8f:	21 fa                	and    %edi,%edx
     a91:	31 c2                	xor    %eax,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     a93:	8d 84 05 af 0f 7c f5 	lea    -0xa83f051(%rbp,%rax,1),%eax
     a9a:	44 01 ca             	add    %r9d,%edx
   return (X & Y) | (~X & Z);
     a9d:	41 89 f9             	mov    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     aa0:	c1 ca 0a             	ror    $0xa,%edx
   return (X & Y) | (~X & Z);
     aa3:	41 31 c9             	xor    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     aa6:	01 fa                	add    %edi,%edx
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     aa8:	0f 29 64 24 20       	movaps %xmm4,0x20(%rsp)
   return (X & Y) | (~X & Z);
     aad:	41 21 d1             	and    %edx,%r9d
			| ( ((unsigned int )block[j+2]) << 16 )
     ab0:	66 0f 6f e1          	movdqa %xmm1,%xmm4
   return (X & Y) | (~X & Z);
     ab4:	41 31 c9             	xor    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ab7:	44 01 c8             	add    %r9d,%eax
			| ( ((unsigned int )block[j+2]) << 16 )
     aba:	66 0f 61 e3          	punpcklwd %xmm3,%xmm4
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     abe:	44 8b 7c 24 24       	mov    0x24(%rsp),%r15d
     ac3:	c1 c0 07             	rol    $0x7,%eax
   return (X & Y) | (~X & Z);
     ac6:	41 89 d1             	mov    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ac9:	01 d0                	add    %edx,%eax
   return (X & Y) | (~X & Z);
     acb:	41 31 f9             	xor    %edi,%r9d
     ace:	41 21 c1             	and    %eax,%r9d
			| ( ((unsigned int )block[j+2]) << 16 )
     ad1:	66 0f 72 f4 10       	pslld  $0x10,%xmm4
     ad6:	66 0f eb e0          	por    %xmm0,%xmm4
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ada:	41 8d 8c 0f 2a c6 87 	lea    0x4787c62a(%r15,%rcx,1),%ecx
     ae1:	47 
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     ae2:	66 0f 6f c7          	movdqa %xmm7,%xmm0
   return (X & Y) | (~X & Z);
     ae6:	41 31 f9             	xor    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ae9:	44 8b 7c 24 28       	mov    0x28(%rsp),%r15d
     aee:	44 01 c9             	add    %r9d,%ecx
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     af1:	66 0f 61 c3          	punpcklwd %xmm3,%xmm0
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     af5:	c1 c1 0c             	rol    $0xc,%ecx
   return (X & Y) | (~X & Z);
     af8:	41 89 c1             	mov    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     afb:	01 c1                	add    %eax,%ecx
   return (X & Y) | (~X & Z);
     afd:	41 31 d1             	xor    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b00:	41 8d bc 3f 13 46 30 	lea    -0x57cfb9ed(%r15,%rdi,1),%edi
     b07:	a8 
   return (X & Y) | (~X & Z);
     b08:	41 21 c9             	and    %ecx,%r9d
			| ( ((unsigned int )block[j+2]) << 16 )
     b0b:	66 0f eb e0          	por    %xmm0,%xmm4
			| ( ((unsigned int )block[j+3]) << 24 );
     b0f:	66 0f 6f c2          	movdqa %xmm2,%xmm0
   return (X & Y) | (~X & Z);
     b13:	41 31 d1             	xor    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b16:	44 01 cf             	add    %r9d,%edi
     b19:	44 8b 7c 24 2c       	mov    0x2c(%rsp),%r15d
   return (X & Y) | (~X & Z);
     b1e:	41 89 c9             	mov    %ecx,%r9d
			| ( ((unsigned int )block[j+3]) << 24 );
     b21:	66 0f 61 c3          	punpcklwd %xmm3,%xmm0
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b25:	c1 cf 0f             	ror    $0xf,%edi
     b28:	01 cf                	add    %ecx,%edi
   return (X & Y) | (~X & Z);
     b2a:	41 31 c1             	xor    %eax,%r9d
     b2d:	41 21 f9             	and    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b30:	41 8d 94 17 01 95 46 	lea    -0x2b96aff(%r15,%rdx,1),%edx
     b37:	fd 
			| ( ((unsigned int )block[j+3]) << 24 );
     b38:	66 0f 72 f0 18       	pslld  $0x18,%xmm0
     b3d:	66 0f eb e0          	por    %xmm0,%xmm4
   return (X & Y) | (~X & Z);
     b41:	41 31 c1             	xor    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b44:	44 01 ca             	add    %r9d,%edx
   return (X & Y) | (~X & Z);
     b47:	41 89 f9             	mov    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b4a:	c1 ca 0a             	ror    $0xa,%edx
   return (X & Y) | (~X & Z);
     b4d:	41 31 c9             	xor    %ecx,%r9d
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     b50:	66 41 0f 7e e4       	movd   %xmm4,%r12d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b55:	01 fa                	add    %edi,%edx
   return (X & Y) | (~X & Z);
     b57:	41 21 d1             	and    %edx,%r9d
			| ( ((unsigned int )block[j+2]) << 16 )
     b5a:	66 0f 6f c1          	movdqa %xmm1,%xmm0
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b5e:	41 8d 84 04 d8 98 80 	lea    0x698098d8(%r12,%rax,1),%eax
     b65:	69 
   return (X & Y) | (~X & Z);
     b66:	41 31 c9             	xor    %ecx,%r9d
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     b69:	0f 29 64 24 30       	movaps %xmm4,0x30(%rsp)
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b6e:	44 01 c8             	add    %r9d,%eax
     b71:	44 8b 7c 24 34       	mov    0x34(%rsp),%r15d
   return (X & Y) | (~X & Z);
     b76:	41 89 d1             	mov    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b79:	c1 c0 07             	rol    $0x7,%eax
   return (X & Y) | (~X & Z);
     b7c:	41 31 f9             	xor    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b7f:	01 d0                	add    %edx,%eax
			| ( ((unsigned int )block[j+2]) << 16 )
     b81:	66 0f 69 c3          	punpckhwd %xmm3,%xmm0
   return (X & Y) | (~X & Z);
     b85:	41 21 c1             	and    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b88:	41 8d 8c 0f af f7 44 	lea    -0x74bb0851(%r15,%rcx,1),%ecx
     b8f:	8b 
     b90:	44 8b 7c 24 38       	mov    0x38(%rsp),%r15d
   return (X & Y) | (~X & Z);
     b95:	41 31 f9             	xor    %edi,%r9d
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     b98:	66 0f 69 fb          	punpckhwd %xmm3,%xmm7
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     b9c:	44 01 c9             	add    %r9d,%ecx
   return (X & Y) | (~X & Z);
     b9f:	41 89 c1             	mov    %eax,%r9d
			| ( ((unsigned int )block[j+2]) << 16 )
     ba2:	66 0f 72 f0 10       	pslld  $0x10,%xmm0
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ba7:	c1 c1 0c             	rol    $0xc,%ecx
   return (X & Y) | (~X & Z);
     baa:	41 31 d1             	xor    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     bad:	41 8d bc 3f b1 5b ff 	lea    -0xa44f(%r15,%rdi,1),%edi
     bb4:	ff 
     bb5:	01 c1                	add    %eax,%ecx
			| ( ((unsigned int )block[j+2]) << 16 )
     bb7:	66 0f eb c6          	por    %xmm6,%xmm0
   return (X & Y) | (~X & Z);
     bbb:	41 21 c9             	and    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     bbe:	44 8b 7c 24 3c       	mov    0x3c(%rsp),%r15d
   return (X & Y) | (~X & Z);
     bc3:	41 31 d1             	xor    %edx,%r9d
			| ( ((unsigned int )block[j+3]) << 24 );
     bc6:	66 0f 69 d3          	punpckhwd %xmm3,%xmm2
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     bca:	44 01 cf             	add    %r9d,%edi
			| ( ((unsigned int )block[j+2]) << 16 )
     bcd:	66 0f eb c7          	por    %xmm7,%xmm0
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     bd1:	c1 cf 0f             	ror    $0xf,%edi
   return (X & Y) | (~X & Z);
     bd4:	41 89 c9             	mov    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     bd7:	41 8d 94 17 be d7 5c 	lea    -0x76a32842(%r15,%rdx,1),%edx
     bde:	89 
     bdf:	01 cf                	add    %ecx,%edi
   return (X & Y) | (~X & Z);
     be1:	41 31 c1             	xor    %eax,%r9d
			| ( ((unsigned int )block[j+3]) << 24 );
     be4:	66 0f 72 f2 18       	pslld  $0x18,%xmm2
   return (X & Y) | (~X & Z);
     be9:	41 21 f9             	and    %edi,%r9d
			| ( ((unsigned int )block[j+3]) << 24 );
     bec:	66 0f eb c2          	por    %xmm2,%xmm0
   return (X & Y) | (~X & Z);
     bf0:	41 31 c1             	xor    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     bf3:	44 01 ca             	add    %r9d,%edx
   return (X & Y) | (~X & Z);
     bf6:	41 89 f9             	mov    %edi,%r9d
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     bf9:	66 41 0f 7e c5       	movd   %xmm0,%r13d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     bfe:	c1 ca 0a             	ror    $0xa,%edx
   return (X & Y) | (~X & Z);
     c01:	41 31 c9             	xor    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c04:	01 fa                	add    %edi,%edx
   return (X & Y) | (~X & Z);
     c06:	41 21 d1             	and    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c09:	41 8d 84 05 22 11 90 	lea    0x6b901122(%r13,%rax,1),%eax
     c10:	6b 
   return (X & Y) | (~X & Z);
     c11:	41 31 c9             	xor    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c14:	44 01 c8             	add    %r9d,%eax
   return (X & Y) | (~X & Z);
     c17:	41 89 d1             	mov    %edx,%r9d
		X[k] = ( (unsigned int )block[j] )         // 8byte*4 -> 32byte conversion
     c1a:	0f 29 44 24 40       	movaps %xmm0,0x40(%rsp)
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c1f:	44 8b 7c 24 44       	mov    0x44(%rsp),%r15d
     c24:	c1 c0 07             	rol    $0x7,%eax
   return (X & Y) | (~X & Z);
     c27:	41 31 f9             	xor    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c2a:	01 d0                	add    %edx,%eax
   return (X & Y) | (~X & Z);
     c2c:	41 21 c1             	and    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c2f:	41 8d 8c 0f 93 71 98 	lea    -0x2678e6d(%r15,%rcx,1),%ecx
     c36:	fd 
   return (X & Y) | (~X & Z);
     c37:	41 31 f9             	xor    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c3a:	44 8b 7c 24 48       	mov    0x48(%rsp),%r15d
     c3f:	44 01 c9             	add    %r9d,%ecx
   return (X & Y) | (~X & Z);
     c42:	41 89 c1             	mov    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c45:	c1 c1 0c             	rol    $0xc,%ecx
   return (X & Y) | (~X & Z);
     c48:	41 31 d1             	xor    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c4b:	41 8d bc 3f 8e 43 79 	lea    -0x5986bc72(%r15,%rdi,1),%edi
     c52:	a6 
     c53:	01 c1                	add    %eax,%ecx
     c55:	44 8b 7c 24 4c       	mov    0x4c(%rsp),%r15d
   return (X & Y) | (~X & Z);
     c5a:	41 21 c9             	and    %ecx,%r9d
     c5d:	41 31 d1             	xor    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c60:	44 01 cf             	add    %r9d,%edi
   return (X & Y) | (~X & Z);
     c63:	41 89 c9             	mov    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c66:	41 8d 94 17 21 08 b4 	lea    0x49b40821(%r15,%rdx,1),%edx
     c6d:	49 
     c6e:	c1 cf 0f             	ror    $0xf,%edi
   return (X & Y) | (~X & Z);
     c71:	41 31 c1             	xor    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c74:	44 8b 7c 24 14       	mov    0x14(%rsp),%r15d
     c79:	01 cf                	add    %ecx,%edi
   return (X & Y) | (~X & Z);
     c7b:	41 21 f9             	and    %edi,%r9d
     c7e:	41 31 c1             	xor    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c81:	42 8d 84 38 62 25 1e 	lea    -0x9e1da9e(%rax,%r15,1),%eax
     c88:	f6 
     c89:	44 8b 7c 24 28       	mov    0x28(%rsp),%r15d
     c8e:	44 01 ca             	add    %r9d,%edx
     c91:	c1 ca 0a             	ror    $0xa,%edx
     c94:	01 fa                	add    %edi,%edx
   return (X & Z) | (Y & ~Z);
     c96:	41 89 d1             	mov    %edx,%r9d
     c99:	41 31 f9             	xor    %edi,%r9d
     c9c:	41 21 c9             	and    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     c9f:	42 8d 8c 39 40 b3 40 	lea    -0x3fbf4cc0(%rcx,%r15,1),%ecx
     ca6:	c0 
     ca7:	44 8b 7c 24 3c       	mov    0x3c(%rsp),%r15d
   return (X & Z) | (Y & ~Z);
     cac:	41 31 f9             	xor    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     caf:	44 01 c8             	add    %r9d,%eax
     cb2:	c1 c0 05             	rol    $0x5,%eax
     cb5:	01 d0                	add    %edx,%eax
   return (X & Z) | (Y & ~Z);
     cb7:	41 89 c1             	mov    %eax,%r9d
     cba:	41 31 d1             	xor    %edx,%r9d
     cbd:	41 21 f9             	and    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     cc0:	42 8d bc 3f 51 5a 5e 	lea    0x265e5a51(%rdi,%r15,1),%edi
     cc7:	26 
     cc8:	44 8b 7c 24 24       	mov    0x24(%rsp),%r15d
   return (X & Z) | (Y & ~Z);
     ccd:	41 31 d1             	xor    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     cd0:	44 01 c9             	add    %r9d,%ecx
     cd3:	c1 c1 09             	rol    $0x9,%ecx
     cd6:	01 c1                	add    %eax,%ecx
   return (X & Z) | (Y & ~Z);
     cd8:	41 89 c9             	mov    %ecx,%r9d
     cdb:	41 31 c1             	xor    %eax,%r9d
     cde:	41 21 d1             	and    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ce1:	42 8d 94 32 aa c7 b6 	lea    -0x16493856(%rdx,%r14,1),%edx
     ce8:	e9 
   return (X & Z) | (Y & ~Z);
     ce9:	41 31 c1             	xor    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     cec:	44 01 cf             	add    %r9d,%edi
     cef:	c1 c7 0e             	rol    $0xe,%edi
     cf2:	01 cf                	add    %ecx,%edi
   return (X & Z) | (Y & ~Z);
     cf4:	41 89 f9             	mov    %edi,%r9d
     cf7:	41 31 c9             	xor    %ecx,%r9d
     cfa:	41 21 c1             	and    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     cfd:	42 8d 84 38 5d 10 2f 	lea    -0x29d0efa3(%rax,%r15,1),%eax
     d04:	d6 
     d05:	44 8b 7c 24 38       	mov    0x38(%rsp),%r15d
   return (X & Z) | (Y & ~Z);
     d0a:	41 31 c9             	xor    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     d0d:	44 01 ca             	add    %r9d,%edx
     d10:	c1 ca 0c             	ror    $0xc,%edx
     d13:	01 fa                	add    %edi,%edx
   return (X & Z) | (Y & ~Z);
     d15:	41 89 d1             	mov    %edx,%r9d
     d18:	41 31 f9             	xor    %edi,%r9d
     d1b:	41 21 c9             	and    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     d1e:	42 8d 8c 39 53 14 44 	lea    0x2441453(%rcx,%r15,1),%ecx
     d25:	02 
     d26:	44 8b 7c 24 4c       	mov    0x4c(%rsp),%r15d
   return (X & Z) | (Y & ~Z);
     d2b:	41 31 f9             	xor    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     d2e:	44 01 c8             	add    %r9d,%eax
     d31:	c1 c0 05             	rol    $0x5,%eax
     d34:	01 d0                	add    %edx,%eax
   return (X & Z) | (Y & ~Z);
     d36:	41 89 c1             	mov    %eax,%r9d
     d39:	41 31 d1             	xor    %edx,%r9d
     d3c:	41 21 f9             	and    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     d3f:	42 8d bc 3f 81 e6 a1 	lea    -0x275e197f(%rdi,%r15,1),%edi
     d46:	d8 
     d47:	44 8b 7c 24 34       	mov    0x34(%rsp),%r15d
   return (X & Z) | (Y & ~Z);
     d4c:	41 31 d1             	xor    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     d4f:	44 01 c9             	add    %r9d,%ecx
     d52:	c1 c1 09             	rol    $0x9,%ecx
     d55:	01 c1                	add    %eax,%ecx
   return (X & Z) | (Y & ~Z);
     d57:	41 89 c9             	mov    %ecx,%r9d
     d5a:	41 31 c1             	xor    %eax,%r9d
     d5d:	41 21 d1             	and    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     d60:	8d 94 2a c8 fb d3 e7 	lea    -0x182c0438(%rdx,%rbp,1),%edx
   return (X & Z) | (Y & ~Z);
     d67:	41 31 c1             	xor    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     d6a:	44 01 cf             	add    %r9d,%edi
     d6d:	c1 c7 0e             	rol    $0xe,%edi
     d70:	01 cf                	add    %ecx,%edi
   return (X & Z) | (Y & ~Z);
     d72:	41 89 f9             	mov    %edi,%r9d
     d75:	41 31 c9             	xor    %ecx,%r9d
     d78:	41 21 c1             	and    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     d7b:	42 8d 84 38 e6 cd e1 	lea    0x21e1cde6(%rax,%r15,1),%eax
     d82:	21 
     d83:	44 8b 7c 24 48       	mov    0x48(%rsp),%r15d
   return (X & Z) | (Y & ~Z);
     d88:	41 31 c9             	xor    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     d8b:	44 01 ca             	add    %r9d,%edx
     d8e:	c1 ca 0c             	ror    $0xc,%edx
     d91:	01 fa                	add    %edi,%edx
   return (X & Z) | (Y & ~Z);
     d93:	41 89 d1             	mov    %edx,%r9d
     d96:	41 31 f9             	xor    %edi,%r9d
     d99:	41 21 c9             	and    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     d9c:	42 8d 8c 39 d6 07 37 	lea    -0x3cc8f82a(%rcx,%r15,1),%ecx
     da3:	c3 
     da4:	44 8b 7c 24 1c       	mov    0x1c(%rsp),%r15d
   return (X & Z) | (Y & ~Z);
     da9:	41 31 f9             	xor    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     dac:	44 01 c8             	add    %r9d,%eax
     daf:	c1 c0 05             	rol    $0x5,%eax
     db2:	01 d0                	add    %edx,%eax
   return (X & Z) | (Y & ~Z);
     db4:	41 89 c1             	mov    %eax,%r9d
     db7:	41 31 d1             	xor    %edx,%r9d
     dba:	41 21 f9             	and    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     dbd:	42 8d bc 3f 87 0d d5 	lea    -0xb2af279(%rdi,%r15,1),%edi
     dc4:	f4 
     dc5:	44 8b 7c 24 44       	mov    0x44(%rsp),%r15d
   return (X & Z) | (Y & ~Z);
     dca:	41 31 d1             	xor    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     dcd:	44 01 c9             	add    %r9d,%ecx
     dd0:	c1 c1 09             	rol    $0x9,%ecx
     dd3:	01 c1                	add    %eax,%ecx
   return (X & Z) | (Y & ~Z);
     dd5:	41 89 c9             	mov    %ecx,%r9d
     dd8:	41 31 c1             	xor    %eax,%r9d
     ddb:	41 21 d1             	and    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     dde:	42 8d 94 22 ed 14 5a 	lea    0x455a14ed(%rdx,%r12,1),%edx
     de5:	45 
   return (X & Z) | (Y & ~Z);
     de6:	41 31 c1             	xor    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     de9:	44 01 cf             	add    %r9d,%edi
     dec:	c1 c7 0e             	rol    $0xe,%edi
     def:	01 cf                	add    %ecx,%edi
   return (X & Z) | (Y & ~Z);
     df1:	41 89 f9             	mov    %edi,%r9d
     df4:	41 31 c9             	xor    %ecx,%r9d
     df7:	41 21 c1             	and    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     dfa:	42 8d 84 38 05 e9 e3 	lea    -0x561c16fb(%rax,%r15,1),%eax
     e01:	a9 
     e02:	44 8b 7c 24 18       	mov    0x18(%rsp),%r15d
   return (X & Z) | (Y & ~Z);
     e07:	41 31 c9             	xor    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     e0a:	44 01 ca             	add    %r9d,%edx
     e0d:	c1 ca 0c             	ror    $0xc,%edx
     e10:	01 fa                	add    %edi,%edx
   return (X & Z) | (Y & ~Z);
     e12:	41 89 d1             	mov    %edx,%r9d
     e15:	41 31 f9             	xor    %edi,%r9d
     e18:	41 21 c9             	and    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     e1b:	42 8d 8c 39 f8 a3 ef 	lea    -0x3105c08(%rcx,%r15,1),%ecx
     e22:	fc 
     e23:	44 8b 7c 24 2c       	mov    0x2c(%rsp),%r15d
   return (X & Z) | (Y & ~Z);
     e28:	41 31 f9             	xor    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     e2b:	44 01 c8             	add    %r9d,%eax
     e2e:	c1 c0 05             	rol    $0x5,%eax
     e31:	01 d0                	add    %edx,%eax
   return (X & Z) | (Y & ~Z);
     e33:	41 89 c1             	mov    %eax,%r9d
     e36:	41 31 d1             	xor    %edx,%r9d
     e39:	41 21 f9             	and    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     e3c:	42 8d bc 3f d9 02 6f 	lea    0x676f02d9(%rdi,%r15,1),%edi
     e43:	67 
   return (X & Z) | (Y & ~Z);
     e44:	41 31 d1             	xor    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     e47:	44 01 c9             	add    %r9d,%ecx
     e4a:	c1 c1 09             	rol    $0x9,%ecx
     e4d:	01 c1                	add    %eax,%ecx
   return (X & Z) | (Y & ~Z);
     e4f:	41 89 c9             	mov    %ecx,%r9d
     e52:	41 31 c1             	xor    %eax,%r9d
     e55:	41 21 d1             	and    %edx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     e58:	42 8d 94 2a 8a 4c 2a 	lea    -0x72d5b376(%rdx,%r13,1),%edx
     e5f:	8d 
   return (X & Z) | (Y & ~Z);
     e60:	41 31 c1             	xor    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     e63:	44 01 cf             	add    %r9d,%edi
     e66:	c1 c7 0e             	rol    $0xe,%edi
     e69:	01 cf                	add    %ecx,%edi
     e6b:	41 89 ff             	mov    %edi,%r15d
     e6e:	41 31 cf             	xor    %ecx,%r15d
   return (X & Z) | (Y & ~Z);
     e71:	45 89 f9             	mov    %r15d,%r9d
     e74:	41 21 c1             	and    %eax,%r9d
     e77:	41 31 c9             	xor    %ecx,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     e7a:	42 8d 8c 21 81 f6 71 	lea    -0x788e097f(%rcx,%r12,1),%ecx
     e81:	87 
     e82:	44 01 ca             	add    %r9d,%edx
     e85:	44 8b 4c 24 24       	mov    0x24(%rsp),%r9d
     e8a:	c1 ca 0c             	ror    $0xc,%edx
     e8d:	01 fa                	add    %edi,%edx
     e8f:	42 8d 84 08 42 39 fa 	lea    -0x5c6be(%rax,%r9,1),%eax
     e96:	ff 
   return X ^ Y ^ Z;
     e97:	41 31 d7             	xor    %edx,%r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     e9a:	44 01 f8             	add    %r15d,%eax
   return X ^ Y ^ Z;
     e9d:	41 89 d7             	mov    %edx,%r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ea0:	c1 c0 04             	rol    $0x4,%eax
   return X ^ Y ^ Z;
     ea3:	41 31 ff             	xor    %edi,%r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ea6:	44 8d 0c 10          	lea    (%rax,%rdx,1),%r9d
   return X ^ Y ^ Z;
     eaa:	44 89 f8             	mov    %r15d,%eax
     ead:	45 89 cf             	mov    %r9d,%r15d
     eb0:	44 31 c8             	xor    %r9d,%eax
     eb3:	41 31 d7             	xor    %edx,%r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     eb6:	01 c1                	add    %eax,%ecx
   return X ^ Y ^ Z;
     eb8:	44 89 f8             	mov    %r15d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ebb:	44 8b 7c 24 3c       	mov    0x3c(%rsp),%r15d
     ec0:	c1 c1 0b             	rol    $0xb,%ecx
     ec3:	44 01 c9             	add    %r9d,%ecx
   return X ^ Y ^ Z;
     ec6:	31 c8                	xor    %ecx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ec8:	42 8d bc 3f 22 61 9d 	lea    0x6d9d6122(%rdi,%r15,1),%edi
     ecf:	6d 
   return X ^ Y ^ Z;
     ed0:	41 89 cf             	mov    %ecx,%r15d
     ed3:	45 31 cf             	xor    %r9d,%r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ed6:	01 c7                	add    %eax,%edi
   return X ^ Y ^ Z;
     ed8:	44 89 f8             	mov    %r15d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     edb:	44 8b 7c 24 48       	mov    0x48(%rsp),%r15d
     ee0:	c1 c7 10             	rol    $0x10,%edi
     ee3:	01 cf                	add    %ecx,%edi
     ee5:	42 8d 94 3a 0c 38 e5 	lea    -0x21ac7f4(%rdx,%r15,1),%edx
     eec:	fd 
   return X ^ Y ^ Z;
     eed:	31 f8                	xor    %edi,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     eef:	44 8b 7c 24 14       	mov    0x14(%rsp),%r15d
     ef4:	01 d0                	add    %edx,%eax
   return X ^ Y ^ Z;
     ef6:	89 fa                	mov    %edi,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ef8:	c1 c8 09             	ror    $0x9,%eax
   return X ^ Y ^ Z;
     efb:	31 ca                	xor    %ecx,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     efd:	47 8d 8c 39 44 ea be 	lea    -0x5b4115bc(%r9,%r15,1),%r9d
     f04:	a4 
     f05:	01 f8                	add    %edi,%eax
     f07:	8d 8c 29 a9 cf de 4b 	lea    0x4bdecfa9(%rcx,%rbp,1),%ecx
     f0e:	44 8b 7c 24 2c       	mov    0x2c(%rsp),%r15d
   return X ^ Y ^ Z;
     f13:	31 c2                	xor    %eax,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f15:	41 01 d1             	add    %edx,%r9d
   return X ^ Y ^ Z;
     f18:	89 c2                	mov    %eax,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f1a:	41 c1 c1 04          	rol    $0x4,%r9d
   return X ^ Y ^ Z;
     f1e:	31 fa                	xor    %edi,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f20:	42 8d bc 3f 60 4b bb 	lea    -0x944b4a0(%rdi,%r15,1),%edi
     f27:	f6 
     f28:	41 01 c1             	add    %eax,%r9d
     f2b:	44 8b 7c 24 38       	mov    0x38(%rsp),%r15d
   return X ^ Y ^ Z;
     f30:	44 31 ca             	xor    %r9d,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f33:	01 d1                	add    %edx,%ecx
   return X ^ Y ^ Z;
     f35:	44 89 ca             	mov    %r9d,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f38:	c1 c1 0b             	rol    $0xb,%ecx
   return X ^ Y ^ Z;
     f3b:	31 c2                	xor    %eax,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f3d:	42 8d 84 38 70 bc bf 	lea    -0x41404390(%rax,%r15,1),%eax
     f44:	be 
     f45:	44 01 c9             	add    %r9d,%ecx
   return X ^ Y ^ Z;
     f48:	31 ca                	xor    %ecx,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f4a:	01 fa                	add    %edi,%edx
   return X ^ Y ^ Z;
     f4c:	89 cf                	mov    %ecx,%edi
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f4e:	c1 c2 10             	rol    $0x10,%edx
   return X ^ Y ^ Z;
     f51:	44 31 cf             	xor    %r9d,%edi
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f54:	01 ca                	add    %ecx,%edx
   return X ^ Y ^ Z;
     f56:	41 89 d7             	mov    %edx,%r15d
     f59:	31 d7                	xor    %edx,%edi
     f5b:	41 31 cf             	xor    %ecx,%r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f5e:	01 c7                	add    %eax,%edi
     f60:	42 8d 8c 31 fa 27 a1 	lea    -0x155ed806(%rcx,%r14,1),%ecx
     f67:	ea 
   return X ^ Y ^ Z;
     f68:	44 89 f8             	mov    %r15d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f6b:	44 8b 7c 24 44       	mov    0x44(%rsp),%r15d
     f70:	c1 cf 09             	ror    $0x9,%edi
     f73:	01 d7                	add    %edx,%edi
   return X ^ Y ^ Z;
     f75:	31 f8                	xor    %edi,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f77:	47 8d 8c 39 c6 7e 9b 	lea    0x289b7ec6(%r9,%r15,1),%r9d
     f7e:	28 
   return X ^ Y ^ Z;
     f7f:	41 89 ff             	mov    %edi,%r15d
     f82:	41 31 d7             	xor    %edx,%r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f85:	41 01 c1             	add    %eax,%r9d
   return X ^ Y ^ Z;
     f88:	44 89 f8             	mov    %r15d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f8b:	41 c1 c1 04          	rol    $0x4,%r9d
     f8f:	41 01 f9             	add    %edi,%r9d
   return X ^ Y ^ Z;
     f92:	45 89 cf             	mov    %r9d,%r15d
     f95:	44 31 c8             	xor    %r9d,%eax
     f98:	41 31 ff             	xor    %edi,%r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     f9b:	01 c8                	add    %ecx,%eax
   return X ^ Y ^ Z;
     f9d:	44 89 f9             	mov    %r15d,%ecx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     fa0:	44 8b 7c 24 1c       	mov    0x1c(%rsp),%r15d
     fa5:	c1 c0 0b             	rol    $0xb,%eax
     fa8:	44 01 c8             	add    %r9d,%eax
   return X ^ Y ^ Z;
     fab:	31 c1                	xor    %eax,%ecx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     fad:	42 8d 94 3a 85 30 ef 	lea    -0x2b10cf7b(%rdx,%r15,1),%edx
     fb4:	d4 
     fb5:	44 8b 7c 24 28       	mov    0x28(%rsp),%r15d
     fba:	01 d1                	add    %edx,%ecx
   return X ^ Y ^ Z;
     fbc:	89 c2                	mov    %eax,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     fbe:	c1 c1 10             	rol    $0x10,%ecx
   return X ^ Y ^ Z;
     fc1:	44 31 ca             	xor    %r9d,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     fc4:	42 8d bc 3f 05 1d 88 	lea    0x4881d05(%rdi,%r15,1),%edi
     fcb:	04 
     fcc:	01 c1                	add    %eax,%ecx
     fce:	44 8b 7c 24 34       	mov    0x34(%rsp),%r15d
   return X ^ Y ^ Z;
     fd3:	31 ca                	xor    %ecx,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     fd5:	01 d7                	add    %edx,%edi
   return X ^ Y ^ Z;
     fd7:	89 ca                	mov    %ecx,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     fd9:	c1 cf 09             	ror    $0x9,%edi
   return X ^ Y ^ Z;
     fdc:	31 c2                	xor    %eax,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     fde:	47 8d 8c 39 39 d0 d4 	lea    -0x262b2fc7(%r9,%r15,1),%r9d
     fe5:	d9 
     fe6:	01 cf                	add    %ecx,%edi
     fe8:	42 8d 84 28 e5 99 db 	lea    -0x1924661b(%rax,%r13,1),%eax
     fef:	e6 
   return X ^ Y ^ Z;
     ff0:	31 fa                	xor    %edi,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ff2:	44 01 ca             	add    %r9d,%edx
   return X ^ Y ^ Z;
     ff5:	41 89 f9             	mov    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
     ff8:	c1 c2 04             	rol    $0x4,%edx
     ffb:	01 fa                	add    %edi,%edx
   return X ^ Y ^ Z;
     ffd:	41 31 c9             	xor    %ecx,%r9d
    1000:	41 89 d7             	mov    %edx,%r15d
    1003:	41 31 d1             	xor    %edx,%r9d
    1006:	41 31 ff             	xor    %edi,%r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1009:	41 01 c1             	add    %eax,%r9d
   return X ^ Y ^ Z;
    100c:	44 89 f8             	mov    %r15d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    100f:	44 8b 7c 24 4c       	mov    0x4c(%rsp),%r15d
    1014:	41 c1 c1 0b          	rol    $0xb,%r9d
    1018:	41 01 d1             	add    %edx,%r9d
   return X ^ Y ^ Z;
    101b:	44 31 c8             	xor    %r9d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    101e:	42 8d 8c 39 f8 7c a2 	lea    0x1fa27cf8(%rcx,%r15,1),%ecx
    1025:	1f 
   return X ^ Y ^ Z;
    1026:	45 89 cf             	mov    %r9d,%r15d
    1029:	41 31 d7             	xor    %edx,%r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    102c:	42 8d 94 32 44 22 29 	lea    -0xbd6ddbc(%rdx,%r14,1),%edx
    1033:	f4 
    1034:	01 c1                	add    %eax,%ecx
   return X ^ Y ^ Z;
    1036:	44 89 f8             	mov    %r15d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1039:	44 8b 7c 24 18       	mov    0x18(%rsp),%r15d
    103e:	c1 c1 10             	rol    $0x10,%ecx
    1041:	44 01 c9             	add    %r9d,%ecx
    1044:	42 8d bc 3f 65 56 ac 	lea    -0x3b53a99b(%rdi,%r15,1),%edi
    104b:	c4 
   return X ^ Y ^ Z;
    104c:	31 c8                	xor    %ecx,%eax
   return Y ^ (X | ~Z);
    104e:	45 89 cf             	mov    %r9d,%r15d
    1051:	41 f7 d7             	not    %r15d
    1054:	41 89 ce             	mov    %ecx,%r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1057:	01 c7                	add    %eax,%edi
   return Y ^ (X | ~Z);
    1059:	44 89 f8             	mov    %r15d,%eax
    105c:	41 f7 d6             	not    %r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    105f:	c1 cf 09             	ror    $0x9,%edi
    1062:	01 cf                	add    %ecx,%edi
   return Y ^ (X | ~Z);
    1064:	09 f8                	or     %edi,%eax
    1066:	31 c8                	xor    %ecx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1068:	01 c2                	add    %eax,%edx
   return Y ^ (X | ~Z);
    106a:	44 89 f0             	mov    %r14d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    106d:	44 8b 74 24 2c       	mov    0x2c(%rsp),%r14d
    1072:	c1 c2 06             	rol    $0x6,%edx
    1075:	01 fa                	add    %edi,%edx
   return Y ^ (X | ~Z);
    1077:	09 d0                	or     %edx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1079:	47 8d 8c 31 97 ff 2a 	lea    0x432aff97(%r9,%r14,1),%r9d
    1080:	43 
   return Y ^ (X | ~Z);
    1081:	41 89 fe             	mov    %edi,%r14d
    1084:	31 f8                	xor    %edi,%eax
    1086:	41 f7 d6             	not    %r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1089:	41 01 c1             	add    %eax,%r9d
   return Y ^ (X | ~Z);
    108c:	44 89 f0             	mov    %r14d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    108f:	44 8b 74 24 48       	mov    0x48(%rsp),%r14d
    1094:	41 c1 c1 0a          	rol    $0xa,%r9d
    1098:	41 01 d1             	add    %edx,%r9d
   return Y ^ (X | ~Z);
    109b:	44 09 c8             	or     %r9d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    109e:	42 8d 8c 31 a7 23 94 	lea    -0x546bdc59(%rcx,%r14,1),%ecx
    10a5:	ab 
   return Y ^ (X | ~Z);
    10a6:	41 89 d6             	mov    %edx,%r14d
    10a9:	31 d0                	xor    %edx,%eax
    10ab:	41 f7 d6             	not    %r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    10ae:	42 8d 94 2a c3 59 5b 	lea    0x655b59c3(%rdx,%r13,1),%edx
    10b5:	65 
    10b6:	01 c1                	add    %eax,%ecx
   return Y ^ (X | ~Z);
    10b8:	44 89 f0             	mov    %r14d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    10bb:	44 8b 74 24 24       	mov    0x24(%rsp),%r14d
    10c0:	c1 c1 0f             	rol    $0xf,%ecx
    10c3:	44 01 c9             	add    %r9d,%ecx
   return Y ^ (X | ~Z);
    10c6:	09 c8                	or     %ecx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    10c8:	42 8d bc 37 39 a0 93 	lea    -0x36c5fc7(%rdi,%r14,1),%edi
    10cf:	fc 
   return Y ^ (X | ~Z);
    10d0:	45 89 ce             	mov    %r9d,%r14d
    10d3:	44 31 c8             	xor    %r9d,%eax
    10d6:	41 f7 d6             	not    %r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    10d9:	01 c7                	add    %eax,%edi
   return Y ^ (X | ~Z);
    10db:	44 89 f0             	mov    %r14d,%eax
    10de:	41 89 ce             	mov    %ecx,%r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    10e1:	c1 cf 0b             	ror    $0xb,%edi
   return Y ^ (X | ~Z);
    10e4:	41 f7 d6             	not    %r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    10e7:	01 cf                	add    %ecx,%edi
   return Y ^ (X | ~Z);
    10e9:	09 f8                	or     %edi,%eax
    10eb:	31 c8                	xor    %ecx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    10ed:	01 c2                	add    %eax,%edx
   return Y ^ (X | ~Z);
    10ef:	44 89 f0             	mov    %r14d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    10f2:	44 8b 74 24 1c       	mov    0x1c(%rsp),%r14d
    10f7:	c1 c2 06             	rol    $0x6,%edx
    10fa:	01 fa                	add    %edi,%edx
   return Y ^ (X | ~Z);
    10fc:	09 d0                	or     %edx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    10fe:	47 8d 8c 31 92 cc 0c 	lea    -0x70f3336e(%r9,%r14,1),%r9d
    1105:	8f 
   return Y ^ (X | ~Z);
    1106:	41 89 fe             	mov    %edi,%r14d
    1109:	31 f8                	xor    %edi,%eax
    110b:	41 f7 d6             	not    %r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    110e:	41 01 c1             	add    %eax,%r9d
   return Y ^ (X | ~Z);
    1111:	44 89 f0             	mov    %r14d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1114:	44 8b 74 24 38       	mov    0x38(%rsp),%r14d
    1119:	41 c1 c1 0a          	rol    $0xa,%r9d
    111d:	41 01 d1             	add    %edx,%r9d
   return Y ^ (X | ~Z);
    1120:	44 09 c8             	or     %r9d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1123:	42 8d 8c 31 7d f4 ef 	lea    -0x100b83(%rcx,%r14,1),%ecx
    112a:	ff 
   return Y ^ (X | ~Z);
    112b:	41 89 d6             	mov    %edx,%r14d
    112e:	31 d0                	xor    %edx,%eax
    1130:	41 f7 d6             	not    %r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1133:	42 8d 94 22 4f 7e a8 	lea    0x6fa87e4f(%rdx,%r12,1),%edx
    113a:	6f 
    113b:	01 c1                	add    %eax,%ecx
   return Y ^ (X | ~Z);
    113d:	44 89 f0             	mov    %r14d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1140:	44 8b 74 24 14       	mov    0x14(%rsp),%r14d
    1145:	c1 c1 0f             	rol    $0xf,%ecx
    1148:	44 01 c9             	add    %r9d,%ecx
   return Y ^ (X | ~Z);
    114b:	09 c8                	or     %ecx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    114d:	42 8d bc 37 d1 5d 84 	lea    -0x7a7ba22f(%rdi,%r14,1),%edi
    1154:	85 
   return Y ^ (X | ~Z);
    1155:	45 89 ce             	mov    %r9d,%r14d
    1158:	44 31 c8             	xor    %r9d,%eax
    115b:	41 f7 d6             	not    %r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    115e:	01 c7                	add    %eax,%edi
   return Y ^ (X | ~Z);
    1160:	44 89 f0             	mov    %r14d,%eax
    1163:	41 89 ce             	mov    %ecx,%r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1166:	c1 cf 0b             	ror    $0xb,%edi
   return Y ^ (X | ~Z);
    1169:	41 f7 d6             	not    %r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    116c:	01 cf                	add    %ecx,%edi
   return Y ^ (X | ~Z);
    116e:	09 f8                	or     %edi,%eax
    1170:	31 c8                	xor    %ecx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1172:	01 c2                	add    %eax,%edx
   return Y ^ (X | ~Z);
    1174:	44 89 f0             	mov    %r14d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1177:	44 8b 74 24 4c       	mov    0x4c(%rsp),%r14d
    117c:	c1 c2 06             	rol    $0x6,%edx
    117f:	01 fa                	add    %edi,%edx
   return Y ^ (X | ~Z);
    1181:	09 d0                	or     %edx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1183:	47 8d 8c 31 e0 e6 2c 	lea    -0x1d31920(%r9,%r14,1),%r9d
    118a:	fe 
   return Y ^ (X | ~Z);
    118b:	41 89 fe             	mov    %edi,%r14d
    118e:	31 f8                	xor    %edi,%eax
    1190:	41 f7 d6             	not    %r14d
    1193:	41 89 d7             	mov    %edx,%r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1196:	41 01 c1             	add    %eax,%r9d
   return Y ^ (X | ~Z);
    1199:	44 89 f0             	mov    %r14d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    119c:	44 8b 74 24 28       	mov    0x28(%rsp),%r14d
    11a1:	41 c1 c1 0a          	rol    $0xa,%r9d
   return Y ^ (X | ~Z);
    11a5:	41 f7 d7             	not    %r15d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    11a8:	41 01 d1             	add    %edx,%r9d
   return Y ^ (X | ~Z);
    11ab:	44 09 c8             	or     %r9d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    11ae:	42 8d 8c 31 14 43 01 	lea    -0x5cfebcec(%rcx,%r14,1),%ecx
    11b5:	a3 
    11b6:	44 8b 74 24 44       	mov    0x44(%rsp),%r14d
   return Y ^ (X | ~Z);
    11bb:	31 d0                	xor    %edx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    11bd:	8d 94 2a 82 7e 53 f7 	lea    -0x8ac817e(%rdx,%rbp,1),%edx
    11c4:	01 c1                	add    %eax,%ecx
   return Y ^ (X | ~Z);
    11c6:	44 89 f8             	mov    %r15d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    11c9:	c1 c1 0f             	rol    $0xf,%ecx
    11cc:	42 8d bc 37 a1 11 08 	lea    0x4e0811a1(%rdi,%r14,1),%edi
    11d3:	4e 
   return Y ^ (X | ~Z);
    11d4:	45 89 ce             	mov    %r9d,%r14d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    11d7:	44 01 c9             	add    %r9d,%ecx
   return Y ^ (X | ~Z);
    11da:	41 f7 d6             	not    %r14d
    11dd:	09 c8                	or     %ecx,%eax
    11df:	44 31 c8             	xor    %r9d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    11e2:	01 c7                	add    %eax,%edi
   return Y ^ (X | ~Z);
    11e4:	44 89 f0             	mov    %r14d,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    11e7:	44 8b 74 24 3c       	mov    0x3c(%rsp),%r14d
    11ec:	c1 cf 0b             	ror    $0xb,%edi
    11ef:	01 cf                	add    %ecx,%edi
   return Y ^ (X | ~Z);
    11f1:	09 f8                	or     %edi,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    11f3:	47 8d 8c 31 35 f2 3a 	lea    -0x42c50dcb(%r9,%r14,1),%r9d
    11fa:	bd 
    11fb:	44 8b 74 24 18       	mov    0x18(%rsp),%r14d
   return Y ^ (X | ~Z);
    1200:	31 c8                	xor    %ecx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1202:	01 d0                	add    %edx,%eax
   return Y ^ (X | ~Z);
    1204:	89 ca                	mov    %ecx,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1206:	c1 c0 06             	rol    $0x6,%eax
   return Y ^ (X | ~Z);
    1209:	f7 d2                	not    %edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    120b:	42 8d 8c 31 bb d2 d7 	lea    0x2ad7d2bb(%rcx,%r14,1),%ecx
    1212:	2a 
    1213:	01 f8                	add    %edi,%eax
   return Y ^ (X | ~Z);
    1215:	09 c2                	or     %eax,%edx
    1217:	31 fa                	xor    %edi,%edx
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1219:	44 01 ca             	add    %r9d,%edx
   return Y ^ (X | ~Z);
    121c:	41 89 f9             	mov    %edi,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    121f:	c1 c2 0a             	rol    $0xa,%edx
   return Y ^ (X | ~Z);
    1222:	41 f7 d1             	not    %r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1225:	01 c2                	add    %eax,%edx
   return Y ^ (X | ~Z);
    1227:	41 09 d1             	or     %edx,%r9d
    122a:	41 31 c1             	xor    %eax,%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    122d:	44 01 c9             	add    %r9d,%ecx
   Round4(&A,B,C,D, 12, 6, 52); Round4(&D,A,B,C,  3, 10, 53); Round4(&C,D,A,B, 10, 15, 54); Round4(&B,C,D,A,  1, 21, 55);
   Round4(&A,B,C,D,  8, 6, 56); Round4(&D,A,B,C, 15, 10, 57); Round4(&C,D,A,B,  6, 15, 58); Round4(&B,C,D,A, 13, 21, 59);
   Round4(&A,B,C,D,  4, 6, 60); Round4(&D,A,B,C, 11, 10, 61); Round4(&C,D,A,B,  2, 15, 62); Round4(&B,C,D,A,  9, 21, 63);

   // Then perform the following additions. (let's add)
   *A2 = A + AA;
    1230:	44 8b 0e             	mov    (%rsi),%r9d
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1233:	c1 c1 0f             	rol    $0xf,%ecx
    1236:	01 d1                	add    %edx,%ecx
   *A2 = A + AA;
    1238:	41 01 c1             	add    %eax,%r9d
   return Y ^ (X | ~Z);
    123b:	f7 d0                	not    %eax
   *A2 = A + AA;
    123d:	44 89 0e             	mov    %r9d,(%rsi)
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1240:	8b 74 24 34          	mov    0x34(%rsp),%esi
   return Y ^ (X | ~Z);
    1244:	09 c8                	or     %ecx,%eax
    1246:	31 d0                	xor    %edx,%eax
   return b + ROTATE_LEFT(a + FGHI + pX[k] + T[i], s);
    1248:	8d b4 37 91 d3 86 eb 	lea    -0x14792c6f(%rdi,%rsi,1),%esi
    124f:	01 f0                	add    %esi,%eax
    1251:	c1 c8 0b             	ror    $0xb,%eax
   *B2 = B + BB;
    1254:	41 01 ca             	add    %ecx,%r10d
   *C2 = C + CC;
    1257:	41 01 cb             	add    %ecx,%r11d
   *B2 = B + BB;
    125a:	41 01 c2             	add    %eax,%r10d
    125d:	48 8b 04 24          	mov    (%rsp),%rax
   *C2 = C + CC;
    1261:	48 8b 4c 24 08       	mov    0x8(%rsp),%rcx
   *D2 = D + DD;
    1266:	01 d3                	add    %edx,%ebx
   *B2 = B + BB;
    1268:	44 89 10             	mov    %r10d,(%rax)
   *C2 = C + CC;
    126b:	44 89 19             	mov    %r11d,(%rcx)

   //The clearance of confidential information
   memset(pX, 0, sizeof(X));
}
    126e:	48 8b 44 24 58       	mov    0x58(%rsp),%rax
    1273:	64 48 33 04 25 28 00 	xor    %fs:0x28,%rax
    127a:	00 00 
   *D2 = D + DD;
    127c:	41 89 18             	mov    %ebx,(%r8)
}
    127f:	75 0f                	jne    1290 <MD5_Round_Calculate+0xa10>
    1281:	48 83 c4 68          	add    $0x68,%rsp
    1285:	5b                   	pop    %rbx
    1286:	5d                   	pop    %rbp
    1287:	41 5c                	pop    %r12
    1289:	41 5d                	pop    %r13
    128b:	41 5e                	pop    %r14
    128d:	41 5f                	pop    %r15
    128f:	c3                   	retq   
    1290:	e8 cb f3 ff ff       	callq  660 <__stack_chk_fail@plt>
    1295:	90                   	nop
    1296:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
    129d:	00 00 00 

00000000000012a0 <MD5_Binary>:

void MD5_Binary(const char * string, unsigned char * output)
{
    12a0:	41 57                	push   %r15
    12a2:	41 56                	push   %r14
    12a4:	49 89 f7             	mov    %rsi,%r15
    12a7:	41 55                	push   %r13
    12a9:	41 54                	push   %r12
    12ab:	55                   	push   %rbp
    12ac:	53                   	push   %rbx
    12ad:	48 89 fb             	mov    %rdi,%rbx
    12b0:	48 83 ec 78          	sub    $0x78,%rsp
                *D = &msg_digest[3];
	int i;

//prog
   //Step 3.Initialize MD Buffer (although it is the initialization; step 3 of A, B, C, and D -- unavoidable -- a head)
   *A = 0x67452301;
    12b4:	66 0f 6f 05 b4 03 00 	movdqa 0x3b4(%rip),%xmm0        # 1670 <_IO_stdin_used+0x80>
    12bb:	00 
    12bc:	48 8d 6c 24 10       	lea    0x10(%rsp),%rbp
{
    12c1:	64 48 8b 04 25 28 00 	mov    %fs:0x28,%rax
    12c8:	00 00 
    12ca:	48 89 44 24 68       	mov    %rax,0x68(%rsp)
    12cf:	31 c0                	xor    %eax,%eax
   *A = 0x67452301;
    12d1:	0f 29 44 24 10       	movaps %xmm0,0x10(%rsp)
   *C = 0x98badcfe;
   *D = 0x10325476;

   //Step 1.Append Padding Bits (extension of a mark bit)
   //1-1
   string_byte_len = (unsigned int)strlen(string);    //The byte chief of a character sequence is acquired.
    12d6:	e8 75 f3 ff ff       	callq  650 <strlen@plt>
   pstring = (unsigned char *)string; //The position of the present character sequence is set.

   //1-2  Repeat calculation until length becomes less than 64 bytes.
   for (i=string_byte_len; 64<=i; i-=64,pstring+=64)
    12db:	83 f8 3f             	cmp    $0x3f,%eax
   string_byte_len = (unsigned int)strlen(string);    //The byte chief of a character sequence is acquired.
    12de:	49 89 c6             	mov    %rax,%r14
   for (i=string_byte_len; 64<=i; i-=64,pstring+=64)
    12e1:	7e 3c                	jle    131f <MD5_Binary+0x7f>
    12e3:	44 8d 60 c0          	lea    -0x40(%rax),%r12d
    12e7:	48 8d 6c 24 10       	lea    0x10(%rsp),%rbp
    12ec:	41 c1 ec 06          	shr    $0x6,%r12d
        MD5_Round_Calculate(pstring, A,B,C,D);
    12f0:	4c 8d 6d 0c          	lea    0xc(%rbp),%r13
    12f4:	49 83 c4 01          	add    $0x1,%r12
    12f8:	49 c1 e4 06          	shl    $0x6,%r12
    12fc:	49 01 dc             	add    %rbx,%r12
    12ff:	90                   	nop
    1300:	48 8d 4d 08          	lea    0x8(%rbp),%rcx
    1304:	48 8d 55 04          	lea    0x4(%rbp),%rdx
    1308:	48 89 df             	mov    %rbx,%rdi
    130b:	4d 89 e8             	mov    %r13,%r8
    130e:	48 89 ee             	mov    %rbp,%rsi
   for (i=string_byte_len; 64<=i; i-=64,pstring+=64)
    1311:	48 83 c3 40          	add    $0x40,%rbx
        MD5_Round_Calculate(pstring, A,B,C,D);
    1315:	e8 66 f5 ff ff       	callq  880 <MD5_Round_Calculate>
   for (i=string_byte_len; 64<=i; i-=64,pstring+=64)
    131a:	4c 39 e3             	cmp    %r12,%rbx
    131d:	75 e1                	jne    1300 <MD5_Binary+0x60>

   //1-3
   copy_len = string_byte_len % 64;                               //The number of bytes which remained is computed.
    131f:	45 89 f0             	mov    %r14d,%r8d

__fortify_function char *
__NTH (strncpy (char *__restrict __dest, const char *__restrict __src,
		size_t __len))
{
  return __builtin___strncpy_chk (__dest, __src, __len, __bos (__dest));
    1322:	4c 8d 6c 24 20       	lea    0x20(%rsp),%r13
    1327:	b9 40 00 00 00       	mov    $0x40,%ecx
    132c:	41 83 e0 3f          	and    $0x3f,%r8d
    1330:	48 89 de             	mov    %rbx,%rsi
   strncpy((char *)padding_message, (char *)pstring, copy_len); //A message is copied to an extended bit sequence.
    1333:	45 89 c4             	mov    %r8d,%r12d
    1336:	4c 89 ef             	mov    %r13,%rdi
    1339:	44 89 44 24 0c       	mov    %r8d,0xc(%rsp)
    133e:	4c 89 e2             	mov    %r12,%rdx
    1341:	e8 2a f3 ff ff       	callq  670 <__strncpy_chk@plt>
   memset(padding_message+copy_len, 0, 64 - copy_len);           //It buries by 0 until it becomes extended bit length.
    1346:	44 8b 44 24 0c       	mov    0xc(%rsp),%r8d
    134b:	b8 40 00 00 00       	mov    $0x40,%eax
  return __builtin___memset_chk (__dest, __ch, __len, __bos0 (__dest));
    1350:	31 f6                	xor    %esi,%esi
    1352:	4b 8d 4c 25 00       	lea    0x0(%r13,%r12,1),%rcx
    1357:	44 29 c0             	sub    %r8d,%eax
    135a:	83 f8 08             	cmp    $0x8,%eax
    135d:	0f 83 8d 00 00 00    	jae    13f0 <MD5_Binary+0x150>
    1363:	a8 04                	test   $0x4,%al
    1365:	0f 85 12 01 00 00    	jne    147d <MD5_Binary+0x1dd>
    136b:	85 c0                	test   %eax,%eax
    136d:	74 0b                	je     137a <MD5_Binary+0xda>
    136f:	a8 02                	test   $0x2,%al
    1371:	c6 01 00             	movb   $0x0,(%rcx)
    1374:	0f 85 18 01 00 00    	jne    1492 <MD5_Binary+0x1f2>
   padding_message[copy_len] |= 0x80;                            //The next of a message is 1.
    137a:	42 80 4c 24 20 80    	orb    $0x80,0x20(%rsp,%r12,1)

   //1-4
   //If 56 bytes or more (less than 64 bytes) of remainder becomes, it will calculate by extending to 64 bytes.
   if (56 <= copy_len) {
    1380:	41 83 f8 37          	cmp    $0x37,%r8d
    1384:	0f 87 ae 00 00 00    	ja     1438 <MD5_Binary+0x198>
       MD5_Round_Calculate(padding_message, A,B,C,D);
       memset(padding_message, 0, 56); //56 bytes is newly fill uped with 0.
   }

   //Step 2.Append Length (the information on length is added)
   string_bit_len = string_byte_len * 8;             //From the byte chief to bit length (32 bytes of low rank)
    138a:	42 8d 04 f5 00 00 00 	lea    0x0(,%r14,8),%eax
    1391:	00 
   memcpy(&padding_message[56], &string_bit_len, 4); //32 bytes of low rank is set.

   //When bit length cannot be expressed in 32 bytes of low rank, it is a beam raising to a higher rank.
  if (UINT_MAX / 8 < string_byte_len) {
    1392:	41 81 fe ff ff ff 1f 	cmp    $0x1fffffff,%r14d
    1399:	89 44 24 58          	mov    %eax,0x58(%rsp)
    139d:	0f 86 cd 00 00 00    	jbe    1470 <MD5_Binary+0x1d0>
      unsigned int high = (string_byte_len - UINT_MAX / 8) * 8;
    13a3:	83 c0 08             	add    $0x8,%eax
    13a6:	89 44 24 5c          	mov    %eax,0x5c(%rsp)
      memcpy(&padding_message[60], &high, 4);
  } else
      memset(&padding_message[60], 0, 4); //In this case, it is good for a higher rank at 0.

   //Step 4.Process Message in 16-Word Blocks (calculation of MD5)
   MD5_Round_Calculate(padding_message, A,B,C,D);
    13aa:	48 8d 4d 08          	lea    0x8(%rbp),%rcx
    13ae:	48 8d 55 04          	lea    0x4(%rbp),%rdx
    13b2:	4c 8d 45 0c          	lea    0xc(%rbp),%r8
    13b6:	48 89 ee             	mov    %rbp,%rsi
    13b9:	4c 89 ef             	mov    %r13,%rdi
    13bc:	e8 bf f4 ff ff       	callq  880 <MD5_Round_Calculate>
  return __builtin___memcpy_chk (__dest, __src, __len, __bos0 (__dest));
    13c1:	66 0f 6f 44 24 10    	movdqa 0x10(%rsp),%xmm0

   //Step 5.Output (output)
   memcpy(output,msg_digest,16);
}
    13c7:	48 8b 44 24 68       	mov    0x68(%rsp),%rax
    13cc:	64 48 33 04 25 28 00 	xor    %fs:0x28,%rax
    13d3:	00 00 
    13d5:	41 0f 11 07          	movups %xmm0,(%r15)
    13d9:	0f 85 c1 00 00 00    	jne    14a0 <MD5_Binary+0x200>
    13df:	48 83 c4 78          	add    $0x78,%rsp
    13e3:	5b                   	pop    %rbx
    13e4:	5d                   	pop    %rbp
    13e5:	41 5c                	pop    %r12
    13e7:	41 5d                	pop    %r13
    13e9:	41 5e                	pop    %r14
    13eb:	41 5f                	pop    %r15
    13ed:	c3                   	retq   
    13ee:	66 90                	xchg   %ax,%ax
  return __builtin___memset_chk (__dest, __ch, __len, __bos0 (__dest));
    13f0:	89 c2                	mov    %eax,%edx
    13f2:	48 c7 01 00 00 00 00 	movq   $0x0,(%rcx)
    13f9:	48 c7 44 11 f8 00 00 	movq   $0x0,-0x8(%rcx,%rdx,1)
    1400:	00 00 
    1402:	48 8d 51 08          	lea    0x8(%rcx),%rdx
    1406:	48 83 e2 f8          	and    $0xfffffffffffffff8,%rdx
    140a:	48 29 d1             	sub    %rdx,%rcx
    140d:	01 c8                	add    %ecx,%eax
    140f:	83 e0 f8             	and    $0xfffffff8,%eax
    1412:	83 f8 08             	cmp    $0x8,%eax
    1415:	0f 82 5f ff ff ff    	jb     137a <MD5_Binary+0xda>
    141b:	83 e0 f8             	and    $0xfffffff8,%eax
    141e:	31 c9                	xor    %ecx,%ecx
    1420:	89 cf                	mov    %ecx,%edi
    1422:	83 c1 08             	add    $0x8,%ecx
    1425:	39 c1                	cmp    %eax,%ecx
    1427:	48 89 34 3a          	mov    %rsi,(%rdx,%rdi,1)
    142b:	72 f3                	jb     1420 <MD5_Binary+0x180>
    142d:	e9 48 ff ff ff       	jmpq   137a <MD5_Binary+0xda>
    1432:	66 0f 1f 44 00 00    	nopw   0x0(%rax,%rax,1)
       MD5_Round_Calculate(padding_message, A,B,C,D);
    1438:	48 8d 4d 08          	lea    0x8(%rbp),%rcx
    143c:	48 8d 55 04          	lea    0x4(%rbp),%rdx
    1440:	4c 8d 45 0c          	lea    0xc(%rbp),%r8
    1444:	48 89 ee             	mov    %rbp,%rsi
    1447:	4c 89 ef             	mov    %r13,%rdi
    144a:	e8 31 f4 ff ff       	callq  880 <MD5_Round_Calculate>
    144f:	66 0f ef c0          	pxor   %xmm0,%xmm0
    1453:	48 c7 44 24 50 00 00 	movq   $0x0,0x50(%rsp)
    145a:	00 00 
    145c:	0f 29 44 24 20       	movaps %xmm0,0x20(%rsp)
    1461:	0f 29 44 24 30       	movaps %xmm0,0x30(%rsp)
    1466:	0f 29 44 24 40       	movaps %xmm0,0x40(%rsp)
    146b:	e9 1a ff ff ff       	jmpq   138a <MD5_Binary+0xea>
    1470:	c7 44 24 5c 00 00 00 	movl   $0x0,0x5c(%rsp)
    1477:	00 
    1478:	e9 2d ff ff ff       	jmpq   13aa <MD5_Binary+0x10a>
    147d:	89 c2                	mov    %eax,%edx
    147f:	c7 01 00 00 00 00    	movl   $0x0,(%rcx)
    1485:	c7 44 11 fc 00 00 00 	movl   $0x0,-0x4(%rcx,%rdx,1)
    148c:	00 
    148d:	e9 e8 fe ff ff       	jmpq   137a <MD5_Binary+0xda>
    1492:	89 c2                	mov    %eax,%edx
    1494:	31 c0                	xor    %eax,%eax
    1496:	66 89 44 11 fe       	mov    %ax,-0x2(%rcx,%rdx,1)
    149b:	e9 da fe ff ff       	jmpq   137a <MD5_Binary+0xda>
}
    14a0:	e8 bb f1 ff ff       	callq  660 <__stack_chk_fail@plt>
    14a5:	90                   	nop
    14a6:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
    14ad:	00 00 00 

00000000000014b0 <MD5_String>:
//-------------------------------------------------------------------
// The function for the exteriors


void MD5_String(const char * string, char * output)
{
    14b0:	53                   	push   %rbx
    14b1:	48 89 f3             	mov    %rsi,%rbx
    14b4:	48 83 ec 20          	sub    $0x20,%rsp
	unsigned char digest[16];

	MD5_Binary(string,digest);
    14b8:	48 89 e6             	mov    %rsp,%rsi
{
    14bb:	64 48 8b 04 25 28 00 	mov    %fs:0x28,%rax
    14c2:	00 00 
    14c4:	48 89 44 24 18       	mov    %rax,0x18(%rsp)
    14c9:	31 c0                	xor    %eax,%eax
	MD5_Binary(string,digest);
    14cb:	e8 d0 fd ff ff       	callq  12a0 <MD5_Binary>
	sprintf(output,	"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
    14d0:	0f b6 44 24 0f       	movzbl 0xf(%rsp),%eax
  return __builtin___sprintf_chk (__s, __USE_FORTIFY_LEVEL - 1,
    14d5:	48 8d 0d 34 01 00 00 	lea    0x134(%rip),%rcx        # 1610 <_IO_stdin_used+0x20>
    14dc:	48 c7 c2 ff ff ff ff 	mov    $0xffffffffffffffff,%rdx
    14e3:	be 01 00 00 00       	mov    $0x1,%esi
    14e8:	48 89 df             	mov    %rbx,%rdi
    14eb:	50                   	push   %rax
    14ec:	0f b6 44 24 16       	movzbl 0x16(%rsp),%eax
    14f1:	50                   	push   %rax
    14f2:	0f b6 44 24 1d       	movzbl 0x1d(%rsp),%eax
    14f7:	50                   	push   %rax
    14f8:	0f b6 44 24 24       	movzbl 0x24(%rsp),%eax
    14fd:	50                   	push   %rax
    14fe:	0f b6 44 24 2b       	movzbl 0x2b(%rsp),%eax
    1503:	50                   	push   %rax
    1504:	0f b6 44 24 32       	movzbl 0x32(%rsp),%eax
    1509:	50                   	push   %rax
    150a:	0f b6 44 24 39       	movzbl 0x39(%rsp),%eax
    150f:	50                   	push   %rax
    1510:	0f b6 44 24 40       	movzbl 0x40(%rsp),%eax
    1515:	50                   	push   %rax
    1516:	0f b6 44 24 47       	movzbl 0x47(%rsp),%eax
    151b:	50                   	push   %rax
    151c:	0f b6 44 24 4e       	movzbl 0x4e(%rsp),%eax
    1521:	50                   	push   %rax
    1522:	0f b6 44 24 55       	movzbl 0x55(%rsp),%eax
    1527:	50                   	push   %rax
    1528:	0f b6 44 24 5c       	movzbl 0x5c(%rsp),%eax
    152d:	50                   	push   %rax
    152e:	0f b6 44 24 63       	movzbl 0x63(%rsp),%eax
    1533:	50                   	push   %rax
    1534:	0f b6 44 24 6a       	movzbl 0x6a(%rsp),%eax
    1539:	50                   	push   %rax
    153a:	44 0f b6 4c 24 71    	movzbl 0x71(%rsp),%r9d
    1540:	31 c0                	xor    %eax,%eax
    1542:	44 0f b6 44 24 70    	movzbl 0x70(%rsp),%r8d
    1548:	e8 43 f1 ff ff       	callq  690 <__sprintf_chk@plt>
		digest[ 0], digest[ 1], digest[ 2], digest[ 3],
		digest[ 4], digest[ 5], digest[ 6], digest[ 7],
		digest[ 8], digest[ 9], digest[10], digest[11],
		digest[12], digest[13], digest[14], digest[15]);
}
    154d:	48 83 c4 70          	add    $0x70,%rsp
    1551:	48 8b 44 24 18       	mov    0x18(%rsp),%rax
    1556:	64 48 33 04 25 28 00 	xor    %fs:0x28,%rax
    155d:	00 00 
    155f:	75 06                	jne    1567 <MD5_String+0xb7>
    1561:	48 83 c4 20          	add    $0x20,%rsp
    1565:	5b                   	pop    %rbx
    1566:	c3                   	retq   
    1567:	e8 f4 f0 ff ff       	callq  660 <__stack_chk_fail@plt>
    156c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000001570 <__libc_csu_init>:
    1570:	41 57                	push   %r15
    1572:	41 56                	push   %r14
    1574:	49 89 d7             	mov    %rdx,%r15
    1577:	41 55                	push   %r13
    1579:	41 54                	push   %r12
    157b:	4c 8d 25 16 08 20 00 	lea    0x200816(%rip),%r12        # 201d98 <__frame_dummy_init_array_entry>
    1582:	55                   	push   %rbp
    1583:	48 8d 2d 16 08 20 00 	lea    0x200816(%rip),%rbp        # 201da0 <__init_array_end>
    158a:	53                   	push   %rbx
    158b:	41 89 fd             	mov    %edi,%r13d
    158e:	49 89 f6             	mov    %rsi,%r14
    1591:	4c 29 e5             	sub    %r12,%rbp
    1594:	48 83 ec 08          	sub    $0x8,%rsp
    1598:	48 c1 fd 03          	sar    $0x3,%rbp
    159c:	e8 87 f0 ff ff       	callq  628 <_init>
    15a1:	48 85 ed             	test   %rbp,%rbp
    15a4:	74 20                	je     15c6 <__libc_csu_init+0x56>
    15a6:	31 db                	xor    %ebx,%ebx
    15a8:	0f 1f 84 00 00 00 00 	nopl   0x0(%rax,%rax,1)
    15af:	00 
    15b0:	4c 89 fa             	mov    %r15,%rdx
    15b3:	4c 89 f6             	mov    %r14,%rsi
    15b6:	44 89 ef             	mov    %r13d,%edi
    15b9:	41 ff 14 dc          	callq  *(%r12,%rbx,8)
    15bd:	48 83 c3 01          	add    $0x1,%rbx
    15c1:	48 39 dd             	cmp    %rbx,%rbp
    15c4:	75 ea                	jne    15b0 <__libc_csu_init+0x40>
    15c6:	48 83 c4 08          	add    $0x8,%rsp
    15ca:	5b                   	pop    %rbx
    15cb:	5d                   	pop    %rbp
    15cc:	41 5c                	pop    %r12
    15ce:	41 5d                	pop    %r13
    15d0:	41 5e                	pop    %r14
    15d2:	41 5f                	pop    %r15
    15d4:	c3                   	retq   
    15d5:	90                   	nop
    15d6:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
    15dd:	00 00 00 

00000000000015e0 <__libc_csu_fini>:
    15e0:	f3 c3                	repz retq 

Disassembly of section .fini:

00000000000015e4 <_fini>:
    15e4:	48 83 ec 08          	sub    $0x8,%rsp
    15e8:	48 83 c4 08          	add    $0x8,%rsp
    15ec:	c3                   	retq   
