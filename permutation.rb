## 全排列
# 0 -n 的全排列, 深度遍历, 广度遍历, 递归
# 状态压缩

# 生成[0,n]
# 每次迭代从当前集合中选择一个元素, 之后的集合可以作为一个子问题递归求解
# 依次从左到有选择
# 当集合为空时,递归结束

## 生成集合

def gen_set(n)
  ret = []
  n.times { |i|
    ret << i
  }
  return ret
end

def full_permutation(a, ret=[])
  ##打印全排列
  a.empty? and puts ret.to_s and return
  a.each {|e|
    a1 = a.dup
    ret1 = ret.dup
    ret1 << a1.delete(e)
    full_permutation(a1, ret1)
  }
end

## 全排列生成算法
#1. 插入生成, 从1, 开始, 每次都把一个新的元素插入到之前全排列的缝隙处
#2. 字典序生成算法, 从1234 生成到4321

def insert_full_permutation(a)
  # 需要为何当前全排列集合
  set = [[]]
  #puts a.to_s
  a.each { |e|
    ##插入当前全排列集合,生成新的全排列集合
    nextSet=[]
    set.each { |pl|
      ## 在pl的不同位置插入新元素e,生成新序列
      (0.. pl.length).each{|pos|
        n = pl.dup
        n.insert(pos, e)
        #puts "new pos:#{pos} #{n.to_s}"
        nextSet << n
      }
      set = nextSet
    }
  }
  return set
end

#puts insert_full_permutation(gen_set(4)).to_s

## 字典序生成算法
def dict_permutaion(a)
  ##字典序算法基于这样一个逻辑, 每一个排列都有大小,排列按照大小依次生成,下一个排列
  #是大于当前排列的最小的一个,  比如 123 的下一个排列是132, 各位按照字母序,左边为高位
  #1234 ... 4321 会发现生成从逆序数为0,到最后的最大逆序数
  a.length < 2 and return a
  
  #1. 首先增序sort,从最小开始
  a.sort!
  puts a.to_s

  # 1234  1243 [13|42 逆值42 1324] 
  #2 从后往前找第一个相邻[n-1]<[n]的值, 
  #  解释: 因为生成的过程是从递增排列到递减排列的(造反)过程,所以第一个[n-1] < [n]的位置就是还没有
  #涉及到的位置
  #3. 寻找n-1 后面的比n-1大的最小的数字替换n-1, 再对n-1 后面的序列排序,
  #   上面的过程等价于, 交换n-1 和 min 元素, 然后逆序(n..-1)所有元素.
  #   解释: n-1是比min小的元素,所以交换后, (n..-1)还是一个严格递减序列, 翻转操作等价于sort操作
  #4. 重复2,3 直到没有没有[n-1] < [n] 存在,说明所有排列都生成
  loop {
    isFind=false
    (a.length-1).downto(1).each { |pos|
      #find
      if a[pos-1] < a[pos]
        #找比pos-1 大的最小的数字,由于pos..-1已经是递减序列了,所以从后往前找
        (a.length-1).downto(1).each {|j|
          if a[j] > a[pos-1] 
            a[pos-1], a[j]  = a[j], a[pos-1] 
            break
          end
        }
        #reverse
        a[pos..-1] = a[pos..-1].reverse
        puts a.to_s
        isFind=true
        break
      end
    }
    !isFind and break
  } 
end


dict_permutaion(gen_set(4))

## r 排列生成算法 r <= length
#在N个元素中取出r个元素的排列情况
#分为两部1. 去除r个元素的组合
#2. 对每种r元素进行全排列



