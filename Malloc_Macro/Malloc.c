
#include <stdio.h>
#include <stdlib.h>

#define EXIT_FAILURE 1

//
#define MALLOC(p,t) \
	if( (p=(t *)malloc(sizeof(t))) == (t *)NULL) \
	{\
		fprintf(stderr, "Insufficient memory") ; \
		exit(EXIT_FAILURE) ; \
	}	
//
#define FREE(x)  free(x);  x = NULL  

int main()
{
	int i, *pi ;
	float f, *pf ;

/////////////
	MALLOC(pi, int)
	MALLOC(pf, float) 

	*pi=1024 ;
	*pf=3.14f ;

	i=*pi ;
	f=*pf ;

///////
	FREE(pi) ;
	FREE(pf) ;
	
/*
	free(pi) ;
	pi = NULL ;
	free(pf) ;
	pf = NULL ;
*/
///////	
	printf("i equal %d, f equal %f", i, f) ;

	return 0 ;
}
