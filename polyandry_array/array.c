
////1 数组名和对应指针的转换

#include <stdio.h>

/*
////////////////////第一部分/////////////////////////////////////
//实验数组名被改写成一个指针参数“ 并不是递归定义的”

//接收数组名的函数

void come1(int arr[2][3][4])//最左一维可以不写 如arr[][3][4]
{
		printf("\nThe Functon para is : int arr[2][3][4]\n") ;
		printf("The arr adress is : %p\n", arr) ;
		printf("The arr[1] is        : %p    \nThe *(arr +1) is         : %p | The *(arr + 0) is          : %p\n", arr[1], *(arr + 1), *(arr + 0)) ;
		printf("The arr[1][1] is     : %p    \nThe *(*(arr + 1) +1) is  : %p | The *(*(arr + 1)+0) is     : %p\n", arr[1][1], *(*(arr + 1) +1), *(*(arr + 1) +0)) ;
		printf("Tha &arr[1][1][1] is : %p    \nThe (*(*(arr+1)+1)+1) is : %p | The (*(*(arr + 1) +1)+0)is : %p\n", &arr[1][1][1], (*(*(arr+1)+1)+1), (*(*(arr + 1) +1)+0)) ;
		
		///////经过输出证明 以源数组形式声明的形参在各个维度上有正确的能力（即单位宽度不一样）
} 
//void come3( int (**arr)[4])
//{}
void come4 (int arr[2][3][4]) //arr的实质只是一个指针
{
		arr = NULL ; //指针可赋值
}

void come2(int (*arr)[3][4])//数组名转成 #数组的指针# 一回只能蜕变一个维度（从左边开始）
{                           // 注意要加括号 如不加为int *arr[3][4] 为 #指针数组#


		//int (**a)[4] != arr 
		printf("\nThe Functon para is : int (*arr)[3][4]\n") ;
		printf("The arr adress is : %p\n", arr) ;
		printf("The arr[1] is        : %p    \nThe *(arr +1) is         : %p | The *(arr + 0) is          : %p\n", arr[1], *(arr + 1), *(arr + 0)) ;
		printf("The arr[1][1] is     : %p    \nThe *(*(arr + 1) +1) is  : %p | The *(*(arr + 1)+0) is     : %p\n", arr[1][1], *(*(arr + 1) +1), *(*(arr + 1) +0)) ;
		printf("Tha &arr[1][1][1] is : %p    \nThe (*(*(arr+1)+1)+1) is : %p | The (*(*(arr + 1) +1)+0)is : %p\n", &arr[1][1][1], (*(*(arr+1)+1)+1), (*(*(arr + 1) +1)+0)) ;

		////////////经过输出证明 以（蜕最左一维 的）数组的指针声明的形参在各个维度上有正确的能力（即单位宽度不一样）

		///下面是检验数组名在嵌套函数调用时是否可以继续蜕变
		// 如 int (*arr)[3][4]  作为参数 在调用函数时是否会在蜕变成 int(**a)[4]

		//重 ！！！！！！！！
		//结论： 不行
		//因为： 当函数蜕变一次时他就变成的一个指针（数组指针） ，显然指针无法蜕变。
		//所以： 数组当作为参数传递时只会蜕变一次， 
		
		//come3(arr) ;

		////////////////////可以执行
		//说明！！！！：一个函数的数组形参 并不是一个数组 而是该数组的蜕变指针
		come4(arr) ;
}


int main()
{
		int arr[2][3][4] = { 
							//[0][...][...]
							111, 112, 113, 114,
							121, 122, 123, 124,
							131, 132, 133, 134,

							//[1][...][...]
							211, 212, 213, 214,
							221, 222, 223, 224,
							231, 232, 233, 234,
		} ;

		come1(arr) ;
		come2(arr) ;
		


		
		return 0 ;		             

}
*/

//////////////////////////第二部分/////////////////////////////
//有关char* 的多维数组

void come1(char *arr[4], int count)//count 记录最左边一维的大小
{
		printf("\nThe Function para is : char *arr[4] \n") ;
		printf("The arr adress is : %p\n", arr ) ;
		printf("&arr[0] is : %p | arr[0] is : %p | chars is : %s\n", &arr[0], arr[0], arr[0]) ;
		printf("&arr[1] is : %p | arr[1] is : %p | chars is : %s\n", &arr[1], arr[1], arr[1]) ;
		printf("&arr[2] is : %p | arr[2] is : %p | chars is : %s\n", &arr[2], arr[2], arr[2]) ;
		printf("&arr[3] is : %p | arr[3] is : %p | chars is : %s\n", &arr[3], arr[3], arr[3]) ;
		//注意arr[..] 和 &arr[..] 的不同 
		// &arr[...]是数组每个元素的   地址
		//而 arr[...] 是数组每个元素的值 而这个值又是 指向一个字符串的指针（即他本身又是一个地址）
		
		
		printf("The &arr[1] is : %p     \n The (arr+1) is     : %p  | The (arr+0) is     : %p\n",
				&arr[1], (arr+1), (arr+0) ) ;
		printf("The &arr[1][1] is : %p \n The (*(arr+1)+1) is : %p  | The (*(arr+1)+0) is : %p\n",
				&arr[1][1], (*(arr+1)+1), (*(arr+1)+0) );
}

int main()
{
		char *arr[4] = {
						"112345",
						"212345",
						"312345",
						"312345"		};//////////////由于char*的性质以他元素类型的数组 隐式 的增加一维！
	
		come1(arr, 4) ;

		//char a[4][25]蜕变为 char (*a)[25]
		//显然 char (*a)[25]  != arr ;

						return 0 ;
}