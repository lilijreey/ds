#ifndef HUFFMAN_TREE_H_
#define FUFFMAN_TREE_H_

#define LIST_INIT_SIZE 20 //头结点线性表的初始大小
#define LISTINCREMENT 20  //线性表的增量
#define EOT 4
#define HUFF_BUFFER 256 

typedef unsigned char Elem ;//节点
typedef char * HuffmanCode ; //基于字符串

enum {left = 48, right} ; //0,1 ASCII
////////////////////////////////////////////////
typedef struct BinTreeNode  //二叉树节点定义					
{
		Elem elem ;
		int right ;
		struct BinTreeNode *lchild ;  //left child
		struct BinTreeNode *rchild ;  //right child
		struct BinTreeNode *parent ;
		//为了构造方便
		struct BinTreeNode *per ;
		struct BinTreeNode *next ;
}BinTNode, *pBinTNode ;

///////////////////////////////////////////////
typedef struct ListNode
{
		Elem elem ;
		int right ; //权
		HuffmanCode codes; //编码头结点
}LiNode, *pLiNode ;

typedef struct List
{
		int lenght ;//节点个数
		int size ;
		pLiNode node ;
}Li;


//////////////LIST OPERATE//////////////////////////////

//初始化List
void Init_List(Li *l) ;

//输入节点的值和权
void Input_List(Li *l) ;//HeadNode

void File_Input_List(const char *codName, Li *l) ;
int Find_Elem_In_List(Li *l, Elem elem) ;
//对节点排序（升序）
void Sequence_List(Li *l) ; 

//SHow
void Show_List(Li *l) ;

//包括（Fileinput, Sequence) ;
void File_Create_List(const char *codName, Li *l)  ;
void BinFile_Input_List(const char *codName, Li *l) ; //二进制文件
void Bin_Coding_By_Char(char *inName, Li *l) ;
//
void Get_Coding_File_Name(char *codingName) ;
int Get_Decoding_File_Name(char *codName)  ; //合法文件返回1
/////////////////CREATE_HUFFMAN_TREE////////////////////////////////////////
//构造dLink1 
void Link_Copy_List(Li *l, pBinTNode per) ;//&head Node

void Create_Huffman_Tree(pBinTNode per) ;

//改造Huff_Tree生成哨兵节点
void Set_Sent(pBinTNode root) ;
//找左右节点
void Get_LR_Subtree(pBinTNode per, pBinTNode *lNode, pBinTNode *rNode) ;

void Delete_RL_Add_Par(pBinTNode head, pBinTNode l, pBinTNode r, pBinTNode par) ;

//
void Show_Get_Huffman_Code_Interface(pBinTNode head) ;//give root node

//递归遍历生成编码
void Show_Get_Huffman_Code(pBinTNode p, char *buff, int *i) ;

//buffer 版本
void Buff_Get_Huffman_Code_Interface(pBinTNode head, Li *l) ;

void Buff_Get_Huffman_Code(pBinTNode p, char *buff, int *i, Li *l) ;

char * Get_List_Code(Li *l, Elem elem, int lenght) ;

//销毁哈弗曼树
void Destroy_Huffman_Tree(pBinTNode bNode) ;

////////////Coding//////////////////////////////////////////////////
//生成中间版本
void Coding_By_Char(char *inName, Li *l) ;
//基于bitBuff 为32位
//从char01文件到bit文件
//
void StrFile_To_BitFile(const char *infName) ;

//不生成中间文件版本
void Coding_BitFile_By_Huff(char *inName, Li *l) ;
//在LI中查找给定Elem 返回该elem的codes
char * Coding_Find(Elem elem, Li *l) ;







///////////////////////////解码////////////////
//把建立好的Huffman树传进去，在里面完成从二进制文件解压
void Decode_Huffman(char *infName) ;  
//建立List
void Decode_Create_List(Li *l, char ch, int right, int i) ;


//返回当前要读的位
int Get_Bit_In_Buff(unsigned int *buff) ;
#endif