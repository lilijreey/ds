
#include "Huffman_Tree.h"
#include <stdio.h>

//#define SHOW_HUFFMAN
int main()
{
		Li li ;
		BinTNode head ; //head->next is root
		char chooes ;
		char inName[256] ;
		

		printf("Coding File input 1, Decoding File input 2.\n") ;
		printf("Please chooes: ") ;
		scanf("%c", &chooes) ;
		switch(chooes)
		{
		case '1':
				Init_List(&li) ; //Initial List
				Get_Coding_File_Name(inName) ;
		
	
				BinFile_Input_List(inName, &li) ;
				Sequence_List(&li) ; //基于排序的。
				Link_Copy_List(&li, &head) ;//&head Node
				Create_Huffman_Tree(&head) ;

				//改造Huff_tree设置哨兵叶子 sentinel 哨兵
				//改造后 将不再是标准的huff tree
				Set_Sent(head.next) ;  //root node
				

			//	Show_Get_Huffman_Code_Interface(&head) ; //生成了 Huffman codes


				Buff_Get_Huffman_Code_Interface(&head, &li) ; //生成了 Huffman codes
				 //不生成中间文件的
				Coding_BitFile_By_Huff(inName, &li) ;
				break ;


		case '2':
				//////////////////////////////解码////////////////
				//把建立好的Huffman树传进去，在里面完成从二进制文件解压
				//得到压缩文件名，检验文件名
				if(Get_Decoding_File_Name(inName))
						Decode_Huffman(inName) ;  
				break ;

		default :
				printf("input error!") ;
				fflush(stdin) ;

		}
		
		return 0 ;
}
