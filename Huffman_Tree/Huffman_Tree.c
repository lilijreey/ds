#include "Huffman_Tree.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




//初始化List
void Init_List(Li *l)
{
		l->lenght = 0 ;
		l->size = LIST_INIT_SIZE ;
		if((l->node = (pLiNode) malloc(LIST_INIT_SIZE * sizeof(LiNode))) == NULL)
		{
				perror("malloc memory error!") ;
				exit(1) ;
		}
}

void Get_Coding_File_Name(char *codName) 
{
		printf("Enter you want Coding file name: ") ;
		scanf("%s", codName) ;
}

int Get_Decoding_File_Name(char *codName) 
{
		char infName[5] ;	
		

		printf("Enter you want Decoding file name: ") ;	
		scanf("%s", codName) ;

		strcpy(infName, &codName[strlen(codName) - 4]) ;
		//检验是否为.hfc结尾
		if(strcmp(infName, ".hfc") != 0)
		{
				printf("Not standard file") ;
				return 0 ;
		}
		else
				return 1 ;
}

//输入节点的值和权
void Input_List(Li *l) //HeadNode
{
		int right ;
		Elem elem ;
		int i = 0 ;

		printf("请输入数值和权， 以“CTRL+D” 结束.\n") ;
		while(scanf("%c", &elem))
		{
				if(elem == EOT)
						break ;
				scanf("%d", &right) ;
				///IF_FULL
				if(l->lenght == l->size)
				{
						if((l->node = (pLiNode) realloc(l->node, (l->size + LISTINCREMENT) * sizeof(LiNode)))== NULL)
						{
								perror("malloc memory error!") ;
								exit(1) ;
						}
						l->size = l->size + LISTINCREMENT ;
				}
				///
				l->node[i].codes = NULL ;
				l->node[i].elem = elem ;
				l->node[i].right = right ;

				++ i ;
		}
		fflush(stdin) ;
		l->lenght = i ;

		//////////
		//裁剪掉多余的
}


//对节点排序（降序）
void Sequence_List(Li *l ) 
{
		//快速排序
		int i, j ;
		int min  ;
		pLiNode head = l->node ;
		LiNode temp ;
		for(i = 0; i < l->lenght; ++i)
		{
				min = i ;
				for(j = i+1; j < l->lenght; ++j)
				{
						if(head[min].right < head[j].right)
						{
								min = j ;
						}
				}
				//swap/////////
				if(min != i)
				{
						temp.codes = head[i].codes ;
						temp.elem = head[i].elem ;
						temp.right = head[i].right ;

						head[i].codes = head[min].codes ;
						head[i].elem = head[min].elem ;
						head[i].right = head[min].right ;

						head[min].codes = temp.codes ;
						head[min].elem = temp.elem ;
						head[min].right = temp.right ;
				} 
		}
}

void Show_List(Li *l)
{
		int i = 0 ;
		if(0 == l->lenght)
		{
				printf("线性表为空无法输出!\n") ;
		}
		else
		{
				while(i < l->lenght)
				{
						printf("E_%c-R_%d\n", l->node[i].elem, l->node[i].right) ;
						++i ;
				}
		}
}
void File_Create_List(const char *codName, Li *l)  
{
		File_Input_List(codName, l) ;
		Sequence_List(l) ;

}

//复制LIST到LINK(treeNdode)
void Link_Copy_List(Li *l, pBinTNode per)//&head Node
{
		int i = 0 ;
		pBinTNode p ;

		for(; i < l->lenght; ++i)
		{
				if((p = (pBinTNode) malloc(sizeof(BinTNode))) == NULL)
				{
						perror("malloc momery error!") ;
						exit(1) ;
				}
				per->next  = p ; //rchild is next 
				p->per = per ; //child is per
				p->rchild = p->lchild = p->parent = NULL ;
				p->next = NULL ;
				p->elem = l->node[i].elem ;
				p->right = l->node[i].right ;

				per = per->next ;
		}
}


void Create_Huffman_Tree(pBinTNode per) //&head Node  
{
		//左右子树节点地址
		pBinTNode lNode, rNode, parNode ;

		while(per->next->next != NULL) //只有一个头结点停止
		{
				//得到左右子树地址//引用传递
				//较大权做左子树，较小右子树
				Get_LR_Subtree(per, &lNode, &rNode) ;
				//建立父节点
				if((parNode = (pBinTNode) malloc(sizeof(BinTNode))) == NULL)
				{
							perror("malloc memory error!") ;
							exit(1) ;
				}
				parNode->per = parNode->next = NULL ;
				//链接
				parNode->lchild = lNode ;
				parNode->rchild = rNode ;
				parNode->right = lNode->right + rNode->right  ;
				//lNode->parent = rNode->parent = parNode ;
				
				//在LINk上删除lr并添加par
				Delete_RL_Add_Par(per, lNode, rNode, parNode) ;
		}
}


void Set_Sent(pBinTNode root)
{
		pBinTNode lchild, rchild ;

		//找到最左叶子
		while(root->lchild != NULL)
				root = root->lchild ;
		//以此叶子为父节点生成左右子叶
		if((rchild = (pBinTNode)malloc(sizeof(BinTNode))) == NULL)
		{
				perror("malloc memory error!") ;
				exit(1) ;
		}
		/////////operator=
		rchild->elem = root->elem ;
		rchild->lchild = rchild->rchild = NULL ;
		rchild->parent = root ;
		rchild->right = root->right ;
		////////////////
		root->rchild = rchild ;

		if((lchild = (pBinTNode)malloc(sizeof(BinTNode))) == NULL)
		{
				perror("malloc memory error!") ;
				exit(1) ;
		}
		lchild->elem = -1 ;
		lchild->lchild = lchild->rchild = NULL ;
		lchild->right = -1 ;
		root->lchild = lchild ;
} 

void Get_LR_Subtree(pBinTNode per, pBinTNode *lNode, pBinTNode *rNode)
{
		pBinTNode head = per ;
		*rNode = per->next ;
		per = per->next->next ; //从第二个开始
		
		//right 权小
		while(per != NULL)
		{
				if((*rNode)->right > per->right)
						*rNode = per ;
				per = per->next ;
		}
		//left
		*lNode = head->next ;
		if(*lNode == *rNode) //如果lNode为第一个节点
				*lNode = head->next->next ;
		per = head->next ;
		while(per != NULL)
		{
				if((*lNode)->right > per->right && per != *rNode)
						*lNode = per ;
				per = per->next ;
		}
}

void Delete_RL_Add_Par(pBinTNode head, pBinTNode l, pBinTNode r, pBinTNode par) 
{
		//delete lNode
		l->per->next = l->next ;
		if(l->next == NULL)
				l->per->next = NULL ;
		else
				l->next->per = l->per ;

		//delte rNode
		r->per->next = r->next ;
		if(r->next == NULL)
				r->per->next = NULL ;
		else
				r->next->per = r->per ;

		//在头节点的下一个位置加入par
		if(head->next != NULL)
				head->next->per = par ;

		par->next = head->next ;
		head->next = par ;
		par->per = head ;
}


void Show_Get_Huffman_Code_Interface(pBinTNode head) //give root node
{
		char *buff ;
		int i = -1 ;
		if((buff = (char *)malloc((HUFF_BUFFER) * sizeof(char))) == NULL)
		{
				perror("malloc memory error!") ;
				exit(1) ;
		}
	

 		Show_Get_Huffman_Code(head->next, buff, &i) ; 
		free(buff) ;
}


void Show_Get_Huffman_Code(pBinTNode p, char *buff, int *i)
{
		++(*i) ;
		buff[*i + 1] = '\0' ;

		if(p->lchild != NULL)
		{
				buff[*i] = left ;
				Show_Get_Huffman_Code(p->lchild, buff, i) ;  /////////第一个不算
				--(*i);
				buff[*i+1] = '\0' ;
		}
		else
		{	
				//buff[*i] = left ; 前面已经加了
				printf("Asc_%d--%c_right%d: %s\n", p->elem, p->elem, p->right, buff) ;
		}

		if(p->rchild != NULL)
		{
				buff[*i] = right ;
				Show_Get_Huffman_Code(p->rchild, buff, i) ;
				--(*i) ;
				buff[*i + 1] = '\0' ;
		}
}


void Buff_Get_Huffman_Code_Interface(pBinTNode head, Li *l) //give root node
{
		char *buff ;
		int i = -1 ;
		if((buff = (char *)malloc((HUFF_BUFFER) * sizeof(char))) == NULL)
		{
				perror("malloc memory error!") ;
				exit(1) ;
		}	

 		Buff_Get_Huffman_Code(head->next, buff, &i, l) ;
		
		free(buff) ;
		//destroy trer
		//Destroy_Huffman_Tree(head->next) ;
}


void Buff_Get_Huffman_Code(pBinTNode p, char *buff, int *i, Li *l)
{
		char * code ; //List codes
		++(*i) ;
		buff[*i + 1] = '\0' ;

		if(p->lchild != NULL)
		{
				buff[*i] = left ;
				Buff_Get_Huffman_Code(p->lchild, buff, i, l) ;  /////////第一个不算
				--(*i);
				buff[*i+1] = '\0' ;
		}
		else
		{	

				////////////
				//buff[*i] = left ; 前面已经加了
				//查找list中对应的节点 开辟codes并返回codes的地址
				if(p->right != -1) //不是哨兵位
				{
						code = Get_List_Code(l, p->elem, *i) ;
						if(code != NULL) //查找到得添加 
								strcpy(code, buff) ;
						//////////////////////////
				}
		}

		if(p->rchild != NULL)
		{
				buff[*i] = right ;
				Buff_Get_Huffman_Code(p->rchild, buff, i, l) ;
				--(*i) ;
				buff[*i + 1] = '\0' ;
		}
}

char * Get_List_Code(Li *l, Elem elem, int lenght)
{
		int i = 0 ;

		for( ; i < l->lenght; ++i)
		{
				if(elem == l->node[i].elem )
				{
						if((l->node[i].codes = (char *)malloc((lenght + 2) * sizeof(char))) == NULL)
						{
								perror("malloc memory error!") ;
								exit(1) ;
						}
						break ;
						
				}
		}
		return l->node[i].codes ;
}


void Destroy_Huffman_Tree(pBinTNode bNode) 
{
		if(bNode->lchild != NULL)
		{
			Destroy_Huffman_Tree(bNode->lchild) ;		
		}
		if(bNode->rchild != NULL)
				Destroy_Huffman_Tree(bNode->rchild) ;

		free(bNode) ;
}


void File_Input_List(const char *codName, Li *l) //HeadNode
{

		Elem elem ;
		int  i = 0 ;
		FILE *infile ;
		if((infile = fopen(codName, "r")) == NULL)
		{
				perror("file not open") ;
				exit(1) ;
		}
		//fclose(infile) ;

		while((elem = getc(infile)) != EOF)
		{
				
			if(!Find_Elem_In_List(l, elem))
			{
						if(l->lenght == l->size)
						{
							if((l->node = (pLiNode) realloc(l->node, (l->size + LISTINCREMENT) * sizeof(LiNode)))== NULL)
							{
								perror("malloc memory error!") ;
								exit(1) ;
							}
							l->size = l->lenght + LISTINCREMENT ;
						}
					///
						l->node[i].codes = NULL ;
						l->node[i].elem = elem ;
						l->node[i].right = 1 ;
						l->lenght++ ;
						++i ;
						
				}
		}
		fclose(infile) ;
		
}

//建立文件字符线性表 统计字符个数（权）
void BinFile_Input_List(const char *codName, Li *l) //HeadNode
{

		Elem elem ;
		int  i = 0 ;
		FILE *infile ;
		if((infile = fopen(codName, "rb")) == NULL)
		{
				perror("file not open") ;
				exit(1) ;
		}
		//fclose(infile) ;

		while(fread(&elem, sizeof(unsigned char), 1, infile))
		{
				//没找着到添加
			if(!Find_Elem_In_List(l, elem))
			{
						if(l->lenght == l->size)
						{
							if((l->node = (pLiNode) realloc(l->node, (l->size + LISTINCREMENT) * sizeof(LiNode)))== NULL)
							{
								perror("malloc memory error!") ;
								exit(1) ;
							}
							l->size = l->lenght + LISTINCREMENT ;
						}
					///
						l->node[i].codes = NULL ;
						l->node[i].elem = elem ;
						l->node[i].right = 1 ;
						l->lenght++ ;
						++i ;
						
				}
		}
		fclose(infile) ;
		
}
int Find_Elem_In_List(Li *l, Elem elem)
{
		int i ;
		for(i = 0; i < l->lenght; ++i)
		{
				if(elem == l->node[i].elem)
				{
						++(l->node[i].right) ; 
						return 1;
				}
		}
		return 0 ;
}

//////////////////////Coding/////////////////////////
//char字符的编码
void Coding_By_Char(char *inName, Li *l)
{
		FILE *infile ;
		FILE *outfile ;
		Elem elem ;
		char *codes ;
		char ofileName[256] ;

				//得到名字
		strcpy(ofileName, inName) ;
		strcat(ofileName, ".job.txt") ;
		ofileName[strlen(inName) + 8] = '\0' ;

		if((infile = fopen(inName, "r")) == NULL)
		{
				perror("File not open!") ;
				exit(1) ;
		}

		if((outfile = fopen(ofileName, "w")) == NULL)
		{
				perror("File not open!") ;
				exit(1) ;
		}

		while(elem = getc(infile))
		{
				if(feof(infile))
						break ;
				codes = Coding_Find(elem, l) ;
				//搜索函数在LI中搜索返回Codes的地址
			/////	printf("%c%s ", elem, codes) ;
				fprintf(outfile, "%s",codes) ;
		}

		fclose(infile) ;
		fclose(outfile) ;
}


char * Coding_Find(Elem elem, Li *l)
{
		int i = 0 ;
		while(i < l->lenght)
		{
				if(elem == l->node[i].elem) 
						return l->node[i].codes ;
				++i ;
		}
		return 0 ;
}

///需要已转换成char10的文件
//基于bitBuff 为32位
//从char01文件到bit文件
//
void StrFile_To_BitFile(const char *codName)
{
		FILE *infile ;
		FILE *outfile ;
		//从str0开始，写入bit的最高位 做一
		int j = 0 ;
		char ch = 1 ;//初始化ch用于进入while
		char outfName[250] ;//二进制文件名字
		char infName[250] ;
		//清空 
		
		unsigned int bitBuff = 0 ;

			//得到名字
		strcpy(infName, codName) ;
		strcat(infName, ".job.txt") ;
		infName[strlen(codName) + 8] = '\0' ;
		//open in file
		if((infile = fopen(infName, "r")) == NULL)
		{
				perror("Open file error!") ;
				exit(1) ;
		}
		//open out bin file
		

		strcpy(outfName, codName) ;
		strcat(outfName, ".hfc") ;
		outfName[strlen(codName) + 4] = '\0' ;

		if((outfile = fopen(outfName, "wb")) == NULL)
		{
				perror("Open file error!") ;
				exit(1) ;
		}

		

		while(!feof(infile)) //str[i] != '\0'
		{
				bitBuff &= 0x0 ; 
				j = 0 ;
				//每次装满 32bit
				while(ch = getc(infile)) 
				{ 
						if(feof(infile))
								break ;
					////////	printf("%c", ch) ;
						if(ch == '1')
						(bitBuff) |= 0x1 ;
						if(j == 31)  //BitBuff满了就跳出
								break ;
						++j ;
						(bitBuff) <<= 1 ; //下一个str有值才左移隐式赋0
				}
				//写到二进制文件
				fwrite(&bitBuff, sizeof(unsigned int), 1, outfile) ;
		}

		fclose(infile) ;
		fclose(outfile) ;
}

//不生成中间文件版本
void Coding_BitFile_By_Huff(char *inName, Li *l) 
{
		FILE *infile ;
		FILE *outfile ;
		Elem elem ;
		char *codes ;
		char ofileName[256] ;
		unsigned int buff = 0x0; //二进制缓存buffer
		int i = 0 , j = 0 ;


		//得到相应的压缩文件名
		strcpy(ofileName, inName) ;
		strcat(ofileName, ".hfc") ;
		ofileName[strlen(inName) + 4] = '\0' ;

		//open file
		if((infile = fopen(inName, "r")) == NULL)
		{
				perror("Input File not open!") ;
				exit(1) ;
		}

		if((outfile = fopen(ofileName, "wb")) == NULL)
		{
				perror("Output file not open!") ;
				exit(1) ;
		}


		////写入字符编码和权
		//1写入总共的字符个数
		fwrite(&(l->lenght), sizeof(int), 1, outfile) ;
		//2写入字符和权值
		for(i = 0; i != l->lenght; ++i)
		{
				fwrite(&(l->node[i].elem), sizeof(Elem), 1, outfile) ;
				fwrite(&(l->node[i].right), sizeof(int), 1, outfile) ;
		}

		//////查找对应字符HUFFMAN code
		while(elem = getc(infile))
		{
				if(feof(infile))
						break ;
				codes = Coding_Find(elem, l) ;
				//搜索函数在LI中搜索返回Codes的地址
				//1把code char 01 逐个转换成 bit 01 写入 buff
				for(i = 0; codes[i] != '\0'; ++i)
				{
						if(codes[i] == '1')
								buff |= 0x1 ;
						
						if(j == 31) //buff 满了就写入文件
						{
								fwrite(&buff, sizeof(unsigned int), 1, outfile) ;
								j = 0 ;
								buff = 0x0 ;
						}
						else
						{
								++j ;
								buff <<= 1 ;
						}
				}
		}
		if(j != 0) //左移到顶端
		{
				buff <<= 31 -j ;
				fwrite(&buff, sizeof(unsigned int), 1, outfile) ;
		}
				

		fclose(infile) ;
		fclose(outfile) ;

		printf("\nComperssion Success!") ;
}

//把建立好的Huffman树传进去，在里面完成从二进制文件解压
void Decode_Huffman(char *infName)
{
		FILE *infile ;
		FILE *outfile ;
		Li li ;
		
		BinTNode head ;
		pBinTNode currNode ;
		int i ; //当前bit的下标
		int currbit = -1 ; //当前bit
		unsigned int buff = 0 ;
		int totalWord = 0 ;
		char ch ;
		int right ;
		char outfName[256] ;
		
		
		//打开压缩文件
		if((infile = fopen(infName, "rb")) == NULL)
		{
				perror("Open file error!") ;
				exit(1) ;
		}

		//得到相应的解压缩文件名
		strcpy(outfName, infName) ;
		outfName[strlen(outfName) - 4] = '\0' ;


		//打开解压文件
		if((outfile = fopen(outfName, "w")) == NULL)
		{
				perror("Open file error!") ;
				exit(1) ;
		}


		/////读取开头字符建立Huffman tree
		//1 读取字符个数
		Init_List(&li) ; //Initial List

		fread(&totalWord, sizeof(int), 1, infile) ;
		for(i = 0; i != totalWord; ++i)
		{
				fread(&ch, sizeof(Elem), 1, infile) ;
				fread(&right, sizeof(int), 1, infile) ;
				//建立List
				Decode_Create_List(&li, ch, right, i) ;
		}
	
		Link_Copy_List(&li, &head) ;//&head Node
		Create_Huffman_Tree(&head) ; //建立Huffman树
		//改造Huff_tree设置哨兵叶子 sentinel 哨兵

		Set_Sent(head.next) ;  //root node

		//currNode 初始化为root
		currNode = head.next ;

		//从二进制文件写入buff
		while(!feof(infile))  //文件结尾时推出
		{
				//读取Buff
				fread(&buff, sizeof(unsigned int), 1, infile) ;

				for(i = 31; i >= 0; --i) //读完Buff跳出
				{

						//从高位开始诸位读出
						//返回当前要读的位
						currbit = Get_Bit_In_Buff(&buff) ;
						///////////printf("%d", currbit) ;

						//根据读出来的bit查找相应字符
						if(currNode->lchild != NULL) //当前节点不是叶子节点
						{
								//得到当前节点 注意要
								currbit ? (currNode = currNode->rchild) : (currNode = currNode->lchild) ; //向下搜索
								if(currNode->lchild == NULL && currNode->right != -1) //哨兵位不输出
								{	//当找到叶子节点时1写入文件，2回到头结点
										putc(currNode->elem, outfile) ;
									/////////	printf("%c\n", currNode->elem ) ;
										currNode = head.next ;	
								}
						}

				}
				
		}
		fclose(infile) ;
		fclose(outfile) ;

		printf("\nFile Decompression success!") ;
}


//返回当前要读的位
int Get_Bit_In_Buff(unsigned int *buff) 
{
		//从高位向低位读
		//采用&的方式屏蔽其他位
		//没读一位buff左移一位以便将下一位移到最高位。

		if((*buff & 0x80000000) == 0x0)
		{
				*buff <<= 1 ;
				return 0 ;
		}
		else
		{
				*buff <<= 1 ;
				return 1 ;
		}
}

void Decode_Create_List(Li *l, char ch, int right, int i) 
{
		if(l->lenght == l->size)
		{
				if((l->node = (pLiNode) realloc(l->node, (l->size + LISTINCREMENT) * sizeof(LiNode)))== NULL)
				{
						perror("malloc memory error!") ;
						exit(1) ;
				}
				l->size = l->size + LISTINCREMENT ;
		}
		l->node[i].elem = ch ;
		l->node[i].right = right ;
		++(l->lenght) ;
}
