

//数组操作
//给予c语言对动态开辟多维数组的缺陷， 实现用一维数组模拟多维数组
//基于变长参数的方法


///////ANSI C
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#define DIMMAXSIZE 8  //dims开辟的空间长度
#define ONE_DIM_MAX_SIZE 20  //单个维度上的最大长度
enum {FAILE, SUCCESS} ;

typedef int ElemType ;

//数组维度结构
typedef struct 
{
	int dims ; //维度
	int totalSize ; // 总共大小
	ElemType everyDims[DIMMAXSIZE] ; //各个维度的值从左向右一次写入
}Dim, *pDim ;

//Array struct
typedef struct 
{
	ElemType * base ; //数组的基址
	Dim dim ;
}Arr, *pArr ;

void menu(void) ;
/////////////////////////////////ARRAY_ADT/////////////////////////////
//1初始化数组结构
int InitArray(pArr *parr, int dims, ...) ; //////可变参数在固定参数的基础上建立

//2 函数1的交互界面
void InitArray_Interface(pArr *parr) ;

//3 销毁数组
void DistroyArray(pArr *parr) ;

//4从数组里去数
int Get_Value(pArr, ElemType *, ...) ;

//5函数4的交互界面
void Get_Value_Interface(pArr) ;

//6显示数组的属性
void Show_Array(pArr) ;

//7输入函数一次只能赋一位
int Assign_Array(pArr, ElemType , ...) ;

//函数7的交互界面
void Assign_Array_Interface(pArr) ; 
//////
/////Get 和 Assign 基本相同 可复用 



int main()
{
	//定义一个数组结构的指针
	pArr parr = NULL ;
	int select ;

	while(1)
	{
			menu() ;
			scanf("%d", &select) ;
			switch(select)
			{
			case 0:
				return 0 ;

			case 1:
					InitArray_Interface(&parr) ;
					break ;

			case 2:
					Assign_Array_Interface(parr) ;
					break ;

			case 3:
					Get_Value_Interface(parr) ;
					break ;

			case 4:
					DistroyArray(&parr) ;
					break ;

			case 5:
					Show_Array(parr) ;
					break ;

			default:
					printf("\n选择错误，请从选\n") ;
					fflush(stdin) ;
					break ;			
			}
	}

	return 0 ;
}



void menu(void)
{
		printf("\n***********************************\n") ;
		printf("*1 建立数组           2 赋值到数组*\n") ;
		printf("*3 从数组取值         4 销毁数组  *\n") ;
		printf("*5 显示数组属性       0 退出      *\n") ;
		printf("***********************************\n") ;
		printf("请选择： ") ;
}


int InitArray(pArr *pparr, int dims, ...) //////可变参数在固定参数的基础上建立
{
	int i, totalDims = 1 ;//各个dim的和
	va_list ap ;
	
	//开辟*ppArr
	if( (*pparr = (pArr)malloc(sizeof(Arr))) == NULL)
	{
		printf("\n MM is error\n") ;
		return FAILE ;
	}
	(*pparr)->dim.dims = dims ;

	va_start(ap, dims) ;
	//各个维度复制
	for(i = 0; i < dims; ++i)
	{
		totalDims *= (*pparr)->dim.everyDims[i] = va_arg(ap, int) ;
	}

	(*pparr)->dim.totalSize = totalDims ;

	va_end(ap) ;

	//开辟(*ppArr)->base
	if( ((*pparr)->base = (ElemType *)malloc(totalDims * sizeof(ElemType))) == NULL) 
	{
		printf("\n MM is error\n") ;
		return FAILE ;
	}
		
	return SUCCESS ;
}

void InitArray_Interface(pArr *parr) 
{

		int dims ; //维数
		int i ;
		int d[DIMMAXSIZE] ; // 每个维度上的大小 因为最大8维，有8个
		                             //这样定义不好， 有更好的吗？？
		
		if((*parr) != NULL)
		{
				DistroyArray(parr) ;
		}
		else
		{
				printf("\n请输入数组的维数：") ;
				scanf("%d", &dims) ;
				while(dims > DIMMAXSIZE || dims < 1)
				{
						printf("无效输入，请重输： ") ;
						scanf("%d", &dims) ;
						fflush(stdin) ;
				}
				printf("请输入个维的维数: ") ;
				for(i = 0; i < dims; ++i)
				{
						scanf("%d", &d[i]) ;
						while(d[i] > ONE_DIM_MAX_SIZE || d[i] < 1)
						{
								printf("无效输入，请重输： ") ;
								scanf("%d", &d[i]) ;
								fflush(stdin) ;
						}
				}

				//这没有可移植性
				if(InitArray(parr, dims, d[0], d[1], d[2], d[3], d[4], d[5], d[6], d[7] ) == SUCCESS)
				{
						printf("数组创建成功\n") ;
				}
				else
				{
						printf("数组创建失败\n") ;
				}
		}
}

void DistroyArray(pArr *parr) 
{
		if((*parr) = NULL)
			printf("无效指针，无法删除\n")	;
		else
		{
				free(*parr) ;
				*parr = NULL ;
				printf("删除成功\n") ;
		}
}


int Get_Value(pArr parr, ElemType *get, ...) 
{
		va_list ap ;
		int i ;
		int power = parr->dim.totalSize ; //每个维度的单位大小
		int offer =0 ; //最终的偏移量

		va_start(ap, get) ;
		for(i = 0; i < parr->dim.dims; ++i)
		{
				power /= parr->dim.everyDims[i] ;
				offer += power * va_arg(ap, int) ;
		}
		*get = parr->base[offer] ;

		return SUCCESS ;
}

void Get_Value_Interface(pArr parr) 
{
		int d[DIMMAXSIZE] ;
		int i ;
		ElemType get ;
		if(parr == NULL)
		{
				printf("无效指针，无法取数\n") ;
		}
		else
		{///////////////////////////////可以做成一个函数
			printf("请输入元素下标: ") ;
			for(i = 0; i < parr->dim.dims; ++i)
			{
					scanf("%d", &d[i]) ;

					while(d[i] >= parr->dim.everyDims[i] || d[i] < 0)//有缺陷
					{
							fflush(stdin) ;
							printf("第 %d 下标无效，请重新输入：", i) ;
							scanf("%d", &d[i]) ;
					}
			}
			/////////////////////////////////////////////
			Get_Value(parr, &get, d[0], d[1], d[2], d[3], d[4], d[5], d[6], d[7]) ;//令人恶心

			printf("下标为") ;
			for(i = 0; i < parr->dim.dims; ++i)
			{
					printf("[%d]", d[i]) ;
			}
			printf("\n为: %d", get) ; 
		}
}

void Show_Array(pArr parr)
{
		int i ;
		if(parr == NULL)
				printf("\n无效指针，无法显示\n") ;
		else
		{
				printf("数组的维数为：%d\n", parr->dim.dims) ;
				printf("数组每个维数为\n" ) ;
				for(i = 0; i < parr->dim.dims; ++i)
				{
						printf("[%d]",parr->dim.everyDims[i]) ;
				}
				printf("\n数组的总大小为：%d\n", parr->dim.totalSize) ;
				printf("数组的基址为：%p\n", parr->base) ;
		}
}

int Assign_Array(pArr parr, ElemType assign, ...) 
{
		va_list ap ;
		int i ;
		int power = parr->dim.totalSize ; //每个维度的单位大小
		int offer =0 ; //最终的偏移量

		va_start(ap, assign) ;
		/////////////////////////////////////可做成一个函数
		for(i = 0; i < parr->dim.dims; ++i)
		{
				power /= parr->dim.everyDims[i] ;
				offer += power * va_arg(ap, int) ;
		}
		///////////////////////////////////////
		parr->base[offer] = assign ;

		return SUCCESS ;
}


void Assign_Array_Interface(pArr parr) 
{
		int d[DIMMAXSIZE] ;
		int i ;
		ElemType assign ;
		if(parr == NULL)
		{
				printf("无效指针，无法取数\n") ;
		}
		else
		{
				printf("请输入赋值元素：") ;
				scanf("%d", &assign) ;
			///////////////////////////////可以做成一个函数
				printf("请输入元素下标: ") ;
				for(i = 0; i < parr->dim.dims; ++i)
				{
						scanf("%d", &d[i]) ;

						while(d[i] >= parr->dim.everyDims[i] || d[i] < 0)//有缺陷
						{
								fflush(stdin) ;
								printf("第 %d 下标无效，请重新输入：", i) ;
								scanf("%d", &d[i]) ;
						}
				}
			/////////////////////////////////////////////
			if(Assign_Array(parr, assign, 	d[0], d[1], d[2], d[3], d[4], d[5], d[6], d[7]) == SUCCESS)
					printf("\n输入成功\n") ;
			else
			{
					printf("\n输入失败\n") ;
			}
		}
}