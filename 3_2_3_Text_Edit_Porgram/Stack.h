
#include <stdio.h>
#include <stdlib.h>

enum status {FAIL, SUCCESS} ;


typedef struct stack
{
		char data ;
		struct	stack *next ;
}Stack, *pStack ;



////////////////////ADT Stack 的基本操作

//3 销毁栈
void Distroy_Stack_S(pStack *) ;

//5 检测栈是否为空
int If_Stack_Empty_S(pStack *) ;

//7 插入元素
void Push_S(pStack *, char a) ;


//10 出栈返回出栈的元素
char Pop_S(pStack *) ;



void Distroy_Stack_S(pStack *top) 
{
		pStack del ;

		while(*top != (pStack)NULL)
		{
				del = *top ;
				*top = (*top)->next ;
				free(del) ;
		}
}




int If_Stack_Empty_S(pStack *top)
{
		if(*top != (pStack)NULL)
				return SUCCESS ;
		return FAIL ;
} 


void Push_S(pStack *top, char a) 
{
		 pStack newNode ;

		if((newNode = (pStack)malloc(sizeof(Stack))) == (pStack)NULL)
		{
				printf("\n空间分配错误！\n") ;
				exit(1) ;
		}
		newNode->data = a ;
		newNode->next = *top ;
		*top = newNode ;
}



char  Pop_S(pStack *top) 
{
		pStack del ;
		char a = EOF ;

		if(*top != (pStack)NULL)
		{
				a = (*top)->data ;
				del = *top ;
				*top = (*top)->next ;
				free(del) ;
				return a ;
		}
		return a ;
}

