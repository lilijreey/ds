=begin
   学习笔记
    https://www.wiz.cn/wapp/folder/fb33adf0-762f-11ea-a88a-09486b3626db?c=%2F%E7%AE%97%E6%B3%95%2F&docGuid=51ca4cd0-9531-11ea-9d0c-d37abec15f37
    树状数组
* 操作
1. 求前N项和
2. 更细元素
3. 遍历子节点
4. 节点高度

=end

def lowbit(x)
    return x & -x
end

## 算法描述中是从最小下标是从1开始的
def create_tree_array(a)
    c= []
    a.length.times {|i|
        n=0
        th = i+1##+1 下标从0开始
        lb = lowbit(th)
        # printf("===i:%d th:%d lb:%d\n", i, th, lb)
        (0..i).each {|j|
            ## c[th-1] = a[th-2^k] ... a[th-1]; 2^k = lowbit(th)
            m = th - lb +j
            # printf("i:%d j:%d m:%d\n", i, j, m)
            break if (m > i)
            n +=a[m]
        }
        c << n
    }
    return c
end

def get_sum(c, i)
    if (i >= c.length); return nil; end
    ## i = i -lowbit(i), 覆盖之前的子树和
    m = 0
    th = i +1
    while th > 0
        m+= c[th-1]
        # printf("th:%d lowbit:(th):%d\n", th, lowbit(th))
        th -= lowbit(th)
    end
    return m
end

def get_origin_element(c, i)
    #得到原始值, 遍历当前节点的子树
    # TODO 直接求解
    if (i == 0)
        return c[0]
    end
    get_sum(c, i) - get_sum(c, i-1)
end

def update_element(_c, i, v)
    c = _c.dup
    ## 一直向父节点回溯, i 的父节点为, 向上补齐0 (lowbit(i+1) + i + 1) -1
    # 101 + 1
    # 110 + 10
    #1000

    th = i+1
    ## 得到原始a[i]的值
    diff = v - get_origin_element(_c, i)
    while th <= c.length
        c[th-1] += diff
        th += lowbit(th)
        # printf("th:%d\n", th)
    end
    return c
end

def node_high(i)
    return Math.log2(lowbit(i))
end


## 遍历所有子节点
def child_node(i)
    iHight = node_high(i)
    child = []
    (i.downto 1).each {|e|
        node_high(e) > iHight and break
        child << e
    }
    return child.reverse
end


require_relative './assert'
a = 1,2,3,4,5,6
c = create_tree_array(a)
AssertEq(c, [1,3,3,10,5,11])

AssertEq(get_sum(c, 0), 1)
AssertEq(get_sum(c, 1), 3)
AssertEq(get_sum(c, 2), 6)
AssertEq(get_sum(c, 3), 10)
AssertEq(get_sum(c, 4), 15)
AssertEq(get_sum(c, 5), 21)

AssertEq(update_element(c, 0,-1), create_tree_array([-1,2,3,4,5,6]))
AssertEq(update_element(c, 2,10), create_tree_array([1,2,10,4,5,6]))
AssertEq(update_element(c, 5,0), create_tree_array([1,2,3,4,5,0]))

AssertEq(node_high(1), 0)
AssertEq(node_high(2), 1)
AssertEq(node_high(3), 0)
AssertEq(node_high(4), 2)
AssertEq(node_high(5), 0)
AssertEq(node_high(6), 1)
AssertEq(node_high(7), 0)
AssertEq(node_high(8), 3)
AssertEq(node_high(9), 0)

(1..9).each {|i|
    printf("%d childs: %s\n", i ,child_node(i).to_s)
}

puts 'all passed'