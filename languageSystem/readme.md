## 程序设计语言理论

##Books
* 计算的本质
* Foundations for Progarmming Languages (John.C MIT 1995)
* 程序设计语言理论 陈意云 (抄MIT的)

## 依赖
* 数理逻辑
* 语言学
* 可计算性


## 机器
#### 自动机
* 有限状态机 是一个计算机的极简模型
  * 确定性有限状态机 DFA (简单，有限制)
     对没个状态都需要能够处理每个输入,并进入一个状态
      很难设计一台DFA,实现能够接受由a,b组成的，倒数第三个字符必须是b的机器

  * 非确定性有限状态机
    NFA, 每个状态对一个输入不在保证能够进入一个状态，而是可能有多个状态

###图灵机 TM
终极机器
* 通过对纸带的读写移动和设计读写的规则，就可以实现一个计算程序, 图灵机通过
   应用不同的纸带和规则，就能够处理一起可计算的问题
* 能访问无限纸带的有限状态机都是图灵机

* 确定性图灵机 DTM
* 非确定性图灵机 NTM

* 没有比图灵机功能更强的机器了, 因为图灵机本身就能够模拟任何增强特性.
   计算的本质

* 通用图灵机 UTM
   可以接受执行任何规则的图灵机, 可以模拟自己

* 图灵完备
   意味着这个系统可以定义一台UTM

### 计算机的天花板-限制

### 不可判断问题(undecidable problems)

## 计算复杂性

## 语言
  能被一台机器接受的所有字符串集合，成为这个机器的语言


Qus.
* 为啥需要谓词演算

## lambda 演算 calculus
  由邱奇发明
  λ演算是一套用于研究函数定义、函数应用和递归的形式系统, 源于可计算理论
  在这种系统在，所有的计算都会被规约为函数的定义和应用（调用)上
  lambda 演算具有严格的数学可证明性，和准确性。
  一种极小的通用编程语言(只使用lambdb，可以用来定义图灵机)

   lambad 表示可以带类型也可以不带类型
   e.g 有类型的
    λx:t.M
    λx:int.x
    意思是接受一个参数x, 类型为int
    .的意思是开始定义函数body
    这个函数的返回值就是参数自己
    λx:int.5
     穿参的写法
    (λx:int.5)3
    这里3是自有变元(实参)，x为约束变元/绑定变元(形参)
      

    两个lambda表达式是否等价，可以用下面这两个规约判定
      * alpha-规约
          就是如果两个lambda表达式只是自有变量的名字不同，那么这两个表达式等价
          e.g.  λx:intM == λy:intM

      *  beta-规约 一种等价的书写形式
          (λx:int.M)N -> [N/x]M

    * 范式
        lambda表达式化简后的最简形式

    * Church-Rosser 定理 
        当且仅当两个表达式等价时，它们会在形式参数换名后得到同一个范式。
       
        


  



  用Proc 替换下面的FizzBuzz 程序
```ruby
  (1..100).map do |n|
     if (n % 15).zero?
        'FizzBuzz'
     elsif (n % 3).zero?
        'Fizz'
     elsif (n % 5).zero?
        'Buzz'
     else
          n.to_s
     end
  end 
```



* 用 lambda 表示一个数字
### 邱奇编码 (Church encoding)
把数据表示为纯代码的技术

ZERO = -> p {-> x { x }}
ONE  = -> p {-> x { p[x] }}
TWO  = -> p {-> x { p[p[x]] }}

p 是一个lambda表示一个过程， 对应的是迭代这个过程的次数
```ruby
def to_i(proc) 
   proc[-> n {n+1}][0]
end
e.g. to_i(ZERO)

```

* lambda 表示布尔
TRUE  = -> x { y -> x }
FALSE = -> x { y -> y }

```ruby
def to_bool(proc) 
  proc[true][false]
end
e.g. to_bool(TRUE)
TRUE[1][3]

```

* ifelse
IFELSE = -> cond {-> then {-> else {cond[then][else]}} }

* lambda 表示zero?
  def zero?(proc)
    proc[-> x {FALSE}][TRUE]
  end

* 实现数据结构 pair
PAIR = -> first { -> second {-> f { f[first][second]}}}
FIRST = -> pair { pair[-> x { -> y { x }}]}
SECOND= -> pair { pair[-> x { -> y { y }}]}
 
 PAIR[THREE][FIVE]


* 不动点组合子（英语：Fixed-point combinator，或不动点算子）
   是计算其他函数的一个不动点的高阶函数。
   函数 f 的不动点是一个值x 使得f(x) = x

* Y 组合子
  Y组合子的用处是使得 lambda 定义递归函数不需要名字。

  Y = -> f { -> x 
                { f[x[x]]}[-> x {f[x[x]]}]}


* Z 组合子
   Y 组合子的一种严格转换


* lambda 演算
  包含三种表达式: 变量， 函数，调用
    可以模拟图灵机，两个系统完全等价

* SKI 组合子演算
  比lambda 演算更简单的表达式语法规则系统
  包含 调用和字母符号
  S[a][b][c]  规约为 a[c][b[c]]
  K[a][b] 规约为 a
  I[a]    规约为 a
    

## 计算的本质


## 语义
* denotational semantic (表示语义)
    用其他的语言来解释新的语言的含义
    通常他把程序转换为数学化的对象,从而可以用数学工具来研究他们
