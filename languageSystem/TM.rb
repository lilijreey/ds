## 图灵机 ruby 实现
# 拥有一条无限长纸带+规则的机器， 可以在纸袋上移动，和读写纸带 
#

## 纸带
## 分为三部分， 左边，当前指向字符， 右边字符
class Tape < Struct.new(:left, :now, :right, :blank)
  def to_s
    "#<Tapel #{left.join}(#{now})#{right.join}>"
  end

  def move_left
    Tape.new(left[0..-2], left.last || blank, [middle] + right, blank)
  end

  def move_right
    Tape.new(left + now, right.first || blank, right.drop(), blank)
  end

  def write(c)
    Tape.new(left, c, right, blank)
  end

end
