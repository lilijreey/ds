## 使用Ruby 实现一种小语言
#实现使用ruby 定义这个小语言的语素,然后把他们构成ast
#
#规约: 语法的检测
#* 以一个AST作为输入 输出一个规约树
#
#Env 环境，用于存放变量,
# VM 可以生成新的环境，代替当前环境， Env Stack, 
#
#* 表达式
#* 语句和表达式的区别
# 规约表达式得到一个新表达式， 规约语句的到一个新语句，和一个新环境
#
#* 序列
#  一系列的表达式或语句
#   exp;exp;...
#
#小步语义
#大步语义


## 语言语素
#.is_reducible? 是否可规约
#.reduce 执行规约
#.to_ruby 得到simple 代码对应的ruby代码，（表示语义)
class Num < Struct.new(:value)
  def to_s #override Object
    value.to_s
  end


  def to_ruby
    ## 本来可以直接返回value,但是我们这里，需要提供统一的ruby表示
    #我们返回一个lamdba, 字符串，然后通过eval(ast.ruby).call({})来计算
    "-> e {#{value.inspect}}"
  end

  def is_reducible?()
    false;
  end

  def reduce(env)
    value
  end

end

class Bool < Struct.new(:value)
  def to_s
    value.to_s
  end

  def to_ruby
    "-> e {#{value.inspect}}"
  end

  def is_reducible?
    false
  end

  def reduce(env)
    value
  end
end

## 变量
# 变量是实现，需要引入一个环境，存放一个变量的名字和他对应的value
# 这个环境在VM中
# 现在的变量，只能存储不可规约的值
class Var < Struct.new(:name)
  def to_s
    name.to_s
  end

  def to_ruby
    "-> e {e[#{name.inspect}]}"
  end

  def is_reducible?
    true
  end

  def reduce(env) ## 变量规约得到对应的值
    env[name]
  end
end

class Assgin < Struct.new(:var, :exp)
  def to_s
    "#{var.name} = #{exp}"
  end

  def to_ruby
    "-> e {e[#{var.name.inspect}] = #{(exp.to_ruby).call(e)}}"
  end

  def is_reducible?
    true;
  end


  def reduce(env) ##赋值不对左边进行规约
    if exp.is_reducible?
      Assgin.new(var, exp.reduce(env))
    else
      env[var.name] = exp
      Nop.new
    end
  end

end

#op +
class Add < Struct.new(:left, :right)
  def to_s
    "#{left} + #{right}"
  end

  def to_ruby
    "-> e { (#{left.to_ruby}).call(e) + (#{right.to_ruby}).call(e)} "
  end

  def is_reducible?()
    true;
  end

  def reduce(env) ##每次只进行一步
    if left.is_reducible?
      Add.new(left.reduce(env), right)
    elsif right.is_reducible?
      Add.new(left, right.reduce(env))
    else
      Num.new(left.value + right.value)
    end
  end

end

#op *
class Mul < Struct.new(:left, :right)
  def to_s
    "#{left} * #{right}"
  end

  def to_ruby
    "-> e { (#{left.to_ruby}).call(e) * (#{right.to_ruby}).call(e)} "
  end

  def is_reducible?()
    true;
  end
  
  def reduce(env) ##每次只进行一步
    if left.is_reducible?
      Mul.new(left.reduce(env), right)
    elsif right.is_reducible?
      Mul.new(left, right.reduce(env))
    else
      Num.new(left.value * right.value)
    end
  end

end


#op < 
class LessThen < Struct.new(:left, :right)
  def to_s
    "#{left} < #{right}"
  end

  def to_ruby
    "-> e { (#{left.to_ruby}).call(e) < (#{right.to_ruby}).call(e)} "
  end

  def is_reducible?()
    true;
  end
  
  def reduce(env) ##每次只进行一步
    if left.is_reducible?
      LessThen.new(left.reduce(env), right)
    elsif right.is_reducible?
      LessThen.new(left, right.reduce(env))
    else
      Bool.new(left.value < right.value)
    end
  end

end

#if cond ; exp; elseExp 
#if cond; trueExp;  可以作为 if cond; exp; else nop;
#  
class IfElse < Struct.new(:cond, :trueExp, :elseExp)
  def to_s
    "if (#{cond}) {#{trueExp}} else {#{elseExp}}"
  end

  def is_reducible?
    true
  end

  def reduce(env)
    if cond.is_reducible?
      IfElse.new(cond.reduce(env), trueExp, elseExp)
    else
      if cond.value
        trueExp
      else
        elseExp
      end
    end
  end
end

## 序列 这里可以使用向Lisp 那样的定义，
#一个Seq分为两部分 first, last
#last 又可以是一个Seq, Cons
class Seq < Struct.new(:first, :last)
  def to_s
    "#{first}; #{last}"
  end

  def is_reducible?
    true;
  end

  def reduce(env)
    if (first.is_reducible?)
      Seq.new(first.reduce(env), last)
    else
      if first.instance_of?(Nop)
        last
      else
        raise "bad op #{fist}"
      end
    end
  end

end

##op while
#while(cond) exp;
#规约为一级展开的 if(cond) {exp; wihle(cond) exp}
class While < Struct.new(:cond, :body) 
  def to_s
    "while (#{cond}) {#{body}}"
  end

  def is_reducible?
    true;
  end

  def reduce(env)
    IfElse.new(cond, 
               Seq.new(body, self),
               Nop.new)
  end
end


class Nop
  def to_s
    "nop"
  end

  def ==(o)
    o.instance_of?(Nop)
  end

  def is_reducible?
    false
  end
end


## 解释simple 虚拟机
class VM < Struct.new(:ast, :env)
  def step
    self.ast = ast.reduce(env)
  end

  def run
    while ast.is_reducible?
      puts "step: #{self}"
      step
    end
    puts self
  end

  def to_s
    "#{ast.to_s}; #{env}"
  end

end


def test_while
  #a=1; while(a<3){a = a+1}
  ast = Seq.new(Assgin.new(Var.new(:a), Num.new(1)),
                While.new(LessThen.new(Var.new(:a),
                                       Num.new(3)),
                Assgin.new(Var.new(:a),
                           Add.new(Var.new(:a), Num.new(1)))))

  VM.new(ast, {})
end

def test_seq
  # a=3; b=1+1; c=a+b
  ast = Seq.new(Assgin.new(Var.new(:a), Num.new(3)),
                Seq.new(Assgin.new(Var.new(:b),
                                   Add.new(Num.new(1), Num.new(1))),
                Assgin.new(Var.new(:c), 
                           Add.new(Var.new(:a), Var.new(:b)))))
  VM.new(ast, {})
end

def test_ifelse
  #if(3<4+1) 1+1 else 2+2
  ast = IfElse.new(LessThen.new(Num.new(3), 
                                Add.new(Num.new(4), Num.new(1))),
                   Add.new(Num.new(1), Num.new(1)),
                   Add.new(Num.new(2), Num.new(2)))
  VM.new(ast, {})
end

def test_assgin
  ast = Assgin.new(Var.new(:x), Add.new(Num.new(3), Num.new(4)))
  env = {}
  VM.new(ast, env)
end

def test_ast
  ## 1*2 + 3 *4 AST, 也可以通过解释器自动构建
  Add.new(
    Mul.new(Num.new(1), Num.new(2)),
    Mul.new(Num.new(3), Num.new(4)),
  )
end

def test_ast_lessThan
  ## 1*2 + 3 *4 AST, 也可以通过解释器自动构建
  LessThen.new(
    Mul.new(Num.new(1), Num.new(2)),
    Mul.new(Num.new(3), Num.new(4)),
  )
end

def test_var
  VM.new(Add.new(Var.new(:x), Var.new(:y)),
          {x:Num.new(3), y:Num.new(4)})
end
