## 素因数 分解
#
### 短除法

def divide(n)
  n < 2 and return nil
  m = n
  ret={} # key=p, value=幂(次数)

  (2..Math.sqrt(n)).each {|e|
    if n % e == 0 # 这里的n一定是个素数, 因为一个合数因子在到这个之前已经被去掉了
      ret[e]=0
      while n % e == 0 ## 除掉该因数的幂
        n /= e
        ret[e] +=1
      end
    end
  }
  n > 1 and ret[n]=1 ## n是质数

  str= m.to_s
  str << "="
  ret.sort.each {|p,c|
    str << p.to_s << '^' << c.to_s << ' * '
  }
  str.delete_suffix!(' * ')
  puts str
end

divide(12)
divide(7)
divide(101)
divide(341)
