#include <stdio.h>
#include <stdlib.h>

#define EXIT_FAILURE 1
#define MAX_SIZE 100
#define SWAP(x,y,t)  ((t) = (x), (x) = (y), (y) = (t))

void GenerateList(int[], int) ; //1 生成一个序列
void Sort(int[], int) ;//2 排序


int main()
{
	int n, i ;
	int list[MAX_SIZE] ;

	printf("Enter the number of numbers to generate: ") ;
	scanf("%d", &n) ;

	if(n < 1 || n > MAX_SIZE)
	{
		fprintf(stderr, "Improper value of n/n") ;
		exit(EXIT_FAILURE) ;
	}

	GenerateList(list, n) ;
	Sort(list, n) ;

	//Show Sorted
	for(i=0; i < n; i++)
		printf("%d, ", list[i]) ;

	return 0 ;
}

void GenerateList(int * pListHead, int n)
{
	int i;
	for(i=0; i < n; i++)
	{
		printf("%d, ",*(pListHead+i) = rand() % 1000) ;
	}
	printf("\n") ;
}

void Sort(int list[], int n)
{
	int i, j, min, temp ;

	for(i=0; i < n; i++)
	{
		min = i ;
		for(j=i+1; j< n; j++)
		{
			if(list[j] < list[min])
				min = j ;
		}
		SWAP(list[i],list[min],temp) ;
	}
}