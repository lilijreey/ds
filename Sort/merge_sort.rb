=begin
归并排序, 有两种实现方式
1. 自底向上
2. 自顶向下

归并排序是分治算法的思想,
排序稳定
    
算法:
    将待排序的线性表不断地切分成若干个子表，直到每个子表只包含一个元素，这时，可以认为只包含一个元素的子表是有序表。
    将子表两两合并，每合并一次，就会产生一个新的且更长的有序表，重复这一步骤，直到最后只剩下一个子表，这个子表就是排好序的线性表。

=end


def merge_sort_top_down(a)
    Assert(a.class == Array, __LINE__)
    ## 递归分割数组,直到只有一个元素为止
    if (a.length > 1)
        len = a.length
        l = merge_sort_top_down(a[0 ... len/2])
        r = merge_sort_top_down(a[len/2 .. -1])
        ## 开始归并
        AssertNot(l.empty? || r.empty?, __LINE__)
        out = []
        while (!l.empty? && !r.empty?)
            if (l.first < r.first)
                out << l.shift ## TODO 就低排序,最小化内存分配
            elsif (l.first == r.first)
                out << l.shift << r.shift
            else
                out << r.shift
            end
        end
        out +=  l if (not l.empty?)
        out +=  r if (not r.empty?)
        # puts "out:#{out.to_s}"
        return out
    end
    AssertNot(a.empty?, __LINE__)

    return a
end
## 迭代法
def merge_sort_down_top(_a)
    a = _a.dup
=begin
    从2 开始 ->4->8 .. 2^n > a.len
    每次最后一组长度为len%2*n 不进行归并
    直到最后一次与2^n len%2^n 进行归并
=end
    out=[]
    step = 1
    while step < a.length
        n=0
        loop { ##each step
        #0...step step .. [2*step, a.length].min
        lbegin = l = n*step
        r = (n+1) *step
        lend = [r, a.length].min
        rend = [(n+2)*step, a.length].min
        break if l >= a.length || r >= a.length

        out.clear
        while (l < lend && r < rend)
            if (a[l] < a[r])
                out << a[l]; l+=1
            elsif (a[l] == a[r])
                out << a[l] << a[r]
                l+=1;r+=1
            else
                out << a[r]; r+=1
            end
        end
        out += a[l...lend] if l != lend
        out += a[r...rend] if r != rend
        ## 回填到a
        # AssertEq(out.length, rend - lbegin)
        a[lbegin...rend] = out[0..-1]
        # printf("out:%s a:%s n:%d l:%d lend:%d r:%d rend:%d\n",out.to_s, a.to_s, n, l, lend, r, rend)
        n+=2 ##move to next units
        }
        step *=2
        # puts "========== step:#{step}"
    end
    return a
end

def Assert(exp, line)
    if (!exp)
        raise "Assert failed line:#{line}\n"
    end
end

def AssertEq(a,b)
    if (a != b)
        print "Assert failed: a:#{a} != b:#{b}\n"
        exit 1
    end
end

def AssertNot(exp, line)
    if (exp)
    print "AssertNot failed line:#{line} \n"
    exit 1
    end
end

# a = [6, 33, 2, 7, 13, 5, 14, 8, 9,100,53, 22]
require 'benchmark'
100.times {
    t = Array.new(rand(1000)) {rand(1000)}
    AssertEq(merge_sort_top_down(t), t.sort)
    AssertEq(merge_sort_down_top(t), t.sort)
}
puts "all passed"

Benchmark.bm { |b|
  a =  Array.new(1111) {rand(1000)}
    puts "top_down cost"
  b.report { merge_sort_top_down(a) }
    puts "down_top cost"
  b.report { merge_sort_down_top(a) }
} 


