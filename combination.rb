## 组合
=begin
生成 参考全排列的字典生成算法,类似的可以完成组合的生成
  
e.g. 12345  选3的所有组合,由于组合不关系顺序,
所以 123, 132,312, 是一种,所有我们只允许递增数列,或者无逆序数列
     因为一旦有逆序出现,则说明之前可定已经出现过该逆序的顺序组合,重复了
     e.g. 123 132 312 后面两种不允许出现
     也是寻找下一个组合, 下一个组合定义为使当前组合递增最小的组合
     e.g 123  -> 124 -> 125 -> 134 -> 135 -> 145 -> 234 -> 235 .... -> 345
=end

require_relative './assert.rb'

def gen_set(n)
  ret = []
  n.times { |i|
    ret << i
  }
  return ret
end

def dict_r_combination(a, r)
  count=0
  ## 保持递增性质, 下一组合是当前队列最后一个可以递增的数字,并且递增后不产生逆序数
  #  对于第i位(0开始),他的最大值为 (len-r) + i
  #  每次更新后, 该位的后面数字重置为n+1, n+2, ....

  a.sort!
  r > a.length and return nil
  n = a[0...r]
  #   e.g 123  -> 124 -> 125 -> 134 -> 135 -> 145 -> 234 -> 235 .... -> 345
  loop { catch :show do
      count +=1
      puts n.to_s
      #从后往前遍历每位是否还能增加,第i位的最大值为a[i+ a.len-r]
      (r-1).downto(0).each {|i|
        i > 0 and Assert(n[i] > n[i-1])
        #printf("i:%d n[i]:%d a:%d \n", i, n[i], a[-(r-i)])
        if n[i] < a[-(r-i)]
          x = n[i]
          #自增1,重置i后面的所有数字为
          (i...r).each { |j|
            x += 1
            n[j] = x 
          }
          throw :show
        end
        i == 0 and return count
      }
    end }#catch loop 
end

def C(n, r)
  ret = 1
  
  r.times {ret *= n; n-=1}
  #再除以排列数
  r.times {ret /= r; r-=1}

  return ret
end

AssertEq(dict_r_combination(gen_set(5),3), C(5,3))
AssertEq(dict_r_combination(gen_set(5),5), C(5,5))
AssertEq(dict_r_combination(gen_set(5),1), C(5,1))
puts 'all passed'
