#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define EXIT_FAILURE 1
#define MAX_SIZE 100
#define SWAP(x,y,t)  ((t) = (x), (x) = (y), (y) = (t))

#define COMPARE(x,y) (((x) < (y)) ? -1 : ((x) == (y)) ? 0 : 1)

void GenerateList(int[], int) ; 
void Sort(int[], int) ;

//�۰���Һ���
int Binsearch(int[], int, int, int) ;


int main()
{
	int n=16, i ;
	int searchnum, find ;
	int list[MAX_SIZE] ;

	GenerateList(list, n) ;
	Sort(list, n) ;

	//Show Sorted
	for(i=0; i < n; i++)
		printf("%d, ", list[i]) ;
	printf("\nEnter a search number: ");
	scanf("%d", &searchnum) ;

	if((find = Binsearch(list, searchnum, 0, n-1)) == -1) 
		printf("Not search the number.") ;
	else
		printf("Find the number, the number index is %d.", find) ;

	return 0 ;
}

void GenerateList(int * pListHead, int n)
{
	time_t t ;
	int i;
	srand(time(&t)) ;
	for(i=0; i < n; i++)
	{
		*(pListHead+i) = rand() % 1000 ;
	}
}

void Sort(int list[], int n)
{
	int i, j, min, temp ;

	for(i=0; i < n; i++)
	{
		min = i ;
		for(j=i+1; j< n; j++)
		{
			if(list[j] < list[min])
				min = j ;
		}
		SWAP(list[i],list[min],temp) ;
	}
}


int Binsearch(int list[], int searchnum, int left, int right)
{
	while(left <= right)
	{
		int middle = (left + right) /2 ;

		switch( COMPARE(searchnum, list[middle]) )
		{
		case -1:
			right = middle - 1 ;
			break ;
		case 0:
			return middle;
		case 1:
			left = middle + 1 ;
		}
	}
	return -1 ;
}