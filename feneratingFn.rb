=begin
母函数,多项式计算
* 整数拆分
   几个乘法对应几个有几个元素, 括号里的包是元素的可能大小,
   比如, 两个筛子可以有多少种点数,每种点数有几种组合

 G(x) = ( 1 + x + x^2 + x^3 + ... + x^6 )*(1 +x, x^2 + ...+ x^6) 
   三个塞子就是成3次
 G(x) = ( 1 + x + x^2 + x^3 + ... + x^6 )*(1 +x, x^2 + ...+ x^6) 
 没
=end

# 每个项用hash[m] -> a 来表示x的系数和幂
# 也可用array[a] index为对应的幂,方便相同幂合并

require_relative 'assert.rb'

#return dirc= [[1,0],[1,1],[1,2],[1,3]...[1,n]]
def gen_x(n)
  ret = {}
  (0..n).each {|i|
    ret[i]=1
  }
  return ret
end

#return string -> 类似数学符号输出
def show_pp(x)
  ret = ""
  a = x.sort
  a.each { |(m,a)|
    ret << a.to_s << 'x^' << m.to_s << ' + '
  }
  ret.delete_suffix!(' + ')
  puts ret
end

AssertEq(gen_x(3), {0=>1, 1=>1, 2=>1, 3=>1})

def mul(a, times)
  ## 依次相乘
  ret = a.dup
  #( 1 + x + x^2 + x^3)*(1 + x,  x^2 + x^3) 
  #( 1 + x + x^2)*(1 + x,  x^2) 
  (times-1).times {
    ## 笛卡尔积, 各项依次相乘
    nextRet={}
    ret.each {|mx, ax|
      ## 系数相乘,幂数相加
      a.each { |my, ay|
        m = mx + my
        nextRet[m] ? 
          nextRet[m] += ax * ay : 
          nextRet[m] = ax * ay
      }
    }
    ret = nextRet
  }
  return ret
end

def mulm(as)
  ## 依次相乘
  #( 1 + x + x^2 + x^3)*(1 + x,  x^2 + x^3) 
  #( 1 + x + x^2)*(1 + x,  x^2) 
  ret = as.first
  (as[1..-1]).each { |a|
    ## 笛卡尔积, 各项依次相乘
    nextRet={}
    ret.each {|mx, ax|
      ## 系数相乘,幂数相加
      a.each { |my, ay|
        m = mx + my
        nextRet[m] ? 
          nextRet[m] += ax * ay : 
          nextRet[m] = ax * ay
      }
    }
    ret = nextRet
  }
  return ret
end

show_pp mul(gen_x(4), 2)
##return array(dict)
def trans(*as)
  return as.map {|a|
  #[[1,0],[1,2]])
    e = {}
    a.each {|a,m|
      e[m]=a
    }
    e
  }
end

#puts trans([[1,0], [1,4]],[[1,0], [1,4]]).to_s
show_pp mulm( trans([[1,0],[1,1]],[[1,0],[1,2]],[[1,0],[1,3]], [[1,0],[1,4]]))
show_pp mulm( trans([[1,0],[1,1]],[[1,0],[1,1]],[[1,0],[1,1]],
      [[1,0],[1,2]],[[1,0],[1,2]],[[1,0],[1,2]],[[1,0],[1,2]],
      [[1,0],[1,4]], [[1,0],[1,4]]))

