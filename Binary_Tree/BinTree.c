
#include "BinTree.h"
#include <stdio.h>
#include <stdlib.h>

void InitTree(pBinTree bTree)
{
		bTree->lenght = 0 ;
		bTree->size = TREE_INIT_SIZE ;
		if((bTree->HeadList =(pBinTNode *) malloc(TREE_INIT_SIZE * sizeof(pBinTNode))) == (pBinTNode *)NULL)
		{
				perror("Malloc Memory error!") ;
				exit(1) ;
		}
}



int If_Full_Tree_List(pBinTree bTree)
{
		if(bTree->lenght == bTree->size)
				return SUCCESS ;
		else
				return FAIL ;
}


int If_Empty_Tree_List(pBinTree bTree)
{
		if(bTree->lenght == 0)
				return SUCCESS ;
		else
				return FAIL ;
}


void Create_Bin_Tree_Interface(pBinTree bTree)
{ 
		TElemType data ;
		printf("Enter Binary Tree\n: ")  ;
		scanf("%c", &data) ; //消除回车
		scanf("%c", &data) ;
		
		//创建头结点
		if((bTree->HeadList[0] = (pBinTNode)malloc(sizeof(BinTNode))) == (pBinTNode)NULL)
		{
				perror("Malloc Memory error!") ;
				exit(1) ;
		}
		bTree->HeadList[0]->data = data ;
		bTree->HeadList[0]->parent = NULL ;


		//把头结点线性表的对应头结点指针传给
		//构造函数
		Create_Bin_Tree(&(bTree->HeadList[0]->lchild), bTree->HeadList[0]) ; //注意
		Create_Bin_Tree(&(bTree->HeadList[0]->rchild), bTree->HeadList[0]) ;
		
	
}

void Create_Bin_Tree(pBinTNode *bNode, pBinTNode f) 
{
		TElemType data ;
		scanf("%c", &data) ;
		if(data == '#')
		{		
				*bNode = NULL ;
		}
		else
		{
				if((*bNode = (pBinTNode)malloc(sizeof(BinTNode))) == (pBinTNode)NULL)
				{
						perror("Malloc Memory error!") ;
						exit(1) ;
				}
				//链接双亲
				(*bNode)->data = data ;
				(*bNode)->parent = f ;
				Create_Bin_Tree(&((*bNode)->lchild), *bNode) ;
				Create_Bin_Tree(&((*bNode)->rchild), *bNode) ;
		}
}


void Clear_Bin_Tree(pBinTNode bNode) 
{
		if(bNode->lchild != NULL)
		{
				Clear_Bin_Tree(bNode->lchild) ;		
		}
		if(bNode->rchild != NULL)
				Clear_Bin_Tree(bNode->rchild) ;

		printf(" %c ", bNode->data) ;

		free(bNode) ;
}


//7先序显示
void Pre_Order_Show(pBinTNode bNode) 
{
		printf("%c, ", bNode->data) ;
		if(bNode->lchild != NULL)
				Pre_Order_Show(bNode->lchild) ;
		if(bNode->rchild != NULL)
				Pre_Order_Show(bNode->rchild) ;
}

//8中序显示
void In_Order_Show(pBinTNode bNode) 
{
		//先遍历到最左子数
		if(bNode->lchild != NULL)
				In_Order_Show(bNode->lchild) ;
		printf("%c, ", bNode->data) ;
		if(bNode->rchild != NULL)
				In_Order_Show(bNode->rchild) ;
}

//9后序显示
void Post_Order_Show(pBinTNode bNode) 
{
		if(bNode->lchild != NULL)
				Post_Order_Show(bNode->lchild) ;
		
		if(bNode->rchild != NULL)
				Post_Order_Show(bNode->rchild) ;
		printf("%c, ", bNode->data) ;
}

void Three_Order_Show(pBinTNode bNode) 
{
		printf("PerOrder: ") ;
		Pre_Order_Show(bNode) ;

		printf("\nInOrder: ") ;
		In_Order_Show(bNode) ;

		printf("\nPostOrder: ") ;
		Post_Order_Show(bNode) ;
		printf("\n") ;
}

//函数11的显示界面
void Get_TreeDepth_Interface(pBinTNode bNode) 
{
		size_t max = 0, depth = 0;
		Get_Tree_Depths(bNode, &max, &depth) ;
		printf("\n树的深度为：%d", max - 1) ;
}


//11返回树的深度
void Get_Tree_Depths(pBinTNode bNode, size_t *max, size_t *depth) 
{
		(*depth)++ ;
		if(bNode->lchild != NULL)
		{
				
				Get_Tree_Depths(bNode->lchild, max, depth) ;
				--(*depth) ;
		}
		else
		{
				if(*max < *depth)
						*max = *depth ;
		}
		
		
		if(bNode->rchild != NULL)
		{
				Get_Tree_Depths(bNode->rchild, max, depth) ;
				--(*depth) ;
		}
		else
		{
				if(*max < *depth)
						*max = *depth ;

		}
}


