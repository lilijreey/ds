
#ifndef BINTREE_H_
#define BINTREE_H_

#include "Bin_Type_Def.h"
#include <stdio.h>
#include <stdlib.h>

////////////////////////////////////////////////////////

#define TREE_INIT_SIZE 5 //头结点线性表的初始大小
#define TREEINCREMENT 5  //线性表的增量

//////////////////////////////////////////////////////////


typedef struct BinTreeNode  //二叉树节点定义	//三叉链					
{
		TElemType data ;
		struct BinTreeNode *lchild ;  //left child
		struct BinTreeNode *rchild ;  //right child
		struct BinTreeNode *parent ;  // parent node

}BinTNode, *pBinTNode ;


//二叉树的定义头结点的线性表
typedef struct BinTree  
{
		int lenght ;//实际二叉树的个数
		int size ; //线性表的空间 初始化大小
		//定义一个二叉树 头结点指针 的线性表
		//里面放的是指向每一个二叉树 的头结点 的节点指针
		pBinTNode *HeadList ; //注意类型pointer of head node pointer List
}BinTree, *pBinTree ;



////////////////////////////Binary_Tree_ADT///////////////////////////////////
//1构造空树：即Bintree的初始化
void InitTree(pBinTree bTree) ;

//2判断头结点表是否已满
int If_Full_Tree_List(pBinTree bTree) ;

//3判断头结点表是否为空
int If_Empty_Tree_List(pBinTree bTree) ;

//4创建二叉树，交互界面
void Create_Bin_Tree_Interface(pBinTree bTree) ;

//5创建二叉树，以节点先序输入， 以‘#’为空
void Create_Bin_Tree(pBinTNode *bNode, pBinTNode f) ;

//6删除一个数//后续删除
void Clear_Bin_Tree(pBinTNode bNode) ;

//7先序显示
void Pre_Order_Show(pBinTNode bNode) ;

//8中序显示
void In_Order_Show(pBinTNode bNode) ;

//9后序显示
void Post_Order_Show(pBinTNode bNode) ;

//10三种一起显示
void Three_Order_Show(pBinTNode bNode) ;

//函数11的显示界面
void Get_TreeDepth_Interface(pBinTNode bNode) ;

//11返回树的深度
void Get_Tree_Depths(pBinTNode bNode, size_t *max, size_t *depth) ;



#endif



