#include <stdio.h>
#include <stdlib.h>
#include "BinTree.h"
#include "Bin_Tree_Show.h"

void Init_Show_List(ShowList *sl) 
{
		sl->showNList = 0 ;
		if((sl->showNList = (pShowNode)malloc(SHOW_INIT_SIZE * sizeof(ShowNode))) == (pShowNode)NULL)
		{
				perror("malloc memory error!") ;
				exit(1) ;
		}
		sl->size = SHOW_INIT_SIZE ;
		sl->lenght =0 ;
		sl->level = -1 ;
}


//2验满 增加
int If_Show_List_Full(ShowList *sl) 
{
		return sl->lenght == sl->size ;
}

//3验空
int If_Show_List_Empty(ShowList *sl) 
{
		return sl->lenght == 0 ;
}

//为了解决
void Create_Show_Level_Interface(ShowList *sl, pShowNode sN, pBinTNode bNode)
{
		if(sl->lenght != 0)
		{
				sl->lenght = 0 ;
				sl->level = -1 ;
		}

		Create_Show_Level(sl, sN, bNode) ;
}

//4创建函数每一个节点的level值 首次调用是要让root-level = -1
void Create_Show_Level(ShowList *sl, pShowNode sN, pBinTNode bNode) 
{
		//值头结点level-1

		//printf("DATA: %c____level: %d\n", bNode->data, sl->level) ;
	    ++sl->lenght ;
		++sl->level ;
		
      	sN->level = sl->level ;
		sN->nodeAdd = bNode ;


	
		if(bNode->lchild != NULL)
		{
				Create_Show_Level(sl, &(sl->showNList[sl->lenght]), bNode->lchild) ;
				--sl->level ;
		}
		if(bNode->rchild != NULL)
		{
				Create_Show_Level(sl, &(sl->showNList[sl->lenght]), bNode->rchild) ;
				--sl->level ;	
		}	
}

void Sequence_Show_Level(ShowList *sl) 
{
		//快速排序
		int i, j ;
		int min  ;
		ShowNode temp ;
		for(i = 0; i < sl->lenght -1; ++i)
		{
				min = i ;
				for(j = i+1; j < sl->lenght; ++j)
				{
						if(sl->showNList[min].level > sl->showNList[j].level)
						{
								min = j ;
						}
				}
				//swap/////////
				if(min != i)
				{
						temp.id = sl->showNList[i].id ;
						temp.level = sl->showNList[i].level ;
						temp.nodeAdd = sl->showNList[i].nodeAdd ;

						sl->showNList[i].id = sl->showNList[min].id ;
						sl->showNList[i].level = sl->showNList[min].level;
						sl->showNList[i].nodeAdd = sl->showNList[min].nodeAdd;

						sl->showNList[min].id = temp.id ;
						sl->showNList[min].level = temp.level ;
						sl->showNList[min].nodeAdd = temp.nodeAdd ;
				}

		}
}


void Level_Show_Tree_Interface(ShowList *sl, pBinTNode bNode) 
{
		int i ;
		int level = -1;

		Create_Show_Level_Interface(sl, sl->showNList, bNode) ;
		Sequence_Show_Level(sl) ;

		//show
		
		for(i = 0; i < sl->lenght; ++i)
		{
				if(sl->showNList[i].level != level)
				{
						level = sl->showNList[i].level ;
						printf("\n%d_Level: ", level) ;	
				}
				printf("%c, ", sl->showNList[i].nodeAdd->data) ;
		}
}

void aa()
{}