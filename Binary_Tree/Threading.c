#include "Threading.h"




void Copy_Tree(pBiThrNode *head, pBinTNode b) //head &(head->lchild)
{//先序 有些难想
		if(b->lchild != NULL)
		{
				if(((*head) = (pBiThrNode) malloc(sizeof(BiThrNode))) == NULL)
				{
						perror("Malloc memory error!") ;
						exit(1) ;
				}
				(*head)->data = b->data ;	
				Copy_Tree(&((*head)->lchild), b->lchild) ;
		}
		else
		{
				if(((*head) = (pBiThrNode) malloc(sizeof(BiThrNode))) == NULL)
				{
						perror("Malloc memory error!") ;
						exit(1) ;
				}
				(*head)->data = b->data ;	
				(*head)->lchild = NULL ;
		}
		
		if(b->rchild != NULL)
		{
				Copy_Tree(&((*head)->rchild), b->rchild) ;
		}
		else
				(*head)->rchild = NULL ;
}


void Thearding_Init(pBiThrNode thr, pBinTNode b) 
{
		Copy_Tree(&(thr->lchild), b) ;  //复制二叉树
}


void In_Threading_Interface(pBiThrNode thr)
{
		pBiThrNode head = thr ;

		thr->LTag = Thread ;//初始化头结点
		thr->RTag = Thread ;

		//
		In_Thearding(thr->lchild, &thr) ; //root, head
		//由于参数是一个引用传递，使用thr返回最后一个节点
		head->rchild = thr ;
		thr->rchild = head ;
		printf("中序线索化成功！\n") ;
}

//2建立中序线索化二叉树,以递归方式
void In_Thearding(pBiThrNode p, pBiThrNode *per) //thr用于endNode的链接
{
		//中序遍历b生成b的线索化二叉树
		if(p->lchild != NULL)
		{
				In_Thearding(p->lchild, per) ;
		//做动作
				p->LTag = Link ;
		}
		else
		{
				p->LTag = Thread ;
				p->lchild = *per ;		
		}
		if((*per)->RTag == Thread)
				(*per)->rchild = p ;
		*per = p ;
		
		if(p->rchild != NULL)
		{
				In_Thearding(p->rchild, per) ;
				p->RTag = Link ;
		}
		else
		{
				(*per)->RTag = Thread ;		
		}
}

void InOrder_Traverse(pBiThrNode head) //give head
{
		pBiThrNode p = head->lchild ;

		while(p != head)
		{
				//找到以p为根节点的最左叶子
				while(p->LTag == Link)
				{
						p = p->lchild ;
				}
				//找到最左叶子
				printf("%c ", p->data) ;

				//调整节点
				while(p->RTag == Thread && p->rchild != head) //无右子树
				{
						p = p->rchild ;
						printf("%c ", p->data) ;
				}
				p = p->rchild ;
		}
}


int Thread_Bin_Tree_Interface(pBinTNode b)  
{
		BiThrNode head ;
		Thearding_Init(&head, b) ; 

		
		while(1)
		{
				int select ;
				Thread_Menu() ;
				scanf("%d", &select) ;

				switch(select)
				{
				case 0:
						return 0 ;
						
				case 1:
						In_Threading_Interface(&head) ;
						break ;

				case 2:
						InOrder_Traverse(&head) ;
						break ;

				default:
						printf("选择错误") ;
						fflush(stdin) ;
						break ;
				}
		}
}

void Thread_Menu(void) 
{
	printf("\n***************线索化二叉树*****************\n") ;
	printf(  "*1 中序线索化               2中序线索遍历  *\n") ;
	printf(  "*3 --------------------------------------  *\n") ;
	printf(  "*                    0 退出                 *\n") ;
	printf(  "*********************************************\n") ;
	printf("请选择：") ;
}