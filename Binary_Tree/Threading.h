#ifndef THREADING_H_
#define THREADING_H_

#include "Bin_Type_Def.h"
#include "BinTree.h"


typedef enum {Link, Thread} PointerTag ; //标志域

typedef struct BiThrNode
{
		TElemType data ;
		struct BiThrNode * lchild ;
		struct BiThrNode * rchild ;
		PointerTag LTag ;
		PointerTag RTag ;
}BiThrNode, *pBiThrNode ;


//增加的头结点的lchild指向二叉树的根节点，rchild指向最后一个节点
//并且令中序序列中的第一个节点的lchild和最后一个NODE的rchild指向头结点
//以便形成循环线索表 

//由于线索化是在已经建立好的二叉树上进行的
//使用先要复制一个二叉树
//拷贝二叉树 //从 BinTNode 到 BiThrNode
void Copy_Tree(pBiThrNode *head, pBinTNode b) ;

//1
//拷贝构造线索化二叉树
void Thearding_Init(pBiThrNode thr, pBinTNode b) ;

//2的交互界面，初始化head节点
void In_Threading_Interface(pBiThrNode thr) ;

//2建立中序线索化二叉树,以递归方式
void In_Thearding(pBiThrNode p, pBiThrNode *per) ;

//3中序线索遍历
void InOrder_Traverse(pBiThrNode p) ;


int Thread_Bin_Tree_Interface(pBinTNode b) ;
void Thread_Menu(void) ;

#endif