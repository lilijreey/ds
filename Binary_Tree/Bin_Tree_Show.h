
#ifndef BIN_TREE_SHOW_H_
#define BIN_TREE_SHOW_H_

#define SHOW_INIT_SIZE 20 //没写拷贝，增加等函数。 一次开辟够
#define SHOWINCREMENT 20

#include "BinTree.h"
//建立一个线性表，他将记录给定二叉树的每个节点的编号
//节点所在层数，根为0层，和节点的地址


enum way {root ,left, right } ;//方向常量

typedef struct Show_Tree_Node
{
		int id ;  //node id
		int level ; //node in level
		pBinTNode nodeAdd ; //Node address
}ShowNode, *pShowNode;

//线性表，节点大数量， 线性表的大小

typedef struct Show_Tree_List
{		
		int level ; //层数

		int size ;
		int lenght ;
		pShowNode showNList ;
}ShowList;


//1初始化Show_Tree_List
void Init_Show_List(ShowList *sl) ;
//2验满 增加
int If_Show_List_Full(ShowList *sl) ;

//3验空
int If_Show_List_Empty(ShowList *sl) ;

//4创建函数每一个节点的level值 
void Create_Show_Level(ShowList *sl, pShowNode sN, pBinTNode bNode) ;
//确保函数4的 正常
void Create_Show_Level_Interface(ShowList *sl, pShowNode sN, pBinTNode bNode) ;

//5对节点的level值排序 从小到大
void Sequence_Show_Level(ShowList *sl) ;

//6按行输出
void Level_Show_Tree_Interface(ShowList *sl, pBinTNode bNode) ;





#endif