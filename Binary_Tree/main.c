#include "BinTree.h"
#include "Bin_Tree_Show.h"
#include "Threading.h"

#include <stdio.h>

void menu(void) ;
//void Thread_Bin_Tree(pBinTNode b) ;//case 6 进入线索化
//void Thread_Menu(void) ;

int main()
{
		BinTree bTree ;
		ShowList sList ;

		InitTree(&bTree) ;
		Init_Show_List(&sList) ;



		while(1)
		{
				int select ;
			
				menu() ;
				scanf("%d", &select) ;
				switch(select)
				{
						case 1:
								Create_Bin_Tree_Interface(&bTree) ;
								break ;

						case 2:
								Clear_Bin_Tree(bTree.HeadList[0]) ;
								break ;

						case 3:
								Three_Order_Show(bTree.HeadList[0]) ;
								break ;
						case 4:
								Level_Show_Tree_Interface(&sList, bTree.HeadList[0]) ;
								break ;

						case 5:
								Get_TreeDepth_Interface(bTree.HeadList[0]) ;
								break ;

						case 6:
								Thread_Bin_Tree_Interface(bTree.HeadList[0]) ;
								break ;

						case 0:
								return 0 ;
								
						default:
								printf("选择错误") ;
								fflush(stdin) ;
								break ;
				}
		}
}


void menu(void)
{
	printf("\n************************************************\n") ;
	printf(  "*1 建立一个二叉树           2 删除二叉树       *\n") ;
	printf(  "*3 以三种遍历显示           4 层序输出         *\n") ;
	printf(  "*5 显示树的深度             6 线索化二叉树     *\n") ;
	printf(  "*7 删除数据                 8 查找数据         *\n") ;
	printf(  "*                    0 退出                    *\n") ;
	printf(  "************************************************\n") ;
	printf("请选择：") ;
}


