#include <stdio.h>
#include <stdlib.h>

#define MAXSIZE  10
enum status {FAIL, SUCCESS} ;

///////////////////////自己约定的与书上的不太一样
//front = rear =-1 为空
///front在rear的下一位为满
///初始front=rear=-1
///

typedef struct
{
		int *base ;
		int front ;
		int rear ;
}CQueue, *pCQueue ;


void menu(void) ;
//////////////////////circual queue 的基本操作
//1 建立一个空循环队列
void Create_CQueue_C(pCQueue) ;

// 2 判空函数
int If_Empty_C(pCQueue) ;

//3 判满函数
int If_Full_C(pCQueue) ;

//4 返回队列的长度
int Lenght_Queue_C(pCQueue) ;

//5 函数4的交互界面
void Lenght_Queue_Interface_C(pCQueue) ;

//6 插入元素
int Input_Queue_C(pCQueue, int) ;

//7 函数6的交互界面
void Input_Queue_Interface_C(pCQueue) ;

//8 元素出队删除成功返回SUCESS
int Output_Queue_C(pCQueue, int*) ;

//9 函数8的交互界面
void Output_Queue_Interface_C(pCQueue) ;

// 10 显示队列
void Show_Queue_C(pCQueue) ;

int main()
{
		int select ;
		CQueue cQueue ;

		Create_CQueue_C(&cQueue) ;


		while(1)
		{
				menu() ;
				scanf("%d", &select) ;

				switch(select)
				{
				case 0:
						return 0 ;

				case 1:
						Lenght_Queue_Interface_C(&cQueue) ;
						break ;

				case 2:
						Input_Queue_Interface_C(&cQueue) ;
						break ;

				case 3:
						Output_Queue_Interface_C(&cQueue) ;
						break ;

				case 4:
						Show_Queue_C(&cQueue) ;
						break ;

				default:
						printf("\n无效输入\n") ;
						break ;
				}
		}
}

void menu(void)
{
	printf("\n*********************************************\n") ;
	printf(  "*1 显示队列长度              2 数据入队列   *\n") ;
	printf(  "*3 数据出队                  6 显示队列     *\n") ;
	printf(  "*                  0 退出                   *\n") ;
	printf(  "*********************************************\n") ;
	printf("请选择：") ;
}


void Create_CQueue_C(pCQueue pQueue) 
{
		if((pQueue->base = (int*)malloc(MAXSIZE * sizeof(int))) == (int*)NULL)
		{
				printf("\nMemory error!\n") ;
				exit(1) ;
		}
		pQueue->front = pQueue->rear = -1 ;
}

int If_Empty_C(pCQueue pQueue)
{
		if(-1 == pQueue->front)
				return SUCCESS ;
		else
				return FAIL ;
} 


int If_Full_C(pCQueue pQueue)
{
		if((pQueue->rear + 1) % MAXSIZE == pQueue->front)
				return SUCCESS ;
		else
				return FAIL ;
}
 

int Lenght_Queue_C(pCQueue pQueue) 
{
		int lenght ;

		if(SUCCESS == If_Empty_C(pQueue))
		{
				return 0 ;
		}
		else
		{
				//除空以外通用
				if((lenght = pQueue->rear - pQueue->front + 1) > 0)
						return lenght ;
				else
						return lenght + MAXSIZE ;			
		}
}

void Lenght_Queue_Interface_C(pCQueue pQueue)
{
		printf("队列的长度为：%d\n", Lenght_Queue_C(pQueue)) ;
}

int Input_Queue_C(pCQueue pQueue, int data) 
{
		//要看满了没还要看是否是第一次入队
		if(SUCCESS == If_Empty_C(pQueue))
		{
				pQueue->front = pQueue->rear = 0 ;	
		}
		else
		{
				if(SUCCESS == If_Full_C(pQueue))
				{
						printf("\n队列已满，无法输入！\n") ;
						return FAIL ;
				}
				else
				{
						pQueue->rear = (pQueue->rear + 1) % MAXSIZE ;
				}
		}
		pQueue->base[pQueue->rear] = data ;
		return SUCCESS ;
}

void Input_Queue_Interface_C(pCQueue pQueue)
{
		int data ;

		printf("输入数据，以CTRL+E结束\n") ;
		while(scanf("%d", &data))
		{
				if(SUCCESS == If_Full_C(pQueue))
				{
						printf("\n队列已满，无法输入！\n") ;
						break ;
				}
				Input_Queue_C(pQueue, data) ;
		}

		fflush(stdin) ;
} 

int Output_Queue_C(pCQueue pQueue, int* data) 
{
		if(SUCCESS == If_Empty_C(pQueue))
				return FAIL ;

		*data = pQueue->base[pQueue->front] ;
		if(pQueue->front == pQueue->rear)
		{		
				pQueue->front = pQueue->rear = -1 ;
		}
		else
		{
				pQueue->front = (pQueue->front + 1) % MAXSIZE ;
		}
		return SUCCESS ;
}


void Output_Queue_Interface_C(pCQueue pQueue) 
{
		int data ;

		if(SUCCESS == Output_Queue_C(pQueue, &data) && FAIL == If_Empty_C(pQueue))
				printf("成功删除%d\n", data) ;
		else
				printf("队列为空，无法删除！\n") ;
}

void Show_Queue_C(pCQueue pQueue) 
{
		int set ;

		if(SUCCESS == If_Empty_C(pQueue))
				printf("\n队列为空\n") ;
		else
		{
				set = pQueue->front ;
				while(set != pQueue->rear)
				{
						printf("%d ", pQueue->base[set]) ;
						set = (set + 1) % MAXSIZE ;
				}
				printf("%d", pQueue->base[set]) ;
		}
}