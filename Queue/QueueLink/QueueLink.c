#include <stdio.h>
#include <stdlib.h>


enum status {FAIL, SUCCESS} ;


typedef struct sQNode
{
		int    data ;
		struct sQNode *next ;
}QNode, *pQNode ;


typedef struct 
{
		int lenght ;//仅用于返回长度
		pQNode front ;
		pQNode rear ;
}LinkQueue, *pLinkQueue ;

void menu(void) ;
//////////////////////队列的基本操作
///////队列是 头出尾进 FIFO的线性表
///////定义front==rear==NULL为空队列，也是初始值
//1 构造一个空队列
void InitQueue(pLinkQueue) ;

//2 销毁队列把队列FREE掉
void DistroyQueue(pLinkQueue) ;

//3 清空队列只对于线性表才有用
//  即把头尾一样
//void ClearQueue(pLinkQueue) ;

//4 检测队列是否为空
int If_Empty(pLinkQueue) ;

//5 返回队列的长度
int Queue_Lenght(pLinkQueue) ;

//6 函数5的交互界面
void Queue_Lenght_Interface(pLinkQueue) ;

//7 返回队列的头元素，成功返回SUCCESS
int GetHead(pLinkQueue, int*) ;

//8 函数7的交换界面
void GetHead_Interface(pLinkQueue) ;

//9 输入元素 从头进
void Input_Queue(pLinkQueue, int) ;

//10 函数9的交互界面
void Input_Queue_Interface(pLinkQueue) ;

//11 删除队尾元素并返回其值
int Delet_Queue(pLinkQueue, int *) ;

//12 函数11的交互界面
void Delet_Queue_Interface(pLinkQueue) ;

//13 显示队列
void ShowQueue(pLinkQueue) ;


int main()
{
		LinkQueue LQueue ;
		int select ;

		InitQueue(&LQueue) ;//建立队列

		while(1)
		{
				menu() ;
				scanf("%d", &select) ;

				switch(select)
				{
				case 0:
						return 0 ;

				case 1:
						DistroyQueue(&LQueue) ;
						break ;

				case 2:
						Queue_Lenght_Interface(&LQueue) ;
						break ;

				case 3:
						GetHead_Interface(&LQueue) ;
						break ;

				case 4:
						Input_Queue_Interface(&LQueue) ;
						break ;

				case 5:
						Delet_Queue_Interface(&LQueue) ;
						break ;

				case 6:
						ShowQueue(&LQueue) ;
						break ;

				default:
						printf("\n无效输入请重输.\n") ;
						break ;

				}
				fflush(stdin) ;
		}

}

void menu(void)
{
	printf("\n*********************************************\n") ;
	printf(  "*1 销毁队列                  2 显示队列长度 *\n") ;
	printf(  "*3 显示对头元素              4 数据入队列   *\n") ;
	printf(  "*5 数据出队                  6 显示队列     *\n") ;
	printf(  "*                  0 退出                   *\n") ;
	printf(  "*********************************************\n") ;
	printf("请选择：") ;
}


void InitQueue(pLinkQueue pLQueue) 
{

		pLQueue->rear = pLQueue->front = (pQNode)NULL ;
		pLQueue->lenght = 0 ;
}

int If_Empty(pLinkQueue pLQueue) 
{
		if((*pLQueue).front != (pQNode)NULL) 
				return SUCCESS ;
		else
				return FAIL ;

}


void DistroyQueue(pLinkQueue pQueue) 
{
		pQNode delNode ;
		
		if(If_Empty(pQueue) == SUCCESS)
		{
				while((*pQueue).front != (*pQueue).rear)
				{
						delNode = (*pQueue).front ;
						(*pQueue).front = (*pQueue).front->next ;
						free(delNode) ;
						//到队头和队尾都指向最后一个节点跳出
				}
				free((*pQueue).front) ;

				pQueue->rear = pQueue->front = (pQNode)NULL ;
				pQueue->lenght = 0 ;
		}
		else
				printf("\n队列为空\n") ;
}


int Queue_Lenght(pLinkQueue pLQueue)
{
		return pLQueue->lenght ;
}
 

void Queue_Lenght_Interface(pLinkQueue pLQueue)
{
				printf("队列长度为：%d", Queue_Lenght(pLQueue)) ;
}


int  GetHead(pLinkQueue pLQueue, int *data)
{
		if(If_Empty(pLQueue) == SUCCESS)
		{
				*data = (*pLQueue).front->data ;
				return SUCCESS ;
		}
		else
		{
				return FAIL ;
		}

}

void GetHead_Interface(pLinkQueue pLQueue) 
{
		int data ;

		if(GetHead(pLQueue, &data) == SUCCESS)
		{
				printf("队头元素为%d.", data) ;
		}
		else
		{
				printf("\n对为空\n") ;
		}
}


void Input_Queue(pLinkQueue pLQueue, int data) 
{
		pQNode newNode ;

		if((newNode = (pQNode)malloc(sizeof(QNode))) == (pQNode)NULL)
		{
				printf("\nMemory error!\n") ;
				exit(1) ;
		}
		newNode->data = data ;
		if(If_Empty(pLQueue) == FAIL)
		{
				(*pLQueue).rear = (*pLQueue).front = newNode ;
		}
		(*pLQueue).rear->next = newNode ;
		(*pLQueue).rear = newNode ;
		(*pLQueue).lenght++ ;
}


void Input_Queue_Interface(pLinkQueue pLQueue) 
{
		int data ;

		printf("请输入数据：以CTRL+E结束\n") ;

		while(scanf("%d", &data)) 
				Input_Queue(pLQueue, data) ;
		fflush(stdin) ;
}

int Delet_Queue(pLinkQueue pLQueue, int *data) 
{
		pQNode delNode ;

		if(If_Empty(pLQueue) == FAIL)
		{
				printf("\n空队列\n") ;
				return FAIL ;
		}
		else
		{
				*data = (*pLQueue).front->data ;
				delNode = (*pLQueue).front ;
				if((*pLQueue).front == (*pLQueue).rear)
				{
					(*pLQueue).front = (*pLQueue).rear = (pQNode)NULL ;
				}
				else
				{
						(*pLQueue).front = (*pLQueue).front->next ;
				}
				free(delNode) ;
				(*pLQueue).lenght-- ;
				return SUCCESS ;
		}

}
void Delet_Queue_Interface(pLinkQueue pLQueue) 
{
		int data ;

		if(Delet_Queue(pLQueue, &data) == SUCCESS)
		{
				printf("出队元素为：%d", data) ;
		}
}


void ShowQueue(pLinkQueue pLQueue) 
{
		pQNode nowNode ;

		if(If_Empty(pLQueue) == FAIL)
		{
				printf("\n空队列\n") ;
		}
		else
		{
				nowNode = (*pLQueue).front ;
				while(nowNode != (*pLQueue).rear) 
				{
						printf("%d ", nowNode->data) ;
						nowNode = nowNode->next ;
				}
				printf("%d ", nowNode->data) ;
		}
}