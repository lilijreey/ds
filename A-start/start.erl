%%%-------------------------------------------------------------------
%%% @author zl
%%% @doc
%%%
%%% @end
%%%-------------------------------------------------------------------

-module(start).

-compile([export_all]).

-define(make_info(S, G, H, PP), {S, G, H, PP}).
%% PP prevPoint
-define(point(P), {point, P}).

-define(G_HV, 10).
-define(G_DN, 14).

%%1424762,

%% HACK is fast then X * X
h({X1, Y1}, {X2, Y2}) ->
    (abs(X1-X2) + abs(Y1-Y2)) * ?G_HV.
    %%math:pow((X1 - X2) * ?G_HV, 2.0) + math:pow((Y1 - Y2) * ?G_HV, 2.0).

a_start_path(StrP, DstP) ->
    StrH = h(StrP, DstP),
    put(?point(StrP), ?make_info(open, 0, StrH, start)),
    find_path(gb_sets:insert({StrH, StrP}, gb_sets:empty()), DstP).

%% -> {error, no}
find_path(FStore, Dst) ->
    case gb_trees:is_empty(FStore) of
        true ->
            {false, FStore};
        false ->
            %% 使用链表还可以优化 order_list
            case gb_sets:take_smallest(FStore) of
                {{_, Dst}, _} ->
                    reconstruct_path(Dst, []);
                {{_, P}, NewFStore} ->
                    {Px, Py} =P,
                    {open, G, H, PP} = get(?point(P)),
                    %%io:format("~p insert close ~n", [P]),
                    put(?point(P), ?make_info(close, G, H, PP)),
                    F8=
                    check_neighbor({Px,Py-1}, ?G_HV+G, P, Dst,
                    check_neighbor({Px,Py+1}, ?G_HV+G, P, Dst,
                    check_neighbor({Px-1,Py}, ?G_HV+G, P, Dst,
                    check_neighbor({Px+1,Py}, ?G_HV+G, P, Dst,
                    check_neighbor({Px-1,Py-1}, ?G_DN+G, P, Dst,
                    check_neighbor({Px-1,Py+1}, ?G_DN+G, P, Dst,
                    check_neighbor({Px+1,Py-1}, ?G_DN+G, P, Dst,
                    check_neighbor({Px+1,Py+1}, ?G_DN+G, P, Dst, NewFStore)))))))), 
                    find_path(F8, Dst)
            end
    end.


reconstruct_path(P, Path) ->
    case get(?point(P)) of
        {_, _, _, start} ->
            [P | Path];
        {_, _, _, PP} ->
            reconstruct_path(PP, [P | Path])
    end.



%% -> new Open
check_neighbor(P, NewG, PP, E, FStore) ->
    case get(?point(P)) of
        undefined ->
            %%io:format("~p insert open  ~n", [P]),
                    H = h(P, E),
                    put(?point(P), ?make_info(open, NewG, H, PP)),
                    gb_sets:insert({NewG+H, P}, FStore);
            %%case scene:is_walkable(P) of
                %%true ->
                    %%H = h(P, E),
                    %%put(?point(P), ?make_info(open, NewG, H, PP)),
                    %%gb_sets:insert({NewG+H, P}, FStore);
                %%false ->
                    %%put(?point(P), block)
            %%end
        block -> FStore;
        {close, _, _, _} -> FStore;
        {open, G, H, _} ->
            if NewG < G ->
                   put(?point(P), ?make_info(open, NewG, H, PP)),
                   gb_sets:insert({NewG+H, P},
                                  gb_sets:delete({NewG+H, P}, FStore));
               true ->
                   FStore
            end
    end.

