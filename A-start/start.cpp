

void astar(Rule30 &map, int n)
{
    Point* start = new Point(n, 0);
    const Point goal = Point(n, n-1);

    std::multimap<int/*f*/, Point*> open;
    std::unordered_map<int64_t/*key*/, Point*> alls;

    open.emplace(start->f(goal), start);
    alls.emplace(start->key(),  start);

    while (not open.empty()) {
        auto it = open.begin();
        Point *p = it->second;

        if (p->is_same_pos(goal)) {
            printf("find path f:%u g:%u n:%d\n", p->f(goal), p->_g, n);
            break;
        }

        p->set_closed();
        open.erase(it);

        for (int i: {-1,0,1}) {
            const int nx = p->_x + i;
            const int ny = p->_y + 1;

            if (not map.is_black_pos(nx,ny))
                continue;

            auto nit = alls.find(Point::make_key(nx,ny));

            if (nit == alls.end()) { //new point
                Point *np = new Point(nx, ny);
                np->_g = p->_g + abs(i) + 1; //2,1,2

                alls.emplace(np->key(), np);
                open.emplace(np->f(goal), np);
                continue;
            } else if (nit->second->is_closed())
                continue;

            Point *np = nit->second;
            const uint32_t ng =  p->_g+abs(i)+1;
            if (ng < np->_g) { //update open
                auto range = open.equal_range(np->f(goal));
                auto it = find_if(range.first, range.second,
                                  [nx, ny](auto &e) { return e.second->_x == nx && e.second->_y == ny;});

                assert(it != range.second);
                open.erase(it);
                np->_g = ng;
                open.emplace(np->f(goal), np);
            }
        }
    }
}

