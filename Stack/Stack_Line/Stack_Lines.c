
#include <stdio.h>
#include <stdlib.h>

#define STACK_INIT_SIZE 10
#define STACKINCREMENT 10

enum status {FAIL, SUCCESS} ;

typedef struct sStack
{
		int *base ;//栈底指针
		int *top ;//栈顶指针
		int stacklenght ;//当前长度
		int stacksize ;//栈的大小
}Stack, *pStack;


void menu(void) ;
////////////////////ADT Stack 的基本操作
//1 构造一个空栈 base=top
void InitStack_S(pStack) ;


//3 销毁栈
void Distroy_Stack_S(pStack) ;

//4 清空栈
void Clear_Stack_S(pStack) ;

//5 检测栈是否为空
int If_Stack_Empty_S(pStack) ;

//6 返回栈顶元素
int Get_Stack_Top_S(pStack, int *) ;

//7 插入元素
void Push_S(pStack, int) ;

//8 函数7的交互界面
void Push_Stack_Interface_S(pStack) ;

//9 函数6的交互界面
void Get_Stack_Top_Interface_S(pStack) ;

//10 出栈返回出栈的元素
int Pop_S(pStack, int *) ;

//11 函数10的交互界面
void Pop_Stack_Interface_S(pStack) ;


int main()
{
		Stack stack ;
		int select ;

		while(1)
		{
				menu() ;
				scanf("%d", &select) ;

				switch(select)
				{
				case 1:
						InitStack_S(&stack) ;
						break ;

				case 2:
						Distroy_Stack_S(&stack) ;
						break ;

				case 3:
						Clear_Stack_S(&stack) ;
						break ;

				case 4:
						Get_Stack_Top_Interface_S(&stack) ;
						break ;

				case 5:
						Push_Stack_Interface_S(&stack) ;
						break ;

				case 6:
						Pop_Stack_Interface_S(&stack) ;
						break ;


				default :
						printf("\n无效输入，请重新输入\n") ;
						break ;

				}
				fflush(stdin) ;
		}
}



void menu(void)
{
	printf("\n*********************************************\n") ;
	printf(  "*1 建立空栈                 2 销毁栈        *\n") ;
	printf(  "*3 清空栈                   4 显示栈顶      *\n") ;
	printf(  "*5 数据入栈                 6 数据出栈      *\n") ;
	printf(  "*                  0 退出                   *\n") ;
	printf(  "*********************************************\n") ;
	printf("请选择：") ;
}

void InitStack_S(pStack stack)
{
		if((stack->base = (int*)malloc(STACK_INIT_SIZE * sizeof(int))) == (int*)NULL)
		{
				printf("\n空间申请失败！\n") ;
				exit(1) ;
		}
		stack->top = stack->base ;
		stack->stacklenght = 0 ;
		stack->stacksize = STACK_INIT_SIZE ;
}



void Distroy_Stack_S(pStack stack) 
{
		if(stack->stacklenght > -1)
		{
				free(stack->base) ;
				stack->base = (int*)NULL ;
				stack->top = (int*)NULL ;
				stack->stacklenght = -1 ;
				stack->stacksize = -1 ;
		}
}


void Clear_Stack_S(pStack stack)
{
		if(stack->stacklenght > -1)
		{
				stack->top = stack->base ;
				stack->stacklenght =0 ;
		}
} 


int If_Stack_Empty_S(pStack stack)
{
		if(stack->stacklenght > 0)
				return SUCCESS ;
		return FAIL ;
} 


int Get_Stack_Top_S(pStack stack, int *top)
{
		if(stack->stacklenght > 0)
		{
				*top = *(stack->top - 1) ;
				return SUCCESS;
		}
		return FAIL ;
} 

void Get_Stack_Top_Interface_S(pStack stack) 
{
		int top ;//调用函数后删除top吗

		
		if(Get_Stack_Top_S(stack, &top) == SUCCESS) 
		{
				printf("栈顶为：%d\n", top ) ;
		}
		else
		{
				printf("\n栈为空\n") ;
		}
}

void Push_S(pStack stack, int data) 
{

		if(stack->stacklenght == -1)
		{
				printf("\n栈以销毁，无法入栈\n");
		}
		else
		{
				If_Full_AddMemory_S(stack) ;
				*(stack->top) = data ;
				stack->top++ ;
				stack->stacklenght++ ;
		}
}


void Push_Stack_Interface_S(pStack stack)
{
		int data ;

		printf("请输入入栈数据，以”CTRL+E“结束.\n") ;
		
		while(scanf("%d", &data) )
		{
				Push_S(stack, data) ;
		}
		fflush(stdin) ;
} 

int  Pop_S(pStack stack, int *top) 
{
		if(stack->top == stack->base)
		{
				printf("\n栈为空,无元素\n") ;
				return FAIL ;
		}

		*top = *(--stack->top) ;
		stack->stacklenght-- ;
		return SUCCESS ;
}


void Pop_Stack_Interface_S(pStack stack) 
{
		int top ;

		if(Pop_S(stack, &top) == FAIL)
		{
				;
		}
		else
		{
				printf("出栈元素：%d", top) ;
		}
}