
#include <stdio.h>
#include <stdlib.h>

enum status {FAIL, SUCCESS} ;


typedef struct stack
{
		int data ;
		struct	stack *next ;
}Stack, *pStack ;



void menu(void) ;
////////////////////ADT Stack 的基本操作

//3 销毁栈
void Distroy_Stack_S(pStack *) ;

//4 清空栈
void Clear_Stack_S(pStack *) ;

//5 检测栈是否为空
int If_Stack_Empty_S(pStack *) ;

//6 返回栈顶元素
int Get_Stack_Top_S(pStack *, int *) ;

//7 插入元素
void Push_S(pStack *, int) ;

//8 函数7的交互界面
void Push_Stack_Interface_S(pStack *) ;

//9 函数6的交互界面
void Get_Stack_Top_Interface_S(pStack *) ;

//10 出栈返回出栈的元素
int Pop_S(pStack *, int *) ;

//11 函数10的交互界面
void Pop_Stack_Interface_S(pStack *) ;


int main()
{
		pStack top = NULL ;
		int select ;

		while(1)
		{
				menu() ;
				scanf("%d", &select) ;

				switch(select)
				{

				case 2:
						Distroy_Stack_S(&top) ;
						break ;

				case 4:
						Get_Stack_Top_Interface_S(&top) ;
						break ;

				case 5:
						Push_Stack_Interface_S(&top) ;
						break ;

				case 6:
						Pop_Stack_Interface_S(&top) ;
						break ;


				default :
						printf("\n无效输入，请重新输入\n") ;
						break ;

				}
				fflush(stdin) ;
		}
		return 0 ;
}



void menu(void)
{
	printf("\n*********************************************\n") ;
	printf(  "*2 销毁栈                   4 显示栈顶      *\n") ;
	printf(  "*5 数据入栈                 6 数据出栈      *\n") ;
	printf(  "*                  0 退出                   *\n") ;
	printf(  "*********************************************\n") ;
	printf("请选择：") ;
}


void Distroy_Stack_S(pStack *top) 
{
		pStack del ;

		while(*top != (pStack)NULL)
		{
				del = *top ;
				*top = (*top)->next ;
				free(del) ;
		}
}




int If_Stack_Empty_S(pStack *top)
{
		if(*top != (pStack)NULL)
				return SUCCESS ;
		return FAIL ;
} 


int Get_Stack_Top_S(pStack *top, int *data)
{
		if(*top != (pStack)NULL)
		{
				*data = (*top)->data ;
				return SUCCESS;
		}
		return FAIL ;
} 

void Get_Stack_Top_Interface_S(pStack *top) 
{
		int data ;//调用函数后删除top吗

		
		if(Get_Stack_Top_S(top, &data) == SUCCESS) 
		{
				printf("栈顶为：%d\n", data ) ;
		}
		else
		{
				printf("\n栈为空\n") ;
		}
}

void Push_S(pStack *top, int data) 
{
		 pStack newNode ;

		if((newNode = (pStack)malloc(sizeof(Stack))) == (pStack)NULL)
		{
				printf("\n空间分配错误！\n") ;
				exit(1) ;
		}
		newNode->data = data ;
		newNode->next = *top ;
		*top = newNode ;
}


void Push_Stack_Interface_S(pStack *top)
{
		int data ;

		printf("请输入入栈数据，以”CTRL+E“结束.\n") ;
		
		while(scanf("%d", &data) )
		{
				Push_S(top, data) ;
		}
		fflush(stdin) ;
} 


int  Pop_S(pStack *top, int *data) 
{
		pStack del ;

		if(*top != (pStack)NULL)
		{
				*data = (*top)->data ;
				del = *top ;
				*top = (*top)->next ;
				free(del) ;
				return SUCCESS ;
		}
		return FAIL ;
}


void Pop_Stack_Interface_S(pStack *top) 
{
		int data;

		if(Pop_S(top, &data) == FAIL)
		{
				printf("\n栈为空！\n") ;
		}
		else
		{
				printf("出栈元素：%d", data) ;
		}
}
