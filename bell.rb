=begin 集合划分数
根据递推, 从大往小

计算S(n,k)则是通过递推的。考虑往n−1

个元素的集合中添加1个元素，有两种可能：

    1、新元素自成1块，n−1

    个元素划分为k−1块，此种情况数目为S(n−1,k−1)；
    2、n−1个元素划分为k块，新元素放进其中的任一块，此种情况数目为kS(n−1,k)

    于是有递推公式
    S(n,k)=S(n−1,k−1)+kS(n−1,k)
=end

## n个数,划分为k块的个数
$env={}# 下面的S,有大量重复计算
def S(n, k)
  #S(n,k)=S(n−1,k−1)+kS(n−1,k)
  $env[[n,k]] and return $env[[n,k]]
  k == 1 || n == k and return 1
  k > n || k < 1 and return 0
  ret = S(n-1, k-1) + k * S(n-1, k)
  $env[[n,k]]= ret
  return ret
end

def Bell(n)
  return (1..n).sum{ |k| S(n, k) }
end

printf "bell:%s = %s\n", 1, Bell(1)
printf "bell:%s = %s\n", 2, Bell(2)
printf "bell:%s = %s\n", 3, Bell(3)
printf "bell:%s = %s\n", 4, Bell(4)
printf "bell:%s = %s\n", 5, Bell(5)
printf "bell:%s = %s\n", 6, Bell(6)
printf "bell:%s = %s\n", 30, Bell(30)
