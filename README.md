## 个人学习算法，数据结构的笔记

渐进符号：
+   上线 最坏情况              : O
+   下线 最好情况(Bogus)       : Ω
+   O与Ω交集 对于n1 < n < n2   : Ɵ
+   o和O相似，对于任意n
+   ω
+   输入量                     : n

增长基准：
+   1
+   lgN
+   n
+   nlgN
+   n^2   
+   n!
+   n^n
    
求递归算法：
    使用递归树


TREE
---------------------------------
B tree -> 2-3-4 tree -> red-black tree

B tree
-------------------------------------
B树（B-tree） 子平衡m叉树
对其进行排序并允许以O(log n)的时间复杂度运行进行查找、
顺序读取、插入和删除的数据结构。
B树，概括来说是一个节点可以拥有多于2个子节点的二叉查找树。
与自平衡二叉查找树不同，B-树为系统最优化大块数据的读和写操作。
B-tree算法减少定位记录时所经历的中间过程，从而加快存取速度。
普遍运用在数据库和文件系统。

B tree 其实就是运行节点的子节点有n-m 个 n < m && n >= 2
 比如 n=2 m=3 的B树就叫做 2-3B树

 一个节点有n的key，则他的子节点的数量必为n+1 个
 每个节点的key，按顺序排列


Advanced Data Structes

MIT class
https://courses.csail.mit.edu/6.851/fall17/lectures/

* Skip Table see skipTable.c


Buffer Tree
--------------------------
write-optimized DS, It can full utilize modern large memory.
Used to as dictionary type.


k-d Tree
-----------------------------

树状数组
-----------------------------
用于区间求和log(n)

B Tree 平衡N叉树
------------------------------
external memory model,为块模型设计
一般n很大，整个树的深度（高度）很小，对磁盘的读写需求较少
磁盘一次就是读一块,那BTree,就把这一块全部用来存相关数据，从而减少对IO的操作
B tree本身没法全部在内存中

B 叉树, 每个非root叶子最多B-2B个孩子
B>=2
每个节点是一个数组
u.chil[i] < u.keys[i] < u.child[i+1]


R Tree
--------------------------------

Mulity Stage Hash Table
--------------------------------
多阶hash表,内存平坦，兼顾使用率和性能

全排列生成 部分排列生成
----------------------
permutation.rb

组合生成算法
combination.rb



母函数 多项式乘法
--------------------
feneratingFn.rb

素因数分解
primeFactor.rb

集合划分数
bell.rb

