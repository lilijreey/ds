
#include <stdio.h>
#include <string.h>

#define MAX 10
#define SWAP(x,y,t)  ((t) = (x), (x) = (y), (y) = (t))

void perm(char [], int, int) ;

int main()
{
	char aggregate[MAX] ;
	int n ;

	printf("Enter a aggregate: ") ;
	scanf("%s", aggregate) ;

	n = strlen(aggregate) ;
	
	perm(aggregate, 0, n) ;


	return 0 ;
}


void perm(char agger[], int i, int n)
{ 
	int j, temp ;
	
	if(i == n-1)
	{
		for(j=0; j < n; j++)
			printf("%c", agger[j]) ;
		printf("\n") ;
	}
	else
	{
		for(j=i; j < n; j++)
		{
			//下面两句用于原理分析
			SWAP(agger[i], agger[j], temp) ;
		//	printf(" a:%s %d/%d/ ", agger, i, j) ;
			perm(agger, i+1, n) ;
			SWAP(agger[i], agger[j], temp) ;
		//	printf(" b:%s %d/%d/ ", agger, i, j) ;
		}
	}	
}