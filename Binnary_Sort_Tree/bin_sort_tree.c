#include "bin_sort_tree.h"
#include <stdio.h>
#include <stdlib.h>



/////////////////////
//1创建一个空的动态查找树
//即初始化指向root的头结点
void Init_DSTree(pBinTNode *h) 
{
		*h = NULL ; //初始化为空
}

//2查找DT中key值，若DT中存在key，则返回TRUE
//p指向该元素，否则返回FALSE，p指向追后一个节点
//(特殊情况，T为空时,不能查找) p 返回NULL
int Search_DSTree(pBinTNode h, pBinTNode root, Elem key, pBinTNode *p) 
{
		if(!h) //T为空时返回
		{		*p = NULL ; 
				return FALSE ;
		}
		else
		{
				if(!root)
						return FALSE ; //没找到 隐式返回p
				else
				{
						*p = root ; //p指向当前节点
						if(key == root->data) 
								return TRUE ;	
						else
						{
								if(key < root->data) //进左子树
										return Search_DSTree(h, root->lchild, key, p) ;
								else
										return Search_DSTree(h, root->rchild, key, p) ;
						}
				}
		}
}


//3插入基于查找若查找成功则不插入，否则在p的左或右插入
//插入成功返回TRUE 反之 RETURN FALES(特殊情况 T为空时）
int Insert_DSTree(pBinTNode *h, Elem key) 
{
		pBinTNode p = NULL ; 
		pBinTNode s = NULL ;

		if(!Search_DSTree(*h, *h, key, &p))
		{
				if((s = (pBinTNode)malloc(sizeof(BinTNode))) == NULL)
				{
						perror("\nMalloc memory error!") ;
						exit(1) ;
				}
				s->data = key ;
				s->lchild = NULL ;
				s->rchild = NULL ;
				s->parent = NULL ;
				if(!p) //T为空 p必为空 
					*h = s ;
				//与p指向的节点data比较大小
				else if(key < p->data)
						p->lchild = s ;
				else
						p->rchild = s ;
				return TRUE ;
		}
		else
				return FALSE ;
}

//4输入元素
void Input_DSTree(pBinTNode *h) 
{
		Elem key ;

		printf("\nEnter some number 'CTRL + D' over the input:\n") ;
		
		while(scanf("%d", &key))
		{
				Insert_DSTree(h, key) ;
		}
		fflush(stdin) ;
}

//5输出排序
void Show_DSTree_Interface(const pBinTNode h) 
{
		if(!h) //Tree为空时
		{
				printf("\n空树无法输出") ;
		}
		else
				Show_DSTree(h) ;
}

void Show_DSTree(const pBinTNode h) 
{
		if(h)
		{
				Show_DSTree(h->lchild) ;
				printf("%d ", h->data) ;
				Show_DSTree(h->rchild) ;
		}
}


//6删除节点
//先在书中查找找到删除，返回TRUE 否则返回FALES
void Delete_DSTree_Node_Interface(pBinTNode *h)
{
		Elem key ; 

		if(!(*h)) //Tree为空时
		{
				printf("\n空树无法输出") ;
		}
		else
		{
				printf("\nEnter you want delete number 'CTRL + D' over the input:\n") ;
				
				while(scanf("%d", &key))
				{
						if(Delete_DSTree_Node(h, key))
								printf("\nDelete %d success!", key) ;
						else
								printf("\nNot find %d.", key) ;
				}
				fflush(stdin) ;
		}
}

//循环查找给DeleCore 提供 
//指向删除节点的指针p 和指向删除节点父节点的指针f
int Delete_DSTree_Node(pBinTNode *h, const Elem key) 
{
		pBinTNode p = *h ; //root
		pBinTNode f = *h ; //root

		while(p)
		{
				if(key == p->data)
						return Delete_DSTree_Node_Core(h, f, p) ;
				else if(key < p->data)
				{
						f = p ;
						p = p->lchild ;
				}
				else
				{
						f = p ;
						p = p->rchild ;
				}
		}
		return FALSE ;
}


int Delete_DSTree_Node_Core(pBinTNode *h, pBinTNode f, pBinTNode p)
{
		pBinTNode leaf = NULL ;
		//判断p是f的左节点还是有节点
		enum way w ;
		if(p != *h)
		{
				if(f->lchild) //f有可能只有一个子树
						(p->data == f->lchild->data) ? (w = LEFT) : (w = RIGHT) ;
				else //f无左子树是

						w = RIGHT ;
		}

		//三种情况
		//1删除叶子节点
		//                                              
        //           X                          X     
        //          / \                        / \
        //         /   \                      /   \
        //        X     X                    X     X  
        //       / \     \                  / \
        //      X   X     X <- dele(1)     X   X     
        //
        //
			
		if(p->lchild == NULL && p->rchild == NULL)
		{
				free(p) ;
				//判断删除的是否为root
				(p == *h) ? (*h = NULL)
						: ((w == LEFT) ? (f->lchild = NULL) : (f->rchild = NULL)) ;		
				return TRUE ;
		}

		//2删除的是有一个子树的中间节点
		//
        //           X                             X     
        //          / \                           / \
        //         /   \                         /   \
        //        X     X                       X     X  
        //       / \     \                     / \     \
        //      X   X     X <- dele(b1)       X   X     Y
        //               /
        //              Y
		//////////////////////////////////////////
		else if(p->lchild == NULL) //p有右子树
		{
					//判断删除的是否为root
				(p == *h) ? (*h = p->rchild)
						: ((w == LEFT) ? (f->lchild = p->rchild) : (f->rchild = p->rchild)) ;	
				free(p) ;
		}
        //           X                             X     
        //          / \                           / \
        //         /   \                         /   \
        //        X     X                       X     X  
        //       / \     \                     / \     \
        //      X   X     Y <- dele(b2)       X   X     Z
        //               /   
        //              Z
		else if(p->rchild == NULL) //p有左子树
		{
					//判断删除的是否为root
				(p == *h) ? (*h = p->lchild)
						: ((w == LEFT) ? (f->lchild = p->lchild) : (f->rchild = p->lchild)) ;	
				free(p) ;
		}

		////////////////////////////////////////////////
		//3删除的是有左右子树的节点
		//有两种嫁接法则
		//1将删除的节点的左子树链接到，删除节点右子树的最左叶子节点, f连右子树
        //           X                             X     
        //          / \                           / \
        //         /   \                         /   \
        //  del-> X     X                       Z     X  
        //       / \     \                     / \     \
        //      Y   Z     Y                   K   M     Z
        //     /   / \                       /
        //    N   K   M                     Y
        //                                 /
        //                                N
        //              
		//2将删除的节点的右子树链接到，删除节点左子树的最右叶子节点。f连左子树
		//我采用第一中
        //           X                             X     
        //          / \                           / \
        //         /   \                         /   \
        //  del-> X     X                       Y     X  
        //       / \     \                     / \     \
        //      Y   Z     Y                   N   Z     Z
        //     /   / \                           / \
        //    N   K   M                         K   M
        //                                 
        //              
		else
		{
				//把左子树连接到删除节点右子树的最左叶子节点
				//找到删除节点右子树的最左叶子节点
				leaf = p->rchild  ;
				while(leaf->lchild)
				{
						leaf = leaf->lchild ;
				}
				leaf->lchild = p->lchild ; //链接

			    //判断删除的是否为root
				(p == *h) ? (*h = p->rchild)
						: ((w == LEFT) ? (f->lchild = p->rchild) : (f->rchild = p->rchild)) ;	
				free(p) ;
		}
		return TRUE ;
}
