#ifndef BIN_SORT_TREE_H_
#define BIN_SORT_TREE_H_


enum status {FALSE, TRUE} ;
enum way {LEFT, RIGHT} ;
typedef int Elem ;

typedef struct BinTreeNode  //二叉树节点定义	//三叉链					
{
		Elem data ;
		struct BinTreeNode *lchild ;  //left child
		struct BinTreeNode *rchild ;  //right child
		struct BinTreeNode *parent ;  // parent node

}BinTNode, *pBinTNode ;


///////////////////////ADT DynamicSearchTable/////////
//1创建一个空的动态查找树
//即初始化指向root的头结点
void Init_DSTree(pBinTNode *h) ;

//2查找DT中key值，若DT中存在key，则返回TRUE
//p指向该元素，否则返回FALSE，p指向追后一个节点
//(特殊情况，T为空时，p的返回NULL
int Search_DSTree(pBinTNode h, pBinTNode root, Elem key, pBinTNode *p) ;

//3插入基于查找若查找成功则不插入，否则在p的左或右插入
//(特殊情况 T为空时）
int Insert_DSTree(pBinTNode *h, Elem key) ;

//4输入元素
void Input_DSTree(pBinTNode *h) ;

//5输出排序中序显示
void Show_DSTree_Interface(const pBinTNode h) ;

//kero
void Show_DSTree(const pBinTNode h) ;

//6删除节点
////////////////////////////
void Delete_DSTree_Node_Interface(pBinTNode *h) ; //传递h是为了解决root的删除问退

//循环查找，找到删除，返回TRUE 否则返回FALES
int Delete_DSTree_Node(pBinTNode *h, const Elem key) ;

//7删除核心算法 删除成功返回TURE 反之返回FALSE
//三种情况f 是 p的父节点 p是删除节点
int Delete_DSTree_Node_Core(pBinTNode *h, pBinTNode f, pBinTNode p) ;
//////////////////////////////////////////////////


#endif