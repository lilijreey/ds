#include "bin_sort_tree.h"
#include <stdio.h>

void menu(void) ;

int main()
{
		pBinTNode h ; //head node
		Init_DSTree(&h) ; //初始化 头结点



		while(1)
		{
				int select ;
			
				menu() ;
				scanf("%d", &select) ;
				switch(select)
				{
						case 1:
								Input_DSTree(&h) ;
								break ;

						case 2:
								Show_DSTree_Interface(h) ;
								break ;
								
						case 3:
								Delete_DSTree_Node_Interface(&h) ;
								break ;

						case 0:
								return 0 ;
								
						default:
								printf("选择错误") ;
								fflush(stdin) ;
								break ;
				}
		}
}


void menu(void)
{
	printf("\n***********************************************\n") ;
	printf(  "*1 插入数据                2 显示排序结果      *\n") ;
	printf(  "*3 删除数据                                    *\n") ;
	printf(  "*                    0 退出                    *\n") ;
	printf(  "************************************************\n") ;
	printf("请选择：") ;
}
