#include <stdio.h>
#include <stdlib.h>

#define MOVE(x,y)  (6 -(x) - (y))

enum eSet {A =1, B, C} ;

typedef struct sPice
{
		int set ;
}Pice, *pPice ;

void CreatePices(pPice*, int) ;
void Hanio(pPice, int num, int move) ;

FILE *fp ;
int count ;

int main()
{
		int num ;
		pPice top ;

		fp = fopen("Hanoi Tower.txt", "w") ;

		printf("请输入铁块个数：") ;
		scanf("%d", &num) ;
		CreatePices(&top, num) ;

		Hanio(top, num,  C) ;
		fclose(fp) ;

		return 0 ;
}


void CreatePices(pPice *top, int num) 
{
		if((*top = (pPice)malloc(num * sizeof(Pice))) == (pPice)NULL)
		{
				printf("\nMEMORY ERROR！\n") ;
				exit(1) ;
		}
		while(--num > -1)
				(*top)[num].set = A ;

}


void Hanio(pPice top, int num, int move)
{
		if(num == 1)
		{
				fprintf(fp, "%3d__1:%d-->%d\n", ++count, top[num-1].set, move) ; 
				top[0].set = move ;
		}
		else
		{
				Hanio(top, num-1, MOVE(top[num-2].set, move)) ;
				fprintf(fp, "%3d__%d:%d-->%d\n", ++count, num, top[num-1].set, move) ;
				top[num-1].set = move ;
				//回位
				Hanio(top, num-1, move) ;		
		}			
}
