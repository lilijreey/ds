#include <stdio.h>
#include <stdlib.h>

#define LIST_INIT_SIZE 10 //100
#define LISTINCREMENT 10

enum status {FAIL, SUCCESS} ;   //枚举名是类型吗？

typedef struct LinearList
{
	int *elem ;
	int lenght ;
	int listsize ;
} SqList ;

void menu(void) ;
////////////////线性表的基本操作
//
//1 建立一个空线性表
//传递一个SqList的指针
void CreateList_Sq(SqList *) ;

//2 输入数据起始位置为l->lenght
void InputList_Back_Sq(SqList *) ;

//3 检验list.elem是否已满，如果满了自动扩充容量
void If_Full_AddMemory_Sq(SqList *) ;

//4 显示线性表
void ShowList_Sq(SqList *) ;

//5在头部输入数据
void InputList_Front_Sq(SqList *) ;

//6在特定位置输入数据首个元素为0
int InputList_Set_Sq(SqList *, int, int) ;

//7函数六的外壳交互界面
void InputList_Set_Interface_Sq(SqList *) ;

//8销毁线性表
void DestroyList_Sq(SqList *) ;

//9将链表清空
void ClearList_Sq(SqList *) ;

//10在特定位置删除数据
int DeleteList_Set_Sq(SqList *, int) ;

//11函数10的交互界面
void DeleteList_Set_Interface_Sq(SqList *l) ;

//12删除表里的特定数据
int DeleteList_Data_Sq(SqList *l, int) ;

//13函数12的交互界面
void DeleteList_Data_Interface_Sq(SqList *) ;

//14在线性表里查找特定数据把符合的数据下标记录在传递给他的线性表中
//返回相同数据的个数
int FindList_DataSet_Sq(SqList *, int, SqList *) ;

//15只查看有无此数据，返回是否查到
int FindList_Data_Sq(SqList *, int) ;

//16函数14的交互界面
void FindList_DataSet_Interface_Sq(SqList *l) ;

//17从给定的位置开始查找，返回下标
int FindList_Select_Head_Sq(SqList *, int head, int data) ;

//18和并线性表 再输入一个和并
void MergeList_Sq(SqList *, SqList *, SqList *) ;

//19函数18的交互界面
void MergeList_Interface_Sq(SqList *) ;



int main()
{
	int select ;
	SqList list ;
	list.lenght = -1 ; //用于输入数据函数检验

	while(1)
	{
		 

		menu() ;
		scanf("%d", &select) ;

		switch(select)
		{
		case 1:
			CreateList_Sq(&list) ;
			break ;

		case 2:
			InputList_Back_Sq(&list) ;
			break ;
			
		case 3:
			ShowList_Sq(&list) ;
			break ;

		case 4:
			InputList_Front_Sq(&list) ;
			break ;

		case 5:
			InputList_Set_Interface_Sq(&list) ;
			break ;

		case 6:
			DeleteList_Set_Interface_Sq(&list) ;
			break ;

		case 7:
			DeleteList_Data_Interface_Sq(&list) ;
			break ;

		case 8:
			FindList_DataSet_Interface_Sq(&list) ;
			break ;

		case 9:
			MergeList_Interface_Sq(&list) ;
			break ;

		case 0:
			return 0 ;

		default:
			printf("选择错误") ;
			//缺一个清空流的函数
			break ;
		}
	}

	return 0 ;
}


void menu(void)
{
	printf("\n*************************************************\n") ;
	printf(  "*1 建立一个空线性表         2 在尾部输入数据    *\n") ;
	printf(  "*3 显示线性表               4 在头部输入数据    *\n") ;
	printf(  "*5 自定义位置输入           6 删除自定义位置数据*\n") ;
	printf(  "*7 删除数据                 8 查找数据          *\n") ;
	printf(  "*9 合并两个线性表                               *\n") ;
	printf(  "*                    0 退出                     *\n") ;
	printf(  "*************************************************\n") ;
	printf("请选择：") ;
}

void CreateList_Sq(SqList *l)
{
	if( (l->elem = (int *)malloc(LIST_INIT_SIZE * sizeof(int))) == (int *)NULL)
	{
		printf("\n内存分配失败！") ;
		exit (1) ;
	}

	l->lenght = 0 ;
	l->listsize = 0 ;
	printf("\n建立成功！\n") ;
} 

void InputList_Back_Sq(SqList *l)
{
	int num ;
	int i ;

	if(l->lenght == -1)
	{
		printf("\n没有建立线性表无法输入数据，请先建立线性表\n") ;
	}
	else
	{
		printf("请输入数据,以CTRL+E结束。 \n") ;

		for(i = l->lenght; scanf("%d", &num); i++)
		{   
			If_Full_AddMemory_Sq(l) ;
			l->elem[i] = num ;
			l->lenght++ ;
		}
		fflush(stdin)  ;
	}	
}

void If_Full_AddMemory_Sq(SqList *l)
{
	int *newMemory ;
	int i ;

	if(l->lenght == l->listsize)
	{
		if( (newMemory = (int *)malloc((l->listsize + LISTINCREMENT) * sizeof(int))) == (int *)NULL)
		{
			printf("\n内存分配失败！") ;
			exit (1) ;
		}
		l->listsize = l->listsize + LISTINCREMENT ;
			
		for(i = 0; i < l->lenght; i++)
		{
			*(newMemory + i) = l->elem[i] ;
		}
		free(l->elem) ;
		l->elem = newMemory ;		
	}
}


void ShowList_Sq(SqList *l)
{
	int i ;

	if(l->lenght == -1 || l->lenght == 0)
	{
		printf("\n无链表或链表为空，无法显示！\n") ;
	}
	else
	{
		printf("\n链表元素个数为：%d\n", l->lenght) ;
		printf("链表大小为：%d\n", l->listsize) ;
		for(i = 0; i < l->lenght; i++)
		{
			printf("%d, ", l->elem[i]) ;
		}
	}
}

void InputList_Front_Sq(SqList *l)
{
	int num ;
	int i ;

	if(l->lenght == -1)
	{
		printf("\n没有建立线性表无法输入数据，请先建立线性表\n") ;
	}
	else
	{
		printf("请输入数据,以CTRL+E结束。 \n") ;

		while(scanf("%d", &num))
		{
			If_Full_AddMemory_Sq(l) ;
			for(i = l->lenght; i > 0; i--)
			{
				l->elem[i] = l->elem[i-1] ;				
			}
			l->elem[0] = num ;
			l->lenght++ ;
		}
		fflush(stdin) ;
	}
}


void InputList_Set_Interface_Sq(SqList *l) 
{
	int set ;
	int num ;

	if(l->lenght == -1)
	{
		printf("\n没有建立线性表无法输入数据，请先建立线性表\n") ;
	}
	else
	{
		printf("\n请输入插入的位置（起始位置为0）：") ;
		scanf("%d", &set) ;
		while(set < 0 || set > l->lenght)
		{
			printf("\n无效位置，请从新输入： ") ;
			scanf("%d", &num) ;
		}	
		printf("请输入插入值： ") ;
		scanf("%d", &num) ;

		InputList_Set_Sq(l, set, num) ;
	}
}

int InputList_Set_Sq(SqList *l, int set, int num)
{
	int i ;

	if(set > l->lenght || set < 0)
	{
		printf("\n输入位置有误！\n") ;
		return FAIL ;
	}
	else
	{
		if(set == l->lenght)
		{
			If_Full_AddMemory_Sq(l) ; //这句很重要。
			l->elem[l->lenght] = num ;
			l->lenght++ ;		
		}
		else
		{
			If_Full_AddMemory_Sq(l) ;
			for(i = l->lenght; i > set; i--)
			{
				l->elem[i] = l->elem[i-1] ;
			}
			l->elem[set] = num ;
			l->lenght++ ;
		}
	}
	return SUCCESS ;
}

void DestroyList_Sq(SqList *l)
{
	if(l->lenght > 0)
	{
		free(l->elem) ;
		l->lenght = -1 ;
		l->listsize = 0 ;
	}
}

void ClearList_Sq(SqList *l)
{
	if(l->lenght == -1)
	{
		printf("\n无链表或链表为空，无法清空请先建立线性表\n") ;
	}
	else
	{
		l->lenght = 0 ;
	}
}

void DeleteList_Set_Interface_Sq(SqList *l)
{
	int set ;

	if(l->lenght < 1)
	{
		printf("\n无链表或链表为空，无法清删除先建立线性表\n") ;
	}
	else
	{
		printf("\n请输入删除元素的位置（起始位置为0）：") ;
		scanf("%d", &set) ;
		while(set < 0 || set >= l->lenght)
		{
			printf("\n无效位置，请从新输入： ") ;
			scanf("%d", &set) ;
		}	

		DeleteList_Set_Sq(l, set) ;
	}
}


int DeleteList_Set_Sq(SqList *l, int set)
{
	int i ;

	if(set < 0 || set >= l->lenght)
	{
		printf("无效删除位置！\n") ;
		return FAIL ;
	}
	else
	{
		for(i = set; i < l->lenght; i++)
		{
			l->elem[i] = l->elem[i+1] ;
		}
		l->lenght-- ;
	}
	return SUCCESS ;
}

void DeleteList_Data_Interface_Sq(SqList *l) 
{
	int num ;

	if(l->lenght < 1)
	{
		printf("\n无链表或链表为空，无法清删除先建立线性表\n") ;
	}
	else
	{
		printf("\n请输入删除数据\n") ;
		scanf("%d", &num) ;
		if(FindList_Data_Sq(l, num) == FAIL)
		{
			printf("\n线性表没有要删除的数据，无法删除\n") ;
		}
		else
		{
			DeleteList_Data_Sq(l, num) ;
		}
	}
}


int DeleteList_Data_Sq(SqList *l, int num) 
{
	int head =0 ;

	if(FindList_Data_Sq(l, num) == 0)
	{
		return FAIL ;
	}
	else
	{
		while(head = FindList_Select_Head_Sq(l, head, num))
		{
			if(head == -1)//防止最后一个
				break ;
			DeleteList_Set_Sq(l, head) ;
		}
		return SUCCESS ;
	}
}


int FindList_Data_Sq(SqList *l, int data) 
{
	int i ;

	if(l->lenght < 1)
	{
		printf("\n无链表或链表为空，无法擦找请先建立线性表\n") ;
		return 0 ;
	}
	else
	{
		for(i = 0; i < l->lenght; i++)
		{
			if(l->elem[i] == data)
				return SUCCESS ;
		}
	}
	return FAIL ;
}


int FindList_Select_Head_Sq(SqList *l, int head, int data)
{
	int i ;

	if(l->lenght < 1)
	{
		printf("\n无链表或链表为空，无法擦找请先建立线性表\n") ;
		return 0 ;
	}
	else
	{
		if(head >= l->lenght)
		{
			//printf("\n无效起始位置\n") ;
			return FAIL ;
		}
		else
		{
			for(i = head; i < l->lenght; i++)
			{
				if(l->elem[i] == data)
				{
					return i ;
				}
			}
		}
		//这步为如果没有匹配数据了返回-1以告诉调用函数以查找完了。
		return -1 ;
	}
}


int FindList_DataSet_Sq(SqList *l, int data, SqList *arrSet)
{
	int i ;
	int value = 0 ;

	if(l->lenght < 1)
	{
		printf("\n无链表或链表为空，无法擦找请先建立线性表\n") ;
		return 0 ;
	}
	else
	{
		for(i = 0; i < l->lenght; i++)
		{
			if(l->elem[i] == data)
			{
				If_Full_AddMemory_Sq(arrSet) ;
				arrSet->elem[value++] = i ;
				arrSet->lenght++ ;
			}
		}
		arrSet->elem[value] = -1 ;//以-1为结束标记
		return value ;
	}
}

void FindList_DataSet_Interface_Sq(SqList *l)
{
	int i ;
	int num ;
	SqList arrSet ;

	if(l->lenght < 1)
	{
		printf("\n无链表或链表为空，无法擦找请先建立线性表\n") ;
	}
	else
	{
		printf("\n请输入查询数据：") ;
		scanf("%d", &num) ;

		if(FindList_Data_Sq(l, num) == FAIL)
		{
			printf("\n无此数据\n") ;
		}
		else
		{
			CreateList_Sq(&arrSet) ;
			FindList_DataSet_Sq(l, num, &arrSet) ;
			printf("数据%d共有%d个.\n", num, arrSet.lenght) ;
			printf("位置为：") ;
			for(i = 0; arrSet.elem[i] != -1 ; i++)
			{
				printf("%d ", arrSet.elem[i]) ;
			}
	
			free(arrSet.elem) ;
		}	
	}
} 

void MergeList_Interface_Sq(SqList *l1)
{
	SqList meList, l2 ;

	CreateList_Sq(&meList) ;
	CreateList_Sq(&l2) ;

	printf("\n请输入第二个线性表的值\n") ;
	InputList_Back_Sq(&l2) ;

	MergeList_Sq(&meList, l1, &l2) ;
	
	printf("\n合并结果为：\n") ;
	ShowList_Sq(&meList) ;

	//删除空间
	free(meList.elem) ;
	free(l2.elem) ;
}

void MergeList_Sq(SqList *meList, SqList *l1, SqList *l2) 
{
	int i, j ;
	int data ;

	for(i = 0, j = 0; i < l1->lenght; i++, j++)
	{
		If_Full_AddMemory_Sq(meList) ;
		meList->elem[j] = l1->elem[i] ;
		meList->lenght++ ;
	}

	for(i = 0, j = meList->lenght; i < l2->lenght; i++)
	{
		data = l2->elem[i] ;
		if(FindList_Data_Sq(meList, data) == FAIL)
		{
			InputList_Set_Sq(meList, j, data) ;
			j++ ;
		}
	}
}


