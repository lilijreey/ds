#ifndef AVL_TREE_H_
#define AVL_TREE_H_

enum status {FALSE, TRUE} ;
//平衡因子为左减右
enum factor {RH = -1, EH, LH} ;	

typedef int Elem ;

typedef struct AVLNode
{
		Elem data ;
		int bf ; //每一个节点的平衡因子
		struct AVLNode *lchild ;
		struct AVLNode *rchild ;
}ATNode, *pATNode ;

///////////////////////////////////////////
//1创建一个空的AVL树头结 
//初始化指向ROOT的头结点
void Init_AVLTree(pATNode *h) ;

//2查找AVL中key值，若AVL中存在key，则返回TRUE
//p指向该元素，否则返回FALSE，p指向追后一个节点
//(特殊情况，T为空时,不能查找) p 返回NULL
int Search_AVLTree(pATNode h, pATNode root, Elem key, pATNode *p) ;

//3插入节点插入平衡为平衡树
void Insert_AVLTree(pATNode *parent, Elem key, int *unbalanced) ; //最初为h

//输入元素界面
void Input_AVLTree(pATNode *h) ;


//4LL 或LR平衡
void Left_Rotation(pATNode *p, int *unbalanced) ;

//5 RR 或RL平衡
void Right_Rotation(pATNode *p, int *unbalanced) ;

//6输出排序中序显示
void Show_AVLTree_Interface(const pATNode h) ;

//kero
void Show_AVLTree(const pATNode h) ;

//7删除节点一个头结点，一个当前节点
//unbalanced 初始化为FALSE 删除节点后改为TRUE
//shorts 初始为FALSE 删除时为TRUE
void Delete_AVLTree_Node(pATNode *p, Elem key, int *shorts) ; //init p = h

//
void Delete_AVLTree_Interface(pATNode *h) ;
#endif