#include "AVL_Tree.h"
#include <stdio.h>

void menu(void) ;

int main(void)
{
		pATNode h ;
		Init_AVLTree(&h) ;

		while(1)
		{
				int select ;
				menu() ;
				scanf("%d", &select) ;
				switch(select)
				{
				case 1:
						Input_AVLTree(&h) ;
						break ;
						
				case 2:
						Show_AVLTree_Interface(h) ;
						break ;

				case 3:
						Delete_AVLTree_Interface(&h) ;
						break ;
						
				case 0:
						return 0 ;
						
				default:
						printf("\n无效选择") ;
						fflush(stdin) ;
				}
		}
}


void menu(void)
{
	printf("\n*******************AVL_TREE**********************\n") ;
	printf(  "*1 插入数据                2 显示排序结果      *\n") ;
	printf(  "*3 删除数据                                    *\n") ;
	printf(  "*                    0 退出                    *\n") ;
	printf(  "************************************************\n") ;
	printf("请选择：") ;
}
