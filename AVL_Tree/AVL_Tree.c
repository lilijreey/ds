#include "AVL_Tree.h"
#include <stdio.h>
#include <stdlib.h>

//1创建一个空的AVL树头结 
//初始化指向ROOT的头结点
void Init_AVLTree(pATNode *h)
{
		*h = NULL ;
} 

//2查找AVL中key值，若AVL中存在key，则返回TRUE
//p指向该元素，否则返回FALSE，p指向追后一个节点
//(特殊情况，T为空时,不能查找) p 返回NULL
int Search_AVLTree(pATNode h, pATNode root, Elem key, pATNode *p) 
{
		if(!h) //T为空时返回
		{		*p = NULL ; 
				return FALSE ;
		}
		else
		{
				if(!root)
						return FALSE ; //没找到 隐式返回p
				else
				{
						*p = root ; //p指向当前节点
						if(key == root->data) 
								return TRUE ;	
						else
						{
								if(key < root->data) //进左子树
										return Search_AVLTree(h, root->lchild, key, p) ;
								else
										return Search_AVLTree(h, root->rchild, key, p) ;
						}
				}
		}
}

//3插入没一会都形成一个平衡树附带查找parents 最初为h
//中序遍历注意P是指向bf为+-2的节点的父节点的lchild或rchild （p == h)除外
void Insert_AVLTree(pATNode *p, Elem key, int *unbalanced) //unbalanced 表示是否要平衡
//推论插入一个节点如果要平衡的话只需平衡一次整个树就会平衡？
{
		if(!*p) //插入一个节点 隐式的为空Tree插入头结点
		{
				*unbalanced = TRUE ;  //不平衡
				if((*p = (pATNode) malloc(sizeof(ATNode))) == NULL)
				{
						perror("\nMalloc memory error!") ;
						exit(1) ;
				}
				(*p)->lchild = (*p)->rchild = NULL ;
				(*p)->data = key ;
				(*p)->bf = EH ; //0
		}
		else if(key < (*p)->data) //Into Left subTree
		{
				//进入左子树
				Insert_AVLTree(&((*p)->lchild), key, unbalanced) ;  

				//递归退回来后
				if(*unbalanced) //如果左子树不平衡
				{
						switch((*p)->bf)
						{
						case RH: //-1
								(*p)->bf = 0 ;
								*unbalanced = FALSE ; //如果插入一个节点使得又该节点的子树平衡，那就不需要平衡
								break ;
						case EH: //0
								(*p)->bf = 1 ;
								break ;
						case LH: //1 将加到2 所以进行LL或LR旋转
								//因为无法判断插入节点是查在P leftsubTree的左子树，还是右子树
								//传入要平衡节点
								Left_Rotation(p, unbalanced) ;
								break ;
						default:
								perror("switch error!\n") ;
								exit(1) ;
						}
				}
		}
		else if(key > (*p)->data)
		{
				//进入右子树
				Insert_AVLTree(&((*p)->rchild), key, unbalanced) ;
				//从右子树递归回来后
				if(*unbalanced)
				{
						switch((*p)->bf)
						{
						case RH: //-1 加变为-2所以进行平衡RL或RR旋转
								////因为无法判断插入节点是查在P right subTree的左子树，还是右子树
								Right_Rotation(p, unbalanced) ;
								break ;
						case EH: //0
								(*p)->bf = RH ; //-1
								break ;
						case LH: //1
								(*p)->bf = EH ;
								*unbalanced = FALSE ; 
								break ;
						default:
								perror("switch error!\n") ;
						}
				}
		}
		else
		{
				*unbalanced = FALSE ;
				printf("\nThe %d key is already in the tree.") ;
		}
}

//输入元素界面
void Input_AVLTree(pATNode *h) 
{

		Elem key ;
		int unbalanced ;
		printf("\nEnter some number 'CTRL + D' over the input:\n") ;
		
		while(scanf("%d", &key))
		{
				Insert_AVLTree(h, key, &unbalanced) ;
		}
		fflush(stdin) ;
		printf("\nInseart success") ;
}

//4LL 或LR平衡用的是替代方法
void Left_Rotation(pATNode *p, int *unbalanced) 
{
		pATNode grandChild, child ;
		child = (*p)->lchild ;
		//判断是LL还是LR Rotation
		//更新三个节点的bf值
		if(LH == child->bf) //LL Rotation
		{
				(*p)->lchild = child->rchild ;
				child->rchild = (*p) ;
				child->bf = (*p)->bf = EH ; //更新三个节点的bf值
				*p = child ; //重！更新头结点。
		}
		else //LR Rotation
		{
				grandChild = child->rchild ; //指向新的根节点
				child->rchild = grandChild->lchild ;
				(*p)->lchild = grandChild->rchild ;
				grandChild->lchild = child ;
				grandChild->rchild = (*p) ;

				switch(grandChild->bf) //更新调整后的bf值
				{
				case RH: //-1
						(*p)->bf = EH ;
						child->bf = LH ;
						break ;
				case EH: //0
						(*p)->bf = child->bf = EH ;
						break ;
				case LH: //1
						(*p)->bf = RH ;
						child->bf = EH ;
						break ;
				default :
						perror("\n switch error") ;
				}
				*p = grandChild ;
				grandChild->bf = EH ;
		}
		*unbalanced = FALSE ;
}

//5 RR 或RL平衡
void Right_Rotation(pATNode *p, int *unbalanced) 
{
		pATNode grandChild, child ;
		child = (*p)->rchild ;

		//判断是RR还是RL Rotation
		if(RH == child->bf) //RR
		{
				(*p)->rchild = child->lchild ;
				child->lchild = (*p) ;
				child->bf = (*p)->bf = EH ;
				*p = child ; //更新头结点
		}
		else
		{
				grandChild = child->lchild ; //新的根节点
				(*p)->rchild = grandChild->lchild ; 
				child->lchild = grandChild->rchild ;
				grandChild->lchild = *p ;
				grandChild->rchild = child ;
			
				switch(grandChild->bf) 
				{
				case RH: //-1
						child->bf = EH ;
						(*p)->bf = LH ;
						break ;
				case EH:
						child->bf = (*p)->bf = EH ;
						break ;
				case LH:
						child->bf = RH ; //-1
						(*p)->bf = EH ;
						break ;
				default:
						perror("\nswitch error") ;
				}
				*p = grandChild ;
				grandChild->bf = EH ;
		}
		*unbalanced = FALSE ;
}


//5输出排序
void Show_AVLTree_Interface(const pATNode h) 
{
		if(!h) //Tree为空时
		{
				printf("\n空树无法输出") ;
		}
		else
				Show_AVLTree(h) ;
}

void Show_AVLTree(const pATNode h) 
{
		if(h)
		{
				Show_AVLTree(h->lchild) ;
				printf("%d ", h->data) ;
				Show_AVLTree(h->rchild) ;
		}
}
 


void Delete_AVLTree_Interface(pATNode *h) 
{
		Elem key ; 
		int shorts = FALSE ;

		if(!(*h)) //Tree为空时
		{
				printf("\n空树无法输出") ;
		}
		else
		{
				printf("\nEnter you want delete number 'CTRL + D' over the input:\n") ;
				
				while(scanf("%d", &key))
				{
						Delete_AVLTree_Node(h, key, &shorts) ;
				}
				fflush(stdin) ;
		}

}
//关于删除 bf的更新如下
//从左子树返回
//原bf== -1 现 bf == -2 转
//bf == 0 bf == -1 shorts 置fales
//bf == 1 bf == 0
//从右子树返回的
//原bf == -1 现bf == 0
//bf == 0 bf = 1 shorts 置false
//bf == 1 bf == 2 转
//重！只要删除的节点的父节点的原bf为0
//则只用改变该父节点的bf值祖先节点bf值不变

//unbalanced 初始化为FALSE 删除节点后改为TRUE
//shorts 初始为FALSE 删除时为TRUE
void Delete_AVLTree_Node(pATNode *p, Elem key, int *shorts)
{
		pATNode dele ;
		int unbalanced = TRUE ;
		int elem ;

		//查找删除节点
				if(*p) //p不为空
				{
						if(key == (*p)->data)
						{
								*shorts = TRUE ;

								////三种情况
								//1删除叶子节点
								if(NULL == (*p)->rchild && NULL == (*p)->rchild)
								{
										free(*p) ;
										*p = NULL ;
								}
								//2删除的是有一个子树的中间节点
								//////////////////////////////////////////
								else if((*p)->lchild == NULL) //p有右子树
								{
										dele = *p ;
										*p = (*p)->rchild ;
										free(*p) ;
										*p = NULL ;

								}
								else if((*p)->rchild == NULL) //p有左子树
								{
										dele = *p ;
										*p = (*p)->lchild ;
										free(*p) ;
										*p = NULL ;
								}
								////////////////////////////////////////////////
								//3删除的是有左右子树的节点
								//将删除的节点的右子树的最左节点和删除节点交换。
								//删除交换后的'删除'节点
								else 
								{	
										
										//找到删除节点右子树的最左叶子节点的父节点										
										dele = (*p)->rchild  ;
										while(dele->lchild)
										{
												dele = dele->lchild ;
										}
										//删除dele 自动把p的右子树bf值更新好
										elem =  dele->data ;
										Delete_AVLTree_Node(&(*p)->rchild, dele->data, shorts) ;				
										(*p)->data = elem ;	
										//经证明删除并不改变子树的高度所以shorts 不变= FALSE
											*shorts = FALSE ;
										//更新删除后的节点bf
									
										if(EH == (*p)->bf)
										{
												(*p)->bf = LH ;	
										}
										else if(LH == (*p)->bf)
												Left_Rotation(p, &unbalanced) ;
										else if(RH == (*p)->bf)
												(*p)->bf = EH ;
										
								}
						}
						else if(key < (*p)->data)
						{
								//进入左子树
								Delete_AVLTree_Node(&((*p)->lchild), key, shorts) ;
								//从左子树递归出来
								if(*shorts)
								{
										switch((*p)->bf)
										{
										case RH: //-1 goto -2 RR　RL
												Right_Rotation(p, &unbalanced) ;
												break ;
										case EH: //0
												(*p)->bf = RH ;
												*shorts = FALSE ; //p的子树的高度不变
												break ;
										case LH:
												(*p)->bf = EH ;
												break ;
										}
								}
						}
						else if(key > (*p)->data)
						{
								Delete_AVLTree_Node(&((*p)->rchild), key, shorts) ;

								if(*shorts)
								{
										switch((*p)->bf)
										{
										case RH: //-1 
												(*p)->bf = EH ;
												break ;
										case EH: //0
												(*p)->bf = LH ;
												*shorts = FALSE ;
												break ;
										case LH: //1 goto 2
												Left_Rotation(p, &unbalanced) ;
												break ;
										}
								}
						}
				}
				else
				printf("\nNo find %d key in the tree", key) ;		

}
		

