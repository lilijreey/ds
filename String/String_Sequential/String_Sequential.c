//定长顺序串
//用数组实现

#include <stdio.h>
#include <stdlib.h>

enum status {FAIL, SUCCESS} ;
#define EOT 4 // 传输结束符 ASCII码 等于 4
              //windows 下的”CTRL+D“产生一个EOT
#define MAXSIZELEN 255 //用户最大输入的串长

typedef unsigned char SString[MAXSIZELEN + 1]; //最后的'\0'占一位


void menu(void) ;
//ADT string 的基本操作
//1 生成一个给定串
void StrAssign(char *) ;

//2 判空
int StrEmpty(const char *) ;
 
//3 显示串
void StrShow(const char *) ;

//4 复制串
void StrCopy(char *, const char *) ;

//5 函数4的交互界面
void StrCopy_Interface(char *) ;

//6 返回串的长度
size_t StrLenght(const char *) ;

//7 函数6的交互界面
void StrLenght_Interface(const char *) ;

//8 清空串 数组实现的最简单
void StrClear(char *) ;

//9 串连接
void StrConcat(char*, const char *) ;

//10 函数9的交互界面
void StrConcat_Interface(char*) ;

//11 比较串
int StrCompar(const char *, const char*) ;

//12 函数11的交互界面
void StrCompar_Interface(const char *) ;

//13 求子串
void StrSub(const char *, char *, size_t , size_t) ;

//14 函数13的交互界面
void StrSub_Interface(const char *) ;

//15 指定位置找子串
size_t StrIndex(const char *, const char *, size_t) ;

//16 函数15的交互界面
void StrIndex_Interface(const char *) ;





int main()
{
		int select ;
		static SString str ; // 


		while(1)
		{
				menu() ;
				scanf("%d", &select) ;

				switch(select)
				{
				case 0:
						return 0 ;

				case 1:
						StrAssign(str) ;
						break ;

				case 2:
						StrShow(str) ;
						break ;

				case 3:
						StrCopy_Interface(str) ;
						break ;

				case 4:
						StrLenght_Interface(str) ;
						break ;

				case 5:
						StrClear(str) ;
						break ;

				case 6:
						StrConcat_Interface(str) ;
						break ;

				case 7:
						StrCompar_Interface(str) ;
						break ;

				case 8:
						StrSub_Interface(str) ;
						break ;

				case 9:
						StrIndex_Interface(str) ;
						break ;

				default:
						printf("\n无效输入\n") ;
						break ;
				}
				fflush(stdin) ;
		}
}


void menu(void)
{
	printf("\n*********************************************\n") ;
	printf(  "*1 初始化串                  2 显示串       *\n") ;
	printf(  "*3 串拷贝                    4 显示串长度   *\n") ;
	printf(  "*5 清空串                    6 连接串       *\n") ;
	printf(  "*7 比较两串的大小            8 显示子串     *\n") ;
	printf(  "*9 查找子串                                 *\n") ; 
	printf(  "*                  0 退出                   *\n") ;
	printf(  "*********************************************\n") ;
	printf("请选择：") ;
}

void StrAssign(char *str)
{
		int i = 0 ;
		char ch ;
		
		printf("\n输入字符串以“CTRL+D”结束。") ;
		fflush(stdin) ; //清空输入流

		while((ch = getchar()) !=  EOT)//5就是
		{
				if(i >MAXSIZELEN - 1)
				{
						printf("\n超出最大长度,自动截取\n") ;
						break ;
				}
				str[i++] = ch ;
		}
		str[i] = '\0' ;
}

int StrEmpty(const char* str) 
{
		if(str[0] == '\0')
				return SUCCESS ;
		return FAIL ;
}

void StrShow(const char * str)
{
		int i = 0 ;

		if(StrEmpty(str) == SUCCESS)
		{
				printf("\n串为空\n") ;
		}
		else
		{
				printf("%s", str) ;
		}
}


void StrCopy(char *str, const char *costr)
{
		int i ;

		for(i = 0; costr[i] != '\0'; i++)
				str[i] = costr[i] ;
		str[i] = '\0' ;
} 


void StrCopy_Interface(char * str) 
{
		SString copystr ;

		printf("请输入被拷贝的串\n") ;
		StrAssign(copystr) ;

		StrCopy(str, copystr) ;
} 

size_t StrLenght(const char *str) 
{
		size_t i ;

		if(StrEmpty(str) == SUCCESS)
				return 0 ;
		else
		{
				for(i = 0; str[i] != '\0'; i++)
						;
				return i ;
		}
}

void StrLenght_Interface(const char *str) 
{	
		printf("串长度为：%d", StrLenght(str)) ;
}

void StrClear(char *str) 
{
		str[0] = '\0' ;
		printf("\n清空完成！\n") ;
}

void StrConcat(char *str, const char *conStr) 
{
		//串的连接不能溢出！
		int i ;
		int strlen ;
		/*
		if( ((strlen = StrLenght(str)) + StrLenght(conStr)) <= MAXSIZELEN)
		{
				for(i = 0; conStr[i] != '\0'; i++)
						str[strlen + i] = conStr[i] ;
				str[strlen + i] = '\0' ;
		}
		else
		{
				if(strlen != MAXSIZELEN)
				{
						for(i = 0; (strlen + i) < MAXSIZELEN; i++)
								str[strlen + i] = conStr[i] ;
						str[strlen + i] = '\0' ;
				}
		}*/

		//上面是严蔚敏的防治溢出的方法 下面是我的
		//只要str不是最大 就加不管conStr能否加完
		//很简单吧


		if( (strlen = StrLenght(str)) != MAXSIZELEN)
		{
				for(i = 0; (strlen + i) < MAXSIZELEN  &&  conStr[i] != '\0'; i++)
						str[strlen + i] = conStr[i] ;
				str[strlen + i] = '\0' ;
		}
	
}


void StrConcat_Interface(char *str) 
{	
		SString concatStr;

		printf("请输入连接的串\n") ;
		StrAssign(concatStr) ;

		StrConcat(str, concatStr) ;
}

int StrCompar(const char *str, const char *comstr) 
{
		size_t i ;
		size_t strlen = StrLenght(str) ;
		size_t comlen = StrLenght(comstr) ;

		//先判断两个长度是否一样
		//若不一样肯定不会相等，总有一个先比较玩
		if(strlen == comlen)
		{
				for(i = 0; i < strlen; i++)
				{
						if(str[i] > comstr[i])
								return 1 ;
						else
						{
								if(str[i] < comstr[i])
										return -1 ;
						}
				}
				return 0 ;//相等
		}
		else
		{
				for(i = 0; 1; i++)
				{
						if(str[i] == '\0')
								return -1 ;

						if(comstr[i] == '\0')
								return 1 ;

						if(str[i] > comstr[i])
								return 1 ;
						else
						{
								if(str[i] < comstr[i])
										return -1 ;
						}
				}
		}

}

void StrCompar_Interface(const char *str) 
{
		int compare ;
		SString comStr;

		printf("请输入比较的串\n") ;
		StrAssign(comStr) ;
		
		compare = StrCompar(str, comStr) ;
	
		if(compare == 0)
				printf("\n两串相等\n") ;
		else
		{
				if(compare == 1)
						printf("\n原来的串大\n") ;
				else
						printf("\n原来的串小\n") ;
		}
}


void StrSub(const char *str, char *s, size_t pos, size_t len) 
{
		size_t i ;

		for(i = 0; i < len; i++)
		{
				s[i]= str[pos++ -1] ;
		}
		s[i] = '\0' ;
}

void StrSub_Interface(const char *str) 
{
		size_t pos, len ;
		size_t strlen = StrLenght(str) ; 
		SString s ;

		if(strlen == 0)
				printf("\n串为空，无法求子串\n") ;
		else
		{
				printf("父串的长度为%d \n", strlen) ;
				printf("请输入子串的头位置。（%d>= headSit >0）", strlen) ;
				scanf("%d", &pos) ;
				while(pos >strlen || pos < 1)
				{
						printf("\n无效位置请重输：") ;
						scanf("%d", &pos) ;
				}

				printf("请输入字串的长度，最大为%d. :", strlen - pos +1) ;
				scanf("%d", &len) ;
				while(len < 1 || len > strlen - pos + 1)
				{
						printf("\n无效长度请重输：") ;
						scanf("%d", &len) ;
				}
				StrSub(str, s, pos, len) ;

				//显示字串
				StrShow(s) ;
		}

}

size_t StrIndex(const char *str, const char *s, size_t pos) 
{
		size_t strlen = StrLenght(str) ;
		size_t slen = StrLenght(s) ;
		SString c ;

		while( (pos + slen - 1) <=  strlen)
		{
				StrSub(str,c , pos, slen) ;
				if(StrCompar(s, c) == 0)
						return pos ;
				pos++ ;//这种比较是最简单法
		}
		return -1 ;

}


void StrIndex_Interface(const char *str) 
{	
		size_t pos ;
		size_t len = StrLenght(str) ;
		SString s;

		if(SUCCESS == StrEmpty(str))
				printf("\n空串无法查找\n") ;
		else
		{
				printf("请输入查找的子串\n") ;
				StrAssign(s) ;
				printf("请输入查找的起始位置. (%d>= pos >0): ", len) ;
				while(pos <1 || pos > len)
				{
						printf("\n无效查找位置请重输：") ;
						scanf("%d", &pos) ;
				}
				pos = StrIndex(str, s, pos) ;

				if(pos != -1)
						printf("第一次比配的位置为%d\n", pos) ;
				else
						printf("没有找到匹配子串\n") ;
		}
}