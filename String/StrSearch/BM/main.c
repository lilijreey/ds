/**
 * @file     main.c
 *           
 *
 * @author   lili  <lilijreey@gmail.com>
 * @date     05/27/2013 01:50:00 PM
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Split 's' with separator in 'sep'. An array
 * of sds strings is returned. *count will be set
 * by reference to the number of tokens returned.
 *
 * On out of memory, zero length string, zero length
 * separator, NULL is returned.
 *
 * Note that 'sep' is able to split a string using
 * a multi-character separator. For example
 * sdssplit("foo_-_bar","_-_"); will return two
 * elements "foo" and "bar".
 *
 * This version of the function is binary-safe but
 * requires length arguments. sdssplit() is just the
 * same function but for zero-terminated strings.
 */
//sds *sdssplitlen(const char *s, int len, const char *sep, int seplen, int *count) {
//    int elements = 0, slots = 5, start = 0, j;
//    sds *tokens;
//
//    if (seplen < 1 || len < 0) return NULL;
//
//    tokens = zmalloc(sizeof(sds)*slots);
//    if (tokens == NULL) return NULL;
//
//    if (len == 0) {
//        *count = 0;
//        return tokens;
//    }
//    for (j = 0; j < (len-(seplen-1)); j++) {
//        /* make sure there is room for the next element and the final one */
//        if (slots < elements+2) {
//            sds *newtokens;
//
//            slots *= 2;
//            newtokens = zrealloc(tokens,sizeof(sds)*slots);
//            if (newtokens == NULL) goto cleanup;
//            tokens = newtokens;
//        }
//        /* search the separator */
//        if ((seplen == 1 && *(s+j) == sep[0]) || (memcmp(s+j,sep,seplen) == 0)) {
//            tokens[elements] = sdsnewlen(s+start,j-start);
//            if (tokens[elements] == NULL) goto cleanup;
//            elements++;
//            start = j+seplen;
//            j = j+seplen-1; /* skip the separator */
//        }
//    }
//    /* Add the final element. We are sure there is room in the tokens array. */
//    tokens[elements] = sdsnewlen(s+start,len-start);
//    if (tokens[elements] == NULL) goto cleanup;
//    elements++;
//    *count = elements;
//    return tokens;
//
//cleanup:
//    {
//        int i;
//        for (i = 0; i < elements; i++) sdsfree(tokens[i]);
//        zfree(tokens);
//        *count = 0;
//        return NULL;
//    }
//}
//
//void sdsfreesplitres(sds *tokens, int count) {
//    if (!tokens) return;
//    while(count--)
//        sdsfree(tokens[count]);
//    zfree(tokens);
//}

static int bt[256]; 

void bad_word_table(const char *pattern)
{
    size_t len = strlen(pattern);

    // 全部先设置为不再pattern中
    size_t i;
    for (i=0; i < 256; ++i)
        bt[i] = len;

    //设置在pattern从的字母
    for (i=0; i < len; ++i) {
        int l = bt[(int)pattern[i]] ;
        if (l == len) //没有设置的可以设置
           bt[(int)pattern[i]] = len - i -1;

    }

    for (i=0; i < len; ++i)
        printf("%c dis:%d\n", pattern[i], bt[(int)pattern[i]]);
}

//得到字符c的 的pattern后移距离
//尽可能的返回最多的数值
int get_jump(char c)
{
    return bt[(int)c];
//    return 1; 如果每次都返回1在会退化为朴素查找算法
//        if (index != pattLen) {
//            //说明已经有匹配后缀
//            printf("index=%d tchar=%c pchar%c\n", 
//                   index, text[tPos-offset], patt[tPos-offset]);
//        } else { //无匹配后缀，不匹配
//            ++tPos;
//            ++pPos; 
//        }
}



//返回pattern在text中的第一个字母的下班
//TODO patten 中的重复模式
int boyer_moore(const char *text, const char* patt)
{
    size_t pattLen = strlen(patt);
    size_t textLen = strlen(text);
    
    //1 初始化为在patt的最右边比较
    int tPos = pattLen-1;
    int pPos = pattLen-1;
    int offset = 0;

next:
    while (text[tPos-offset] != patt[pPos-offset]) {
        //1 先确定不匹配的c 是不是在pattern中
        int dis = get_jump(text[tPos-offset]);
        printf("dis=%d %c:%c pPos:%d tPos:%d, offset%d\n", 
               dis, text[tPos-offset], patt[pPos - offset], pPos, tPos, offset);

        tPos += dis - offset;
        pPos += dis - offset;
        if (pPos > pattLen-1)
            pPos = pattLen-1;
        offset = 0;
        if (tPos > textLen)
            goto over;
    }

    //匹配
    if (offset == pattLen-1) {
        printf("找到一个匹配, tPos:%d pPos:%d\n", tPos, pPos);
        return tPos - offset;
    }

    ++offset;
    goto next;

over:
    printf("查找text完毕， tPos:%d, pPos%d, offset:%d\n",
           tPos, pPos, offset);

    return -1;
}

int main()
{
    char text[] = "HERE IS A SIMPLE EXAMPLE";
    char patt[] = "EXAMPLE";

//    char text[] = "SOF FESMSN MMSNS";
//    char patt[] = "SNS";
    bad_word_table(patt);
    int i = boyer_moore(text, patt);
    printf("index=%d\n", i);

    return 0;
}
