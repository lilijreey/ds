字符串匹配算法
----------------------------

* 单模式匹配算法
  KMP BM

* 多模式匹配算法 AC 字符串查找算法的几个考察点
-------------------------------
1. 查找前的准备时间O
     一些查找算法需要在查找前先对给定的子串进行分析.
2. 最坏情况
3. 典型情况O
4. 是否回溯
      如果查找的文本支持随机读取比如在内存中，则回溯并不
      困难，如果文本在流中，那么不需要回溯的算法在更好些


朴素算法
--------------------------------

Rabin-Karp (RK)
-------------------------------

KMP
-------------------------------

Boyer-Moore(BM)
------------------------------
1977 年发表的论文"A Fast String rithm"
是的无需检查文本中的所有字符即可找到字串。
搜索的模式越长，算法的速度越快。
应为该算法会对搜索模式进行预处理
    对于每一次失败的匹配尝试，
    算法都能够使用这些信息来排除尽可能多的无法匹配的位置
* 准备时间 O(patternLen)
* 典型(avg) O close to O(textLen/patternLen)
* 最坏O(n) ??
* 回溯
 

实际情况比KMP更快
* 对查找目标进行预处理 创建*条转表*
    * 坏字符移动规则
    * 好后最移动规则  pattern 中的重复模式
      用来控制两个字符不相同时查找位置应该向右边移动的距离
      在比较时已经知道text中当前比较的char，
      + 如果这个char 不再patten中（char不是pattern中的任意一个字符）
         则下次比较时要向前移动patternLen的长度。
      + 如果这个char 是pattern中的一个字符，
         则下次比较时要把pattern向右移动到pattern中的相同字符
         和这个字符对齐. = patternLen - charIndex


* 起始状态
   查找指针开始于查找模式的右端
   pattern           -> corn
   当前查找的位置    ->    *
   文本              -> Oaks from acorns grow


* 比较
    不相同
      1 如果这个char 不再patten中（char不是pattern中的任意一个字符）
         则下次比较时要向前移动patternLen的长度。
      2 如果这个char 是pattern中的一个字符，
         则下次比较时要把pattern向右移动到pattern中的相同字符
         和这个字符对齐. = patternLen - charIndex
     相同
       左移index，再次比较, 直到不相同，或者text比较完了或者patten比较完了
    区分比较index是不是在pattern最右边的情况
  
  //can set cursorcolumn e.g. 
TEXT: HERE IS A SIMPLE EXAMPLE
PATTERN: EXAMPLE
         0123456
PL = strlen(PATTERN) =7

//index拖着PATTERN走
step:
  1  HERE IS A SIMPLE EXAMPLE
     EXAMPLE                    //not match S不再PATTERN 中，所有右移 PL=7
           *
  2  HERE IS A SIMPLE EXAMPLE
            EXAMPLE             //not match P在PATTERN 中，所以右移(6-4)=2 当前所在pattern下标 - P在patern中的下标 
                  *
  3  HERE IS A SIMPLE EXAMPLE
              EXAMPLE           //match index lift shift 1 offest
                    *
  4                *            // ...
  5               *
  6              *              // match index lift shift 1 offest
  7  HERE IS A SIMPLE EXAMPLE
              EXAMPLE           //not match && index not in end,I 不在pattern中 right-shift = PL=7
                *               //因为index会拖着pattern走，所以pattern lift-shift 7 - (6-2) =3 (see 8a)
                                
                                //由于好后缀中有E这个字符，在pattern中有重复
                                //使用重复模式, 计算重复模式直接的距离 两个E相差6-0=6 
                                //所以pattern lift-shift=6 index lift-shift=6+ (6-0)=12
  8a HERE IS A SIMPLE EXAMPLE   
                 EXAMPLE        //使用坏字符 移动规则, pattern 移动了 3
                       *        
  8b HERE IS A SIMPLE EXAMPLE   
                    EXAMPLE     //使用重复模式 移动规则, pattern 移动了 6
                          *     //比8a移动的多， 所以7这种情况下，需要计算
                                //移动 = max(坏字符，重复模式)
                                //lift-shift = 6 - P(4) = 2

  9  HERE IS A SIMPLE EXAMPLE 
                      EXAMPLE
                            *
