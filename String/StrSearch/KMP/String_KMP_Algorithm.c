//624行有漏洞 记得补一下

#include <stdio.h>
#include <stdlib.h>

enum status {FAIL, SUCCESS} ;
#define EOT 4 // 传输结束符 ASCII码 等于 4
              //windows 下的”CTRL+D“产生一个EOT

#define LIST_INIT_SIZE 101 //初始化大小 含1个'\0'
#define LISTINCREMENT 50  //增加大小
typedef unsigned char Char ;



typedef struct sString
{
		Char *pWchar ;
		int length ;  //串的长度 不含'\0'
		int StrSize ; //串的空间 含'\0'
}String, *pString ;

//子串的结果
typedef struct sSubString
{
		pString sub ;
		int *pNext ; ////对应子串的next值动态开辟
		int subLength ; //子串的长度不包含'\0' ;
}Sub, *pSub ;



 

void menu(void) ;
//ADT string 的基本操作
//0 初始化串 
void CreateString(pString) ;

//01 验满增加串空间
int If_Full_Add_String_Memory(pString) ;

//1 生成一个给定串
void StrAssign(pString) ;

//2 判空
int StrEmpty(const String *) ;
 
//3 显示串
void StrShow(const String *) ;

//4//////////////////////////////求模式串的next//严蔚敏个垃圾 初始从1开始 草
void Get_Next(pSub) ;

// 4.5 初始化子串
void Initial_SubString(pSub *) ;

//5创建子串
void Create_SubString(pSub) ;

//子串next显示
void Show_SubString(const pSub) ;
 
//释放子串释放后 置为空
void Destroy_SubString(pSub) ;
 
//6 返回串的长度
int Strlength(const String *) ;

//7 函数6的交互界面
void Strlength_Interface(const String *) ;

//8清空母串 即释放母串的Char空间
void ClearString(pString) ;

//9 串连接
void StrConcat(pString, pString) ;

//10 函数9的交互界面
void StrConcat_Interface(pString) ;

//11 比较串
int StrCompar(const String *, const String *) ;

//12 函数11的交互界面
void StrCompar_Interface(const String *) ;

//13 求子串 起始位置， 字串长度
void StrSub(const String *, String *, int , int) ;

//14 函数13的交互界面
void StrSub_Interface(const String *) ;

//15 指定位置找子串
int StrIndex(const String *, const Sub *, int) ;

//16 函数15的交互界面
void StrIndex_Interface(const String *) ;

//17 销毁串
void DestroyString(pString) ;

//18 替换子串
void Replace(pString, const String *, const Sub *) ;

//19 函数18的交互界面
void Replace_Interface(pString) ;

//20 移动母串中从pos位置的子串到to位置
void Remove(pString, int, int) ;

//21 扩充空间函数 size = 新空间大小
void AugmentString(pString, int size) ;




int main()
{
		String string = {0} ;
		int select ;

		CreateString(&string) ; //initiate string
		while(1)
		{
				menu() ;
				scanf("%d", &select) ;

				switch(select)
				{
				case 0:
						return 0 ;
	
				case 1:
						StrAssign(&string) ;
						break ;

				case 2:
						StrShow(&string) ;
						break ;

				case 3:
						StrConcat_Interface(&string) ;
						break ;

				case 4:
						Strlength_Interface(&string) ;
						break ;

				case 5:
						ClearString(&string) ;
						break ;

				case 6:
						StrCompar_Interface(&string) ;
						break ;

				case 7:
						StrSub_Interface(&string) ;
						break ;

				case 8:
						StrIndex_Interface(&string) ;
						break ;

				case 9:
						Replace_Interface(&string) ;
						break ;

				default:
						printf("\n无效输入\n") ;
						break ;
				}
				fflush(stdin) ;
		}
}


void menu(void)
{
	printf("\n**********************************************\n") ;
	printf(  "*1 母串赋值                  2 显示串        *\n") ;
	printf(  "*3 链接串                    4 显示串长度    *\n") ;
	printf(  "*5 清空串                    6 比较两串的大小*\n") ;
	printf(  "*7 返回子串                  8 查找          *\n") ; 
	printf(  "*9 替换                      0 退出          *\n") ;
	printf(  "**********************************************\n") ;
	printf("请选择：") ;
}


void CreateString(pString pstr) 
{
		if( (pstr->pWchar = (Char*)malloc(LIST_INIT_SIZE * sizeof(Char))) == NULL)
		{
				printf("\nMemory error!\n") ;
				exit(1) ;
		}
		//c型的string '\0'
		pstr->pWchar[0] = '\0' ;

		pstr->length = 0 ;
		pstr->StrSize = LIST_INIT_SIZE ;
}

void DestroyString(pString pstr)
{
		if(pstr->pWchar != (Char*)NULL)
		{
				free(pstr->pWchar) ;
				pstr->pWchar = NULL ;

				pstr->length = pstr->StrSize = -1 ;
		}
		else
				printf("\n重复删除，次删除无效\n") ;
}


int If_Full_Add_String_Memory(pString pstr)
{
		int i ;
		Char *newStr ;
		if(pstr->length == pstr->StrSize - 1) //注意'\0'
		{
				if( (newStr = (Char*)malloc((pstr->StrSize + LISTINCREMENT)  * sizeof(Char))) == NULL)
				{
						printf("\nMemory error!\n") ;
						exit(1) ;
				}
				//copy string to newstring
				for(i = 0; i == pstr->length; i++)
				{
						newStr[i] = pstr->pWchar[i] ;
				}

				pstr->StrSize = pstr->length + LISTINCREMENT ;

				return SUCCESS ;
		}
		else
				return FAIL ;
} 


void StrAssign(pString pstr) 
{	
		int i = 0 ;
		Char ch ;
		
		printf("\n输入字符串以“CTRL+D”结束\n") ;
		fflush(stdin) ; //清空输入流

		while((ch = getchar()) !=  EOT)//5就是
		{
			If_Full_Add_String_Memory(pstr) ; //先判断满没
			
			pstr->pWchar[i++] = ch ;
			++pstr->length ;
		}
		pstr->pWchar[i] = '\0' ;
}


int StrEmpty(const String *pstr) 
{
		if(pstr->length == 0)
				return SUCCESS ;
		else
				return FAIL ;
}


void StrShow(const String *pstr)
{
		if(StrEmpty(pstr) == SUCCESS)
		{
				printf("\n串为空\n") ;
		}
		else
		{
				printf("%s", pstr->pWchar) ;
		}
}

void Initial_SubString(pSub *sub)
{
		if((*sub = (pSub)malloc(sizeof(Sub))) == NULL)
		{
				printf("\nMemory error!\n") ;
				exit(1) ;
		}
		(*sub)->pNext = NULL ;
		(*sub)->sub = NULL ;
		(*sub)->subLength = 0 ;
} 


void Create_SubString(pSub subString)
{
		
		if((subString->sub = (pString)malloc(sizeof(String))) == NULL) 
		{
				printf("\nMemory Error!\n") ;
				exit(1) ;
		}

		CreateString(subString->sub) ;
		StrAssign(subString->sub) ;

		subString->subLength = subString->sub->length ;

		//开辟next
		if((subString->pNext = (int *)malloc(sizeof(int) * subString->subLength)) == NULL)
		{
				printf("\nMemory Error!\n") ;
				exit(1) ;
		}
}

 
void Get_Next(pSub subString) 
{
		int i = 0, j = -1;// i和j的起始值很重要。 i=1 的情况通过j的初始值为1隐式赋值了
 //当前的next
//这里有些别扭因为next从1开始而下标从0开始
//我决定对每一个next[j]减一
		//

		subString->pNext[0] = -1 ;

		while(i < subString->subLength - 1) //因为++后在赋值所以要 -1
		{
				if(subString->sub->pWchar[i] == subString->sub->pWchar[j] || j == -1)
				{
						++j ;
						++i ;
						//注意先++ 在赋值
						if(subString->sub->pWchar[i] != subString->sub->pWchar[j])
								subString->pNext[i] = j ;
						else
								subString->pNext[i] = subString->pNext[j] ;
				}
				else
				{
						j = subString->pNext[j] ;
				}
		}
}

void Destroy_SubString(pSub sub)
{
		if(sub != NULL)
		{
				if(sub->sub != NULL)
						free(sub->sub) ;
				free(sub) ;
				sub = NULL ;
		}
}


void Show_SubString(const pSub subString) 
{
		int i = 0 ;

		StrShow(subString->sub) ;

		while(i < subString->subLength)
		{			
				printf("%d ",subString->pNext[i]) ; 
				++i ;
		}
}


void ClearString(pString str) 
{
		if(str->pWchar != NULL)
		{
				free(str->pWchar) ;
				str->length = 0 ;
		}

}


int Strlength(const String * str) 
{
		return str->length ;
}

void Strlength_Interface(const String * str) 
{	
		printf("串长度为：%d", Strlength(str)) ;
}


void StrConcat(pString n_str, pString f_str) 
{
			int i = 0 ;
			int j = f_str->length ;

			while(i <= n_str->length) //包括最后'\0'的复制
			{
					If_Full_Add_String_Memory(f_str) ;
					f_str->pWchar[j++] = n_str->pWchar[i++] ;
					f_str->length ++ ;
			}

			//释放连接串
			DestroyString(n_str) ;
}


void StrConcat_Interface(pString f_str) 
{	
		String n_str ;

		printf("请输入连接的串\n") ;
		CreateString(&n_str) ;
		StrAssign(&n_str) ;

		StrConcat(&n_str, f_str) ;
}




int StrCompar(const String *comstr, const String *str) 
{
		int i ;
		int strlen = str->length ;
		int comlen = comstr->length ;

		//先判断两个长度是否一样
		//若不一样肯定不会相等，总有一个先比较玩
		if(strlen == comlen)
		{
				for(i = 0; i < strlen; i++)
				{
						if(str->pWchar[i] > comstr->pWchar[i])
								return 1 ;
						else
						{
								if(str->pWchar[i] < comstr->pWchar[i])
										return -1 ;
						}
				}
				return 0 ;//相等
		}
		else
		{
				for(i = 0; 1; i++)
				{
						if(str->pWchar[i] == '\0')
								return -1 ;

						if(comstr->pWchar[i] == '\0')
								return 1 ;

						if(str->pWchar[i] > comstr->pWchar[i])
								return 1 ;
						else
						{
								if(str->pWchar[i] < comstr->pWchar[i])
										return -1 ;
						}
				}
		}
	
}


void StrCompar_Interface(const String *str)
{
		int compare ;
		String comStr;

		printf("请输入比较的串\n") ;
		CreateString(&comStr) ;
		StrAssign(&comStr) ;

		compare = StrCompar(&comStr, str) ;
	
		if(compare == 0)
				printf("\n两串相等\n") ;
		else
		{
				if(compare == 1)
						printf("\n原来的串大\n") ;
				else
						printf("\n原来的串小\n") ;
		}
		//释放比较串
		DestroyString(&comStr) ;
} 


void StrSub(const String *str, String *s, int pos, int len) 
{
		int i ;

		for(i = 0; i < len; i++)
		{
				If_Full_Add_String_Memory(s) ;
				s->pWchar[i]= str->pWchar[pos++ -1] ;
				s->length++ ;
		}
		s->pWchar[i] = '\0' ;
		s->length++ ;
}


void StrSub_Interface(const String *str) 
{
		int pos, len ;
		int strlen = str->length ; 
		String sub;

		CreateString(&sub) ; //初始化子串

		if(strlen == 0)
				printf("\n串为空，无法求子串\n") ;
		else
		{
				printf("父串的长度为%d \n", strlen) ;
				printf("请输入子串的头位置。（%d>= headSit >0）", strlen) ;
				scanf("%d", &pos) ;
				while(pos >strlen || pos < 1)
				{
						printf("\n无效位置请重输：") ;
						scanf("%d", &pos) ;
				}

				printf("请输入字串的长度，最大为%d. :", strlen - pos +1) ;
				scanf("%d", &len) ;
				while(len < 1 || len > strlen - pos + 1)
				{
						printf("\n无效长度请重输：") ;
						scanf("%d", &len) ;
				}
				StrSub(str, &sub, pos, len) ;

				//显示字串
				StrShow(&sub) ;

				//释放子串
				DestroyString(&sub) ;
		}

}


///KMP 算法核心
int StrIndex(const String *str, const Sub *sub, int pos) 
{
		int i = pos - 1, strlen = str->length ;
		int j = 0         , sublen = sub->subLength  ;

		while(i < strlen && j < sublen)
		{
				if(sub->sub->pWchar[j] == str->pWchar[i] || j == -1)// j == -1 是两个头元素都不匹配
				{
						i++ ;
						j++ ;
						if( j == sublen) 
								return i - sublen ; // 这返回的是实际位置 即下标从0开始
				}
				else
				{
						j = sub->pNext[j] ;
				}
		}
		return -1 ;
}



void StrIndex_Interface(const String *str) 
{	
		int pos ;
		int len = str->length ;
		pSub sub = NULL;

		
		if(SUCCESS == StrEmpty(str))
				printf("\n空串无法查找\n") ;
		else
		{
				printf("请输入查找的子串\n") ;
				Initial_SubString(&sub) ;
				Create_SubString(sub) ; //Sub 类型子串
				Get_Next(sub) ; // 子串next

				printf("请输入查找的起始位置. (%d>= pos >0): ", len) ;
				scanf("%d", &pos) ;
				
				while(pos <1 || pos > len)
				{
						printf("\n无效查找位置请重输：") ;
						scanf("%d", &pos) ;
				}
				pos = StrIndex(str, sub, pos) ;

				if(pos != -1)
						printf("第一次比配的位置为%d\n", pos) ;
				else
						printf("没有找到匹配子串\n") ;
		}
		//销毁子串
		Destroy_SubString(sub) ;
}

void Replace(pString str, const String *reStr, const Sub *sub) 
{
		int i = 0 ; //母串的下标
		int j = str->length ;
		int sublen = sub->subLength ;
		int relen = reStr->length ;
		int tag = relen > sublen ? 1 : 0 ;

		if(tag == 1)
		{
				while(1)
				{
						i = StrIndex(str, sub, 0) ;
						if(i == -1)//无匹配子串跳出
								break ;

						AugmentString(str, j + relen - sublen + 1) ;
						Remove(str, i + relen, i + 2 * relen - sublen) ;
						
						for(j = 0 ; j < relen; j++)
								str->pWchar[i++] = reStr->pWchar[j] ;
						str->length += relen - sublen ;
				}	
		}
		else
		{
				while(1)
				{
						i = StrIndex(str, sub, 0) ;
						if(i == -1)
								break ;

						Remove(str, i + sublen, i + relen) ;
						
						for(j = 0; j < relen; j++)
						{
								str->pWchar[i++] = reStr->pWchar[j] ;
						}
						str->length -= sublen - relen ;
				}
		}
}

void Replace_Interface(pString str) 
{
		String reStr ;
		pSub sub = NULL ;

		//输入被替换串
		printf("\n请输入被替换串\n") ;
		Initial_SubString(&sub) ;
		Create_SubString(sub) ;
		Get_Next(sub) ;

		//输入替换串
		printf("\n请输入替换串\n") ;
		CreateString(&reStr) ;
		StrAssign(&reStr) ;

		Replace(str, &reStr, sub) ;

		//释放串
		DestroyString(&reStr) ;
		Destroy_SubString(sub) ;
}


void Remove(pString str, int pos , int to) 
{
		int strlen = str->length ;
		int moveLen = strlen - pos + 1 ; //要移动的子串的长度 加上 '\0' ;
		int sit ;

		
		//判断是向前移 还是 向后移
		if( pos < to) // 后移
		{
				sit = to + moveLen -1 ;//最后一位的下标
				//检查溢出
		        if(sit > str->StrSize) 
				{
						printf("\n空间不足无法移动\n") ;
				}
				else
				{
						while(sit >= pos)
						{
								str->pWchar[sit--] = str->pWchar[strlen--] ;
						}
				}
		}
		else //前移
		{
				while(pos <= strlen)
				{
						str->pWchar[to++] = str->pWchar[pos++] ;
				}
		}
}


void AugmentString(pString str, int size)
{
		int i =0 ;
		Char * newChars ; 
		int strlen = str->length ;

		if(str->StrSize < size)
		{

				//创建新空间
				if((newChars = (Char *)malloc(sizeof(Char) * size)) == NULL)
				{
						printf("\nMemory Error\n") ;
						exit(1) ;
				}

				if(size < strlen + 1 ) //溢出检查
				{
						printf("\n拷贝空间不足，无法拷贝\n") ;
				}
				else
				{
						while(i <= strlen)
						{
								newChars[i] = str->pWchar[i] ;
								i++ ;
						}
						//释放旧空间
						free(str->pWchar) ;
						//连接新空间
						str->pWchar = newChars ;

						str->StrSize = size ;
				}
		}
} 