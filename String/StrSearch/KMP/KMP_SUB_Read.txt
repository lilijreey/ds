####子串的next值算法
#注意i 和 j。 i不回溯 j 不相等后回溯到 j[next] 继续比较直到 j== -1
#注意第二的位置的next的隐式初始化。 对于一个子串是从第三个位置开始计算next的
#注意3 先加一在赋值


void Get_Next(pSub subString) 
{
		int i = 0, j = -1;// i和j的起始值很重要。 i=1 的情况通过j的初始值为1隐式赋值了
 //当前的next
//这里有些别扭因为next从1开始而下标从0开始
//我决定对每一个next[j]减一
		//

		subString->pNext[0] = -1 ;

		while(i < subString->subLength)
		{
				if(subString->sub->pWchar[i] == subString->sub->pWchar[j] || j == -1)
				{
						++j ;
						++i ;
						//3注意先++ 在赋值
						//进一步改进
						if(subString->sub->pWchar[i] != subString->sub->pWchar[j]
								subString->pNext[i] = j ;	
						else
								subString->pNext[i] = subString->pNext[j] ;
				}
				else
				{
						j = subString->pNext[j] ;
				}
		}
}


size_t StrIndex(const String *str, const Sub *sub, size_t pos) 
{
		size_t i = pos - 1, strlen = str->length ;
		int j = 0         , sublen = sub->subLength  ;

		while(i < strlen || j < sublen)
		{
				if(sub->sub->pWchar[j] == str->pWchar[i] || j == -1)// j == -1 是两个头元素都不匹配
				{
						i++ ;
						j++ ;
						if( j == sublen) 
								return i - sublen ; // 这返回的是实际位置 即下标从0开始
				}
				else
				{
						j = sub->pNext[j] ;
				}
		}
		return -1 ;
}