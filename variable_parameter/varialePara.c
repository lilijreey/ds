
//varargs 
//va_list
//va_dcl
//va_start
//va_end
//va_arg
//
#include <stdio.h>

#if 0
#include <varargs.h>

void error(va_alist) va_dcl
{
		int i, count = 0 ;	
		va_list ap ;
		va_start(ap) ; //没有初始化不行崩溃
		////////////////////
		for(i = 3; i > 0; i--)
		{
				count = va_arg(ap, int) ; //每回取值，并更新参数列表， 但不提供溢出检测
				printf("%d\n", count) ;
		}
		va_end(ap) ;
}

#else
///////ANSI C
#include <stdarg.h>

void error(int value, ...) //////可变参数在固定参数的基础上建立
{
		int i, count = 0 ;	
		va_list ap ;
		va_start(ap, value) ; //注意和<varargs.h>的不同
		///////////////////////这个取固定参数部分
		count = value ; 
		printf("%d\n", count) ;
		//////////////////////下来的是变长部分
		for(i = 2; i > 0; i--)
		{
				count = va_arg(ap, int)  ; ///////变长部分还是用va_arg 去参数
				printf("%d\n", count) ;
		}
		va_end(ap) ;
}

#endif

int main()
{
		error(1,2,3) ;
			return 0 ;		
}
