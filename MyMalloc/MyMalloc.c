/*
**定义一个不易发生错误的内存分配器
**通过alloc调用malloc并进行检查，确保返回的执针不是NULL
**《Pointers On C》 Chapter 11
*/


#include <stdio.h>
#include "alloc.h"

#undef malloc //取消头文件的malloc的屏蔽

void * alloc(size_t size)
{
		void * new_mem ;

		//请求所需分配的内存，并检查确实分配成功//
		new_mem = malloc(size) ;
		if(new_mem == NULL)
		{
				printf("Out of memory!\n") ;
				exit(1) ;
		}
		return new_mem ;
}


