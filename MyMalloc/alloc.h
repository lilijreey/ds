/*
**定义一个不易发生错误的内存分配器
**通过alloc调用malloc并进行检查，确保返回的执针不是NULL
**《Pointers On C》 Chapter 11
*/

#include <stdlib.h>

#define malloc //用于屏蔽malloc 库函数 
               //以防止其他代码块直接插入程序而导致的偶尔调用malloc的行为

#define MALLOC(num, type)  (type *)alloc( (num) * sizeof(type) ) 
			   //alloc 是自定义的一个分配函数
extern void *alloc(size_t size) ; //这是alloc的声明

