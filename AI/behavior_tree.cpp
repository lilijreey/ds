/**
 * @file     behavior_tree.cpp
 *           行为树实现
 *           用了代替ＦＳＭ，比ＦＳＭ更易维护和扩展.
 * 
 * 
 * FSM 的缺点，状态多了以后维护困难,扩展也很困难．内置逻辑表达不清晰
 *      所欲有了分层的FSM(HFSM), 这已经和分层相同了
 * 
 * 
 * 特征: 树状结构,每次都从顶层节点开始搜索.找到匹配的node,然后递归向下执行
 *       直到执行完叶子节点, 所有的非叶子节点，都返回boolean() 来表示
 *       是否被选择中
 *       
 * 
 *     节点 实现的做什么，可能是逻辑判断，也可能是执行特定动作
 *       基本的节点类型:
 *                +. 行为节点 叶子节点, 用于返回true
 *                   行为节点一般分为两种状态，进行中，已结束
 *
 *             --------------------控制节点--------------------------
 *                +. 选择节点 子节点
 *                      + selectNode , 选择其子节点的某一个进行 like case
 *                      + sequeceNode, 一个一个执行(如果返回false) 就不执行下去了 like return
 *                      + paralleNode, 一起执行所有的子节点 
 *                      
 *          每个节点都有一个前提部分，包含选择这个节点的条件
 *           前台要实现 and/nor/or 
 *           
 *                
 *
 *
 *          通关树可以表达程序设计语言的
 *          流程控制
 *          if/ else
 *          
 *          for
 *          
 *          and/not/or
 *          
 *
 *     和行为树配合的还有外部黑白（环境) ,事件
 *
 * Tree 的构建
 *    1. 描述语言 -> 翻译
 *    2. 游戏语言
 * 
 *  Node() -> fjeosf ->
 * 
 *         -> jfoesfjjsef -> jfoesfj
 *         
 *                        -> jfeosfjeif
 *                        -> jfoesjfoef
 *                        -> ooioijfief
 *                        -> oofefffff
 *          
 *          
 * 
 * @author   lili  <lilijreey@gmail.com>
 * @date     10/02/15 10:05:46
 * 
 *     
 *      我去，砍柴, 砍一次体力减少1, 如果体力<3,回家吃饭，
 *      吃晚饭，再去砍柴
 *
 *  Root() ->S() -> true: 砍柴()
 *               -> if s < 3 :吃饭()
 * 
 *
 * 
 */

#include <stdlib.h>
#include <list>

struct BlackBoard
{
  int _s=3;

};

class NodeBase
{
 public:
  enum Type {
//    SELECT,
//    SEQUEC,
    ACTION,
    SELECT
  };


  NodeBase(Type type) {_type = type};
  virtual ~NodeBase() {};

  virtual bool check(const BlackBoard *) = 0;
  virtual bool exec(BlackBoard *) = 0;

  void add_child(NodeBase *child) { _childs.push_back(child); }

  // empty
  // size

  Type _type;
  std::list<NodeBase *>_childs;
}


class SelectNode : public NodeBase
{
  virtual bool check(const BlackBoard *) 
  virtual bool exec(BlackBoard *) = 0;
}


int main(int argc, char *argv[])
{
  auto root = SelectNode();
  auto e = SelectNode(SELECT);
  e->add_child(ActionNode(eat));
  e->add_child(ActionNode(fat));

  root->add_child(e);

  run_bt(root);

  return 0;
}
