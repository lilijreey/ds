#!/usr/bin/env ruby

## 遗传算法例子
#使用 GA 计算 a + 2b + 3c + 4d =40 的解
#为了快速搜索，限定自变量取值范围[0,30]

# 1. 知识表达:
# 由于解有四个变量，所以基因由4个变量组成
# 用一个4-tuple 表达
# [a,b,c,d]

# 2. 适应函数
# f = abs(a+2b+3c+4d - 30)
# 值越小适应度越高，如果=0则找到一组解
#
# 3. 初始种群InitCount，随机6个种子
#
# 4. 选择

MAX_NUM = 30
$genBest =[]

def make_chrom(a,b,c,d)
  [a,b,c,d]
end

def init(size=6)
  ret = []
  size.times do
    ret << make_chrom(rand(MAX_NUM),rand(MAX_NUM),rand(MAX_NUM),rand(MAX_NUM))
  end

  return ret
end

def finess(chrom)
  ## f = 1 / abs(a+2b+3c+4d - 30) + 1 越小值越大，概率越大
  # + 1 是为了避免除零
  1.0 / ((chrom[0] + 2*chrom[1] + 3*chrom[2] + 4*chrom[3] - 30).abs + 1)
end

def res(chrom)
  chrom[0] + 2*chrom[1] + 3*chrom[2] + 4*chrom[3] - 30
end

def select(set)
  ## 对每一个基因计算适应度，然后根据适应度使用
  #->[finess, e]

  total_finess = 0
  best_fin = 0
  best_chrom = nil
  set = set.map do |e|
    _fin = finess(e)
    if _fin > best_fin
      best_fin = _fin
      best_chrom = e
    end
    [0, _fin, total_finess, total_finess+=_fin, e]
  end

  $genBest << [best_fin, res(best_chrom), best_chrom.dup, set.uniq.size]

  ## 轮盘赌选择方法选择保留的基因
  # 1. 计算适应值总和T
  # 2. 根据各自适应值，计算对应取值范围
  # 3. 生成[0-T] 随机数，保留随机数所在区间的基因
  # (option) 精英方法，直接保留最好的N个基因

  
  ## 随机set.size 次，保留所有被选中基因, 种群数量不变，会存在重复情况
  newSet =[] 
  set.size.times do 
    r = rand(0..total_finess)
    ele = set.find {|e| (e[2]..e[3]).cover? r }
    ele[0] +=1
    newSet << ele[-1].dup
  end

  puts "total #{total_finess}"

  return newSet
end

def crossover(set, rate=0.25)
  ## 交叉
  #1. 先选需要交叉的基因，再交叉
  need_set = set.select {|e| rand < rate}
  if need_set.size <=1 
    puts "no neaf cross chrom"
    return set
  end

  (0..need_set.size-1).step(2) do |i|
    # need_set[i] 和 need_set[i+1] 循环交叉
    j = i + 1
    if j >= need_set.size; j = 0 end
      
    pos = rand(4)
    #puts "crossover pos:#{pos},#{i}:#{j} #{need_set[i]} : #{need_set[j]}"
    #puts "crossover pos:#{pos},#{i}:#{j} #{need_set} "
    pos.times do |idx|
      t = need_set[i][idx]
      need_set[i][idx] = need_set[j][idx]
      need_set[j][idx] = t
    end
  end

  return set
end


def mutaion(set, rate=0.1)
  # 变异概率的对象为基因中的碱基
  # 假设编变异概率为0.01 则6个样本，每个样本4个碱基
  # 总共24个，乘以0.01约等于0.24小于1个,所以不变异

  #如果大于1个则对整个样本所有碱基进行随机,在[0.23]中随机两次，然后变异
  count = set.size * 4 * rate
  return set if count < 1

  count.to_i.times do
    pos = rand(set.size * 4)
    set[pos/4][pos%4] = rand(MAX_NUM)
  end


  return set
end


def nextGen(set)
  set = select(set)
  set = crossover(set)
  mutaion(set)
end

def has_resoule?(set)
  set.find {|chrom| chrom[0] + 2*chrom[1] + 3*chrom[2] + 4*chrom[3] == 30 }
end

## 初始总群尽量大一些，好找

def eveolue(count, generat=30)
  $genBest = []
  set = init(count)

  curr_gen = 1
  while curr_gen <= generat
    set = nextGen(set)
    if (ret = has_resoule?(set)) != nil
      puts "!!!find result #{ret} in #{curr_gen} gen"
      break
    end
    #puts "genen #{curr_gen} #{set}"
    curr_gen += 1

    if set.uniq.size == 1
      puts "gen #{curr_gen} get a local result #{set}"
      #break
    end
  end
end

def showBest
  $genBest.each_index do |i|
    puts "[#{i}] #{$genBest[i]}"
  end

end
