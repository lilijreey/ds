# python3
# coding=utf-8

## Adaptive 线性神经元算法

##使用神经网络 Rosenblatt 感知器,训练iris,数据分类器(网络)

import pandas as pd
from numpy.random import seed
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

## 批量递归下降batch
class AdalineBGD(object):
    def __init__(self, eta=0.01, n_iter=50):
        self.eta = eta #学习率
        self.n_iter = n_iter #训练次数

    def net_input(self, samples):
        return np.dot(samples, self.w_[1:]) + self.w_[0]

    def activation(self, samples):
        # 线性函数，直接返回输入为输出 y=x;
        return self.net_input(samples)

    def fit(self, samples, y):
        self.w_ = np.zeros(1 + samples.shape[1])
        self.cost_ = []

        for i in range(self.n_iter):
            output = self.net_input(samples)
            errors = (y - output) ##得到所有样本误差
            ## 使用每个样本的j维度的值乘以对应误差之和，更新对应维度
            ## samples.T 返回samples的转置
            self.w_[1:] += self.eta * samples.T.dot(errors)
            self.w_[0] += self.eta * errors.sum()
            cost = (errors ** 2).sum() / 2.0
            self.cost_.append(cost)

        return self

    def predict(self, samples):
        return np.where(self.activation(samples) >= 0.0, 1, -1)


class AdalineSGD(object):
    def __init__(self, eta=0.01, n_iter=10,
                 isShuffle=True, random_state=None):
        self.eta = eta
        self.n_iter = n_iter
        self.w_init = False
        self.isShuffle = isShuffle
        if random_state:
            seed(random_state)

    def predict(self, samples):
        return np.where(self.activation(samples) > 0.0, 1, -1)

    def activation(self, samples):
        return self.net_input(samples)

    def net_input(self, samples):
        return np.dot(samples, self.w_[1:]) + self.w_[0]


    def _update_w(self, si, target):
        output = self.net_input(si)
        error = (target - output)
        self.errors_.append(error)
        self.w_[1:] += self.eta * si.dot(error)
        self.w_[0] += self.eta * error
        ## TODO 
        cost = 0.5 * error**2
        return cost

    def _init_w(self, size):
        self.w_ = np.zeros(1+size)
        self.w_isInit = True

    def _shuffle(self, samples, y):
        # 随机打乱样本顺序
        r = np.random.permutation(len(y))
        return samples[r], y[r]

    def partial_fit(self, samples, y):
        if not self.w_isInit:
            self._init_w(samples.shape[1])

        if y.ravel().shape[0] > 1:
            for si, target in zip(samples, y):
                self._update_w(s1, target)
        else:
            self._update_w(samples, y)

        return self


    def fit(self, samples, y):
        self._init_w(samples.shape[1])
        self.cost_ = []
        self.errors_ = []
        for i in range(self.n_iter):
            if self.isShuffle: ##每次都随机样本可以减少噪音数据的影响
                samples, y = self._shuffle(samples, y)

            cost = []
            for si, target in zip(samples, y):
                cost.append(self._update_w(si, target))
            avg_cost = sum(cost)/len(y)
            self.cost_.append(avg_cost)

        return self


def plot_decision_regions(samples, y, classifier, resolution=0.02):
    markers = ('s', 'x','o','^','v')
    colors=('red','blue','lightgreen','gray','cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])

    #plot the decision surface
    x1_min, x1_max = samples[:, 0].min() -1, samples[:,0].max() + 1
    x2_min, x2_max = samples[:, 1].min() -1, samples[:,1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
                           np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())

    #plot class samples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=samples[y==cl, 0], y=samples[y==cl,1],
                    alpha=0.8, c=cmap(idx), marker=markers[idx], label=cl)

## 读取数据
##'https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data',
df = pd.read_csv('./iris.data', header=None)

## 显示花的种类
y = df.iloc[0:100, 4].values # 得到第3列的所有值 -> array
y = np.where(y=='Iris-setosa', -1, 1)# 把对应的值都替换为-1,1
samples= df.iloc[0:100, [0,2]].values # 选择[0,1,2] 列的值

### 使用标准化样本,进行训练
## 首先进行数据标准化
samplesStd = np.copy(samples)
##对每个维度的数据执行标准化操作
samplesStd[:, 0] = (samples[:,0] - samples[:, 0].mean()) / samples[:,0].std()
samplesStd[:, 1] = (samples[:,1] - samples[:, 1].mean()) / samples[:,1].std()

#plt.scatter(samples[:50, 0],  samples[:50,1],
#            color='red', marker='o', label='setosa')
#plt.scatter(samples[50:100, 0],  samples[50:100,1],
#            color='blue', marker='x', label='versicolor')
#plt.xlabel('petal length')
#plt.ylabel('sepal length')
#plt.legend(loc='upper left')
#plt.show()

### 分别使用0.01 和0.0001进行训练
#fig, ax = plt.subplots(nrows =1, ncols=2, figsize=(8,4))
#ada1 = AdalineBGD(n_iter=10, eta=0.01).fit(samples, y)
#ax[0].plot(range(1, len(ada1.cost_) + 1),
#           np.log10(ada1.cost_), marker='o')
#ax[0].set_xlabel('Epochs')
#ax[0].set_ylabel('log(Sum-squared-error')
#ax[0].set_title('AdalineBGD learning rate 0.01')


#ada2 = AdalineBGD(n_iter=10, eta=0.00001).fit(samples, y)
#ax[1].plot(range(1, len(ada2.cost_) + 1),
#           ada2.cost_, marker='o')
#ax[1].set_xlabel('Epochs')
#ax[1].set_ylabel('Sum-squared-error')
#ax[1].set_title('AdalineBGD learning rate 0.00001')
#plt.show();



### 训练
#ada = AdalineBGD(n_iter=15, eta=0.01)
#ada.fit(samplesStd, y)
#plot_decision_regions(samplesStd, y, classifier=ada)
#plt.title('AdalineBGD - Gradient Descent')
#plt.xlabel('sepal length [standardized]')
#plt.xlabel('petal length [standardized]')
#plt.legend(loc='upper left')
#plt.show();

#plt.plot(range(1, len(ada.cost_) + 1), ada.cost_, marker='o')
#plt.xlabel('Epochs')
#plt.ylabel('Sum-squared-error')
#plt.show();


## 使用随机梯度下降
ada = AdalineSGD(n_iter=15, eta=0.01, random_state=1)
ada.fit(samplesStd, y)
plot_decision_regions(samplesStd, y, classifier=ada)
plt.title('Adaline - Stochasticc Gradient Descent')
plt.xlabel('sepal length [std]')
plt.ylabel('petal length [std]')
plt.legend(loc='upper left')
plt.show();
plt.plot(range(1, len(ada.cost_) + 1), ada.cost_, marker='o')
plt.xlabel('Epochs')
plt.ylabel('Average Cost')
plt.show();

plt.plot(list(range(0,len(ada.errors_))),np.copy(ada.errors_),  'r.' )
plt.ylabel('errors value distrbution')
#lt.axis([0, 1500, -10, 10])
plt.show()
print(len(ada.errors_))


