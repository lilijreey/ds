人工智能-一种现代方法
-------------------------------------

人工智能流派
------------------------
符号主义 经典AI
连接主义 神经网络


知识的表示方法:
1. 产生式系统
   有一组 IF..Then 的规则组成, 使用条件匹配

2. 语义网络
    有结点和节点间的连接组成。
    结点表示概念或对象，连接表示关系

表示应该:直白，方便计算，对人类友好


四种基本智能程序
-----------------------------------
*  简单反射智能体
    基于当前感知选择行动，忽略其余的感知历史
    随机的反射行动比固定的反射行动要好些

*  基于模型的反射智能体
   维护依赖于感知历史的内部状态
   当前的动作取决于*内部状态* 和 *当前感知*

*  基于目的的智能体
    搜索和规划是达到目标的常用手段。
    比基于反射的智能体更加智能，因为他要思考自己的动作对自己目标
    的影响

*  基于效用的智能体
    最佳行动决策


问题求解
---------------------------------------------
* 搜索法 - 基于目标的问题求解， 
  问题形式化-> 搜索 -> 执行
* 搜索策略
  区别，是否有附近的信息，扩展节点的次序不同，也就是扩展节点的策略不同

* 无信息搜索（盲目搜索） (BreadthFirstSearch)
    1. 广度优先搜索
        空间和时间都是指数级的增长，对于大的目标深度，和大的分支扩增
        实际上不可能完成计算
        当所有单步耗散相同时，广度优先是最优的.
        + 如果有解则广度有优先一定能找到最优解，因为他是逐层遍历.
    2. 代价一致搜索
         当每个步伐的耗散不相同时，我们通过先扩散最小耗散节点来扩展节点
         就是代价一致搜索，当所有的单步耗散相同时，代价一致算法广度优先
         算法相同
    3. 深度优先搜索 (DFS)
        空间需求为最多深度D * 分支数M +1, 比BFS 大大减少，当只扩展当前
        节点的一个后继节点而不是所有的后继节点时，空间之需要D.
        + 不一定能找到解（对于无限树),并且即使找到解也不一定是最优的
    4. 深度有限搜索
        为了避免纯DFS 一路走到黑的缺点。 使用一个最多深度限制L来限制DFS
        的最多搜索深度，当深度大于L时，就认为没有后继节点了。
        通常从某些实际情况会的出最大深度
    5. 迭代深度搜索 IDS
        在一些无法得知最大有效深度的情况下，可以先设定最多深度为1，2，3
        .. 依次使用这些最大深度来使用DFS搜索。
        注意IDS在不同L时多次有进入相同的状态。但是先前的状态都比现在的
        要少，  他同时兼顾效率和有效性。 **是无信息搜索的首选方法**
    6. 双向搜索
         进行两个同时的搜索，一个从初始状态向前搜索，一个从目标状态
         向后搜索，直到两者相遇。 这个思想是 b^(d/2) + b^(d/2) 要比
         b^d 小的多

*  有信息搜索（启发搜索）limit the scarch scope
  1. 他要扩展的节点是基于(评价函数)*f(n)*进行的, 一般选择评价值低的节点扩展
     因为评价值度量了到目标的距离.
  2. 可以在搜索框架中添加基于f(n)的优先队列来实现
  3. 启发函数 heuristic function 
       h(n) n为节点 -> 从n到目标节点的最低耗散估值.
       如果n是目标节点则 h(n)=0
  4. 启发式搜索得到的解不一定是最优解，而是基于f(n) 的最优解，
      当f(n)和实际情况相同是，才可能通过启发搜索得到实际最优解
  * 两种启发搜索类型
    1. 贪婪最佳优先搜索：
         试图扩展离目标节点最近的节点, 只用启发函数来作为评价函数 
          f(n) = h(n)
         缺点： 不一定是实际最优解， 不一定能够得到解，因为可能解
            不再f(n) 中，注意在重复状态间死循环
    2. A-start
        把从起点到当前节点的耗散g(n) 和从该节点到目标节点的耗散h(n)
        相加作为评价函数 f(n) = g(n) + h(n)
       只用h(n) 满足一定条件??， 则A-start 即使完备的也是最优的

* 搜索的完备性
    一个搜索策略是完备的 <-> 只要有解存在就能保证找到解

* 搜索问题的一般性思考方面
   + 状态， 初始状态，目标状态
   + 操作 操作的表示，操作改变状态
   + 代价

   搜索问题可以看成是，找出从初始状态，到目标状态,的转换操作


## 使用启发式搜索的一些经典问题
* 寻路，八数码， 

## 找到最优函数，从而得到最优搜索
   由A-start 算法可知 h() 如果能找到最优的h函数，就能得到最优的解


# 模拟退火算法 SA (Simulated Annealing)

用来处理多种最优解问题,是一种随机模拟求解方法

它是基于Monte-Carlo迭代求解策略的一种随机寻优算法，
其出发点是基于物理中固体物质的退火过程与一般组合优化问题之间的相似性
模拟退火算法是通过赋予搜索过程一种时变且最终趋于零的概率突跳性，
从而可有效避免陷入局部极小并最终趋于全局最优的串行结构的优化算法


过程
------------------------------------------------------------------
假设求解空间是一个有f(x) 定义的函数， 最优解就是min(f(x)), 时x的值
 当然不能对这个f(x) 求导计算。 f(x)定义的函数有很多局部最小值

ΔE = f(newX) -f(oldX)

0. T 取极大值

1. 随机取x 如果在一个T上x已经改变了10次 则降低T
       New = OldT * a ; a为系数
2. 计算ΔE 
3.
    if ΔE <=0 then ## 说明有了一个更好的解
        接受当前x
        goto 1
    else # 新x 根据某种概率分布被计算得出(就是金属退火概率)
       1. 产生一个0~1之间的随机值 r
       2. if r < exp[-(ΔE/T)] then
             接受当前x #这个是精髓使得有一定概率是的新x 比就x 差，
                       #跳出当前局部最优解，达到全局最优解
             goto 1
           else
             x 不变
             goto 1


* exp[-(ΔE/T)]
    表示当温度为T时，球的能量变化的可能性, 一般T越大，变化的可能性就越大
    x就容易脱离一个波底，但是随着T的减少=0， x脱离波底的概率会越小最终
    最终不断逼近波底, T 必须按照某个规定的计划下降

   根据热力学的原理，在温度为T时，出现能量差为dE的降温的概率为P(dE)，
   表示为：
　　　　P(dE) = exp( dE/(kT) )
　　其中k是一个常数，exp表示自然指数，且dE<0。这条公式说白了就是：
    温度越高，出现一次能量差为dE的降温的概率就越大；
    温度越低，则出现降温的概率就越小。又由于dE总是小于0（否则就不叫退火了）
    因此dE/kT < 0 ，所以P(dE)的函数取值范围是(0,1) 

* T 的取值
  1. 一开始T取得足够高，是的几乎所有的位置变化都能被接受
  2. 下一次T = 当前T × a ; a通常在 [0.8-0.99]
  3. T下降使得平均在每个T上有10个可接受变化, 就是在一个T是x 有10次变化
  4. 如果连续3次T的改变x 都没有改变，则终止算法

Qus.
  当全局最优解的入口很小时，SA可以很好的得到全局最优解吗??

  

* 原理
  * 解空间
  * 目标函数
  * 初始解

* 数学理论
* 解决问题方法
* 适合面
* 归类
* 优点
* 缺点
* 相似算法


罗辑推到
-----------------------------
符号系统

* 命题
* 命题罗辑 
  命题逻辑有一种形式语言，允许我们形式的表示命题。
* FORC frist-order predicate calculus
* 谓词罗辑
* 操作     AND OR NOT if .. then(蕴含), if and only if(双蕴含)
  对应符号  ^   V  -|     ->          <->
* 真值表
* 等价： 两个表达式有相同的真值表
  等价公式， 德.摩根定律 (也实用与集合中)
  用于简化表达式

* 运算率： 交换律，分配率，吸收
  所有的操作都可以用 AND， 和NOT 来表示
  优先级括号第一, NOT, AND,  OR, ->,  <->

* 论证 
    一个过程，证明结论可以通过前提得出
    * 方法 
      1. 真值表
      2. 演绎推理 （当问题规模很大时真值方法表会变得很麻烦)
          推理规则

* FOPC 一阶谓词演算
   出现原因：
      使用_命题罗辑_ 有时候无法表达一些命题，或者有歧义，
      使用FOPC 可以比_命题罗辑_ 的表达能力更强。可用于表达
         命题之间的联系
      谓词用来表示，个体的性质或者个体直接的关系，
          就是返回true/false 的函数， 
      函数就是返回有某项性质的对象，
      量词： 对于所有的, y至少有一个

## 自动罗辑推理 EE 
演绎推理，不适与计算机推理，需要一些经验和规则。 真值表方法在空间很大时，
计算量会指数增加.
* 归结: 一种适用于机器的逻辑推理方法
  前提需要把罗辑表达式转换为一个中*标准形式* - 消除了蕴含和双蕴含以后的表达式
  文字 = 原子命题

* 原理： 如果所有的罗辑表示都能采用一种相同的形式，那么讲容易进行机器推倒
   所有的罗辑表达式都能通过等价转换变为CNF(合取范式)
   括号中的原子都使用 or, not 组成的
   e.g. (p v q) ^ (not s v p)
   罗辑命题。 同理也可以转变为DNF(析取范式)
   e.g. (p ^ q) v (not s ^ p)

* 过程
   1. 命题标准化(把输入表达式，转换为CNF)
     1. 消去 <->
           A <-> = (A -> B) ^ (B -> A)
     2. 消去 ->
        A->B = not A v B
     3. not 内移，移到括号中
        not(A v B) = not A ^ not B
     4. 消去双重not
        not not A = A
     5. 使用分配率, 转换为CNF
        A V (B ^ C) = (A v B) ^ (A v C)
   2. 
      设定一个表达式的*反演* 就是假设前提^ not结论　= FALSE
      如果证明就说明前台-> 结论
      
   3. 将灭个子句表示为一个集合,整个表达式为集合的集合
      在集合中寻找互补对　就是　q/ not q, 因为有互补对则CNF 定为FALSE

   4. 将两个互补对出现的子句(集合) C1 C2 归结
          归结 = C1- Q v C2 - not Q
          也就是分别在两集合中删除互补的元素
   5. 继续归结，直到不能在归结或者产生空子句位置, 空子句意味着子句不相容(false) 
       也就是证明出来了

      
## FOPC 的归结
      TODO
合一


##Prolog
  prolog 的推理是基于归结和合一的.
  又向无环图, 变为谓词



### 贝叶斯网络-不确定性 Bayes
用于处理不确定性推理
也叫做概率网络
传统的基于规则推倒的方法很难表示不确定性这种状态，So使用贝叶斯网络

基础为统计学中的
贝叶斯定理 果/因关系
--------------------------
用于反向推理-> 知道果 -> 求因 也叫逆概率
* P(x|y) 表示为在y发声的情况下ｘ发声的概率
* P(B|A) = P(A, B) / P(A)
* P(A, B) = P(A|B)/ P(B) 
* P(A∩B) = P(A).P(B|A)=P(B).P(A|B)
* P(B|A) =  (P(A|B).P(B)) / P(A) 
* 可以看到 P(B|A) 的概率可以有P(A|B) 计算出,反过来也一样
 这样的性质可以运行我们最双向的推到
* 使用贝叶斯定理可以计算出在一个情况已经发声的状态下，某个情况可能出现的概率
   一个是两个情况A,B, 已知 P(A|B) 要求 P(B|A) A,B没有必然因果关系
    需要求出P(A), P(B), P(A|B) or P(B|A)

    P(A) 称为先验概率,因为不考虑P(B)的因素,也就是说没有度量与B的因果关系


链式规则- 2个变量以上的情况,贝叶斯公式依然成立
-----------------------------
P(v1, v2 , ... vn) 是变量v,v2,..vn 的联合概率分别则
 P(v1, v2 , ... vn) = P(vn|vn-1, vn-2, ..v1) * P(vn-1| vn-2,...nv) ... P(v2|v1) * P(v1)
看到可以把联合概率分布转换为条件概率的组合
* 适用条件？？


* 联合概率


本质:
表示变量集合的联合概率分布的一种简洁方法

节点:
   网络中的节点一般用变量表示，每个变量的值由一些离散状态组成.
     e.g. 好，很好，一般
     每个状态都对应一个发声该状态的概率

连接节点的箭头表示因果关系


概率统计
* 随机变量--互斥事件
* 先验概率 一个自认为的概率
* 后验概率   P(x|y) 某事发声后，其他事件发声的概率

d 分离
------------------------------------------
