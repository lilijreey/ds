%%%-------------------------------------------------------------------
%%% @author zl
%%% @doc erlang 实现的behavire
%%%
%%% @end
%%%-------------------------------------------------------------------

-module(beh).

-export([main/0]).

%%AI文字表达的方式：
%%
%% 环境变量
%% is_has_money
%% is_love_day
%% is_has_flower
%% is_lover_at_home
%%
%%
%% action-node
%%   play_ball 打球
%%   buy_flower 买花
%%   get_money 取钱
%%   send_flower 送花
%%
%% control-node
%%   ONE任选一个
%%   EACH 
%%   
%%  条件
%%  true/1
%%  and/2
%%  or/2
%%  
%%
%%如果不是情人节，我们的Avatar将去球场打球，如果是情人节，他将带上一束花去见他的女友，但是他可能没带钱，所以他要先回家拿钱，然后去花店买花，再去
%%见他的女友，如果女友还在约会地点，则将花送给女友。



%% root 默认是select_one 
%%   (root() 
%%         ONE(true(is_love_day)) ->
%%                                   ONE(true(is_has_flower)) ->
%%                                                               ONE(true(is_lover_at_home)) -> act(send_flower)
%%                                                               ONE(true(ture)) -> act(play_ball)
%%                                   ONE(true(is_has_money)) ->
%%                                                               act(buy_flower)
%%                                   ONE(true(ture)) ->
%%                                                      act(get_money)
%%                                            
%%                                                    
%%                       
%%         true(true) -> act(play_ball)
%%         
%%
%%

-record(env, {
          id,
          is_has_money=false,
          is_love_day=false,
          is_has_flower=false,
          is_lover_at_home=false
         }).


%% ============= 环境的实现
env_new(Id) ->
    #env{id=Id}.

env_show(Env) ->
    io:format("obj env~p \n"
              "is_has_money:~p\n"
              "is_has_flower:~p\n"
              "is_love_day:~p\n"
              "is_lover_at_home:~p\n",
              [Env#env.id,
              Env#env.is_has_money,
              Env#env.is_has_flower,
              Env#env.is_love_day,
              Env#env.is_lover_at_home]).

env_set_true(Env, Index) ->
    erlang:setelement(Index, Env, true).

env_set_false(Env, Index) ->
    erlang:setelement(Index, Env, false).



%%================ 动作节点 action node
-record(act_n,
        {
         percondition=nil,
         act,
         arg
        }).

act_n_new(Act) -> act_n_new(nil, Act, nil).
act_n_new(Per, Act) -> act_n_new(Per, Act, nil).
act_n_new(Per, Act, Arg) ->
    #act_n{percondition=Per,
           act=Act,
           arg=Arg
          }.


%% -> Evn
act(#act_n{percondition=P}=Act, Env) ->
    case is_percondition_pass(P, Env) of
        true ->
            env_show(Env),
            do_act(Act, Env);
        false ->
            io:format("percondition not pass ~p\n", [Act]),
            Env
    end.

modifty_env(Env) ->
    env_set_true(Env, #env.is_love_day).


%% 对每一个action 进行实现
do_act(#act_n{act=play_ball}, Env) ->
    io:format("do action player_ball\n"),
    modifty_env(Env);

do_act(#act_n{act=buy_flower}, Env) ->
    io:format("do action buy_flower \n"),
    env_set_false(
      env_set_true(Env, #env.is_has_flower),
      #env.is_has_money);

do_act(#act_n{act=get_money}, Env) ->
    io:format("do action get_money\n"),
    env_set_true(Env, #env.is_has_money);

do_act(#act_n{act=send_flower}, Env) ->
    io:format("do action send_flower\n"),
    env_set_false(env_set_false(Env, #env.is_has_flower),
                  #env.is_lover_at_home);

do_act(Act, Env) ->
    io:format("unknown action ~p\n", [Act]),
    Env.




%% ======== percondition 实现
%% {true, Var}
%% {TestOp, Var, Value}
%% {fn, Fn/1(Evn)}
%% TestOp:: eq, le, ne, ge, lt, gt
is_percondition_pass(nil, _Env) ->
    true;
is_percondition_pass({Var, Test, Value}, Env) ->
    io:format("test ~p\n" , [{Var, Test, Value}]),
    case get_var_value(Var, Env) of
        nil -> false;
        V1 ->
            test(Test, V1, Value)
    end;
is_percondition_pass({true, Var}, Env) ->
    io:format("test true ~p\n" , [Var]),
    case get_var_value(Var, Env) of
        true -> true;
        _ -> false
    end.

%% 实现每个var 的gettor
get_var_value(is_has_money, Env)  ->
    Env#env.is_has_money;
get_var_value(is_has_flower, Env)  ->
    Env#env.is_has_flower;
get_var_value(is_love_day, Env)  ->
    Env#env.is_love_day;
get_var_value(is_lover_at_home, Env)  ->
    Env#env.is_lover_at_home;
get_var_value(_Var, _Evn) ->
    nil.


%% =========== test 语法实现
test(eq, V1, V2) -> V1 =:= V2;
test(ne, V1, V2) -> V1 =/= V2;
test(le, V1, V2) -> V1 =< V2;
test(lt, V1, V2) -> V1 < V2;
test(ge, V1, V2) -> V1 >= V2;
test(gt, V1, V2) -> V1 > V2.

-record(one_n,
        {percondition,
         nodes
        }).

-record(seq_n,
        {percondition,
         nodes
        }).


one_n_new(PerCond, Nodes) ->
    %% TODO check args
    #one_n{percondition=PerCond,
           nodes=Nodes}.

seq_n_new(PerCond, Nodes) ->
    %% TODO check args
    #seq_n{percondition=PerCond,
           nodes=Nodes}.

-record(bh_tree, {
          root
         }).


bh_tree(Nodes) ->
    #bh_tree{root=one_n_new(nil, Nodes)}.

bh_run(BT, Env) ->
    io:format("start bh_run\n"),
    case node_run(BT#bh_tree.root, Env) of
        stop ->
            io:format("bh_run stoped\n"),
            stop;
        NewEnv -> 
            bh_run(BT, NewEnv)
    end.


%% 执行一个one control node
node_run(#one_n{nodes=Nodes}, Env) ->
    timer:sleep(1000),
    one_n_run(Nodes, Env);
node_run(#seq_n{nodes=Nodes}, Env) ->
    timer:sleep(1000),
    seq_n_run(Nodes, Env);
node_run(#act_n{}=Node, Env) ->
    timer:sleep(1000),
    act(Node, Env).

one_n_run([], Env) ->
    Env;
one_n_run([N | Other], Env) ->
    case is_percondition_pass(get_cond(N), Env) of
        true ->
            node_run(N, Env);
        false ->
            one_n_run(Other, Env)
    end;
one_n_run(A, Env) ->
    io:format("bad one_n_run ~p\n", [A]).

seq_n_run([], Env) ->
    Env;
seq_n_run([N | Other], Env) ->
    NewEnv=
    case is_percondition_pass(get_cond(N), Env) of
        true ->
            node_run(N, Env);
        false ->
            Env
    end,
    seq_n_run(Other, NewEnv).


get_cond(#act_n{percondition=PerCond}) -> PerCond;
get_cond(#one_n{percondition=PerCond}) -> PerCond;
get_cond(#seq_n{percondition=PerCond}) -> PerCond.

main() ->
    erlang:spawn(fun() -> main__() end).


main__() ->
    %% build behavire tree
    BT =
    bh_tree([
             one_n_new({true, is_love_day},
                       [
                        one_n_new({true, is_has_flower},
                                  [
                                   one_n_new({true, is_lover_at_home},
                                             [act_n_new(send_flower)]),

                                   act_n_new(play_ball)
                                  ]),

                        one_n_new({true, is_has_money},
                                  [
                                   act_n_new(buy_flower)
                                  ]),

                        act_n_new(get_money)
                       ]),

             act_n_new(play_ball)
            ]),

    Env =
    env_set_true(env_new(1),
                 #env.is_love_day),

    bh_run(BT, Env),

    ok.

%% 写一个怪物的基本AI
%%  如果有敌人，战斗，如果距离不够，就移动到合适位置
%%  
%%  敌人打死了，就找出敌人
%%  
%%  没有敌人就闲逛
%%  
%%  血量少于10% 就逃跑
%%  
%%  如果敌人理我大于10格就回到出生点

xx() ->
    BT = 
    bh_tree([
             act_n_new({true, is_can_not_move}, step)
             seq_n_run({is_has_enemy},
                       [
                        %% 进入战斗
                        %% 或者已经在战斗中
                        act_n_new({not, is_get_release_skill}, get_release_skill),
                        one_n_new([
                                   act_n_new({true, is_enemy_faraway},back_to_origin),
                                   act_n_new({true, is_out_out_skill_range}, move_to_skill_range),
                                   act_n_new(release_skill)
                                  ]
                                 )
                       ]),

             act_n_new(stroll)
            ]).




