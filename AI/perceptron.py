# python3
# coding=utf-8

##使用神经网络 Rosenblatt 感知器,训练iris,数据分类器(网络)

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

## 预测分析,感知器实现
class Perceptron(object):
    def __init__(self, eta=0.01, n_iter=10):
        ## 初始化　eta 学习率 [0~1] 和　n_iter=训练次数

        self.eta = eta
        self.n_iter = n_iter

    #计算一个样本的当前输入值, sum(每个维度的值* 对应权重)
    def net_input(self, sample):
        ##print(sample, self.w_[1:])
        return np.dot(sample, self.w_[1:]) + self.w_[0]

    #执行激活函数, 和0进行比较，因为已经把阀值加入到w[0]中了
    def predict(self, sample):
        return np.where(self.net_input(sample) >= 0.0, 1, -1)

    def fit(self, samples, y):
        # samples 是输入所有样本，y为所有样本对应输入

        # self._w 用来存放每个维度的权，_w[0]存放的是阀值

        #生成一个X'row+1 的矩阵,w_[0]用来存放阀值,看山去阀值被初始化为0
        #所有样本共用一个权向量
        self.w_ = np.zeros(1 + samples.shape[1]) # 0矩阵,权重全部初始化为0
        self.errors_ = []

        for _ in range(self.n_iter): #迭代次数
            errors = 0
            for si, target in zip(samples,y): #对每个样本执行
                ##修正值 = 学习速率 * (目标输出 - 当前输出) * 当前权重对应的维度输入
                update = self.eta * (target - self.predict(si))
                #print("update=",update," target=", target, " out=", self.predict(si))
                if (update != 0.0):
                    self.w_[1:] += update * si; #更新当前权重
                    self.w_[0] += update  #?? 为何要更新权重
                    errors += 1
                    print("update wight=", self.w_[1:])

            self.errors_.append(errors)


        return self


def plot_decision_regions(samples, y, classifier, resolution=0.02):
    markers = ('s', 'x','o','^','v')
    colors=('red','blue','lightgreen','gray','cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])

    #plot the decision surface
    x1_min, x1_max = samples[:, 0].min() -1, samples[:,0].max() + 1
    x2_min, x2_max = samples[:, 1].min() -1, samples[:,1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
                           np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())

    #plot class samples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=samples[y==cl, 0], y=samples[y==cl,1],
                    alpha=0.8, c=cmap(idx), marker=markers[idx], label=cl)


## 读取数据
##'https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data',
df = pd.read_csv('./iris.data', header=None)

## 显示花的种类
y = df.iloc[0:100, 4].values # 得到第3列的所有值 -> array
y = np.where(y=='Iris-setosa', -1, 1)# 把对应的值都替换为-1,1
samples= df.iloc[0:100, [0,2]].values # 选择[0,1,2] 列的值
plt.scatter(samples[:50, 0],  samples[:50,1],
            color='red', marker='o', label='setosa')
plt.scatter(samples[50:100, 0],  samples[50:100,1],
            color='blue', marker='x', label='versicolor')
plt.xlabel('petal length')
plt.ylabel('sepal length')
plt.legend(loc='upper left')
plt.show()

## 开始训练
ppn = Perceptron(eta=0.1, n_iter=10)
ppn.fit(samples,y)
plt.plot(range(1, len(ppn.errors_) + 1),
         ppn.errors_, marker='o')
plt.xlabel('Epochs')
plt.ylabel('Number of misclassifications')
plt.show()



plot_decision_regions(samples, y, classifier=ppn)
plt.xlabel('sepal length [cm]')
plt.ylabel('petal length [cm]')
plt.legend(loc='upper left')
plt.show()


