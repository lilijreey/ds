////////REVERSE-LINK//////////////
//
//7函数逆置链表
//传递一个链表的头结点的指针


#ifndef REVERSE_LINKED_H_
#define REVERSE_LINKED_H_

#include "singly.h"
struct pLNode ;

void Reverse_Link_Interface_L(pLNode) ;
////基于循环的方法
void Reverse_Link_By_Loop_L(pLNode) ;
//基于递归的方法
pLNode Reverse_Link_By_Recur_L(pLNode, pLNode) ;

#endif

//
///////////////////////////////////