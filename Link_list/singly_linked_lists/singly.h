#ifndef SINGLY_H_
#define SINGLY_H_

enum status {FAIL, SUCCESS} ;

/////////////有头结点 head
////////////head.data 是该链表的长度
typedef struct LNode
{
	int data ;
	struct LNode * pNext ;
} LNode, *pLNode ;


void menu(void) ;

////////////////单链表的基本操作
//1 建立头结点已经在主函数中建立了
//链表的第一个节点为1；

//2 尾部输入数据
void Input_Back_L(pLNode) ;

//3显示链表
void Show_Link_L(pLNode) ;

//4自定义位置输入
int Input_Set_L(pLNode, int, int) ;

//函数4的交互界面
void Input_Set_Interface_L(pLNode) ;

//5自定义位置删除
void Delet_Set_Interface_L(pLNode) ;

//函数5的交互界面
int Delet_Set_L(pLNode, int) ;

////////REVERSE-LINK//////////////
//
//7函数逆置链表
//传递一个链表的头结点的指针
//extern void Reverse_Link_Interface_L(pLNode) ;
////基于循环的方法
//extern void Reverse_Link_By_Loop_L(pLNode) ;
//基于递归的方法
//extern pLNode Reverse_Link_By_Recur_L(pLNode, pLNode) ;
//
///////////////////////////////////

#endif