
//单链表的逆置
//方法1 基于循环方式 
//方法2 基于递归方式  #自动开辟空间 不用你管方便#



///求救！！！！ 分开就报错
#include <stdio.h>
#include "singly.h"
#include "reverse_linked.h"



//基于循环的方法
//void Reverse_Link_By_Loop_L(pLNode) ;

//基于递归的方法
//void Reverse_Link_By_Recur(pLNode) ;

int Lenght_Link_L(pLNode headNode) ;

void Reverse_Link_Interface_L(pLNode headNode)
{
		if(0 == Lenght_Link_L(headNode))
				printf("\n链表为空，无法逆置\n") ;
		else
		{
			//	Reverse_Link_By_Loop_L(headNode) ;
				Reverse_Link_By_Recur_L(headNode, headNode->pNext) ;
		}

		Show_Link_L(headNode) ;
} 


void Reverse_Link_By_Loop_L(pLNode headNode) 
{
		if(headNode->pNext->pNext == NULL)
				;
			//单接点是无需reverse	;
		else
		{
				//定义三个节点指针指向三个连续的节点
				pLNode a = headNode->pNext ;
				pLNode b = a->pNext ;
				pLNode c = b->pNext ;

				a->pNext = NULL ; //尾节点

				while(1)
				{
						b->pNext = a ;
						//三个指针都后移到下一结点
						if(c == NULL) 
								break ;
						a = b ;
						b = c  ;
						c = c->pNext ;
				}
				headNode->pNext = b ; //头结点指向原来的最会一个节点
				//OK
		}
}


////////////////////
pLNode  Reverse_Link_By_Recur_L(pLNode headNode, pLNode NowNode) 
{
		pLNode RNode = NULL ;

		if(NowNode->pNext != NULL)
				RNode = Reverse_Link_By_Recur_L(headNode, NowNode->pNext) ;

		if(NowNode->pNext == NULL)
				headNode->pNext = NowNode ;
		else
		{
				NowNode->pNext = RNode->pNext ; // 传递NULL
				RNode->pNext = NowNode ;
		}
		return NowNode ;
}
