#include <stdio.h>
#include <stdlib.h>

enum status {FAIL, SUCCESS} ;

typedef struct LNode
{
	int data ;
	struct LNode * pNext ;
} LNode, *pLNode ;


void menu(void) ;

////////////////单链表的基本操作
//1 建立头结点已经在主函数中建立了
//链表的第一个节点为1；

//2 尾部输入数据
void Input_Back_L(pLNode) ;

//3显示链表基于链表长度的循环
void Show_Link_L(pLNode) ;

//4自定义位置输入
int Input_Set_L(pLNode, int, int) ;

//函数4的交互界面
void Input_Set_Interface_L(pLNode) ;

//5自定义位置删除
void Delet_Set_Interface_L(pLNode) ;

//函数5的交互界面
int Delet_Set_L(pLNode, int) ;


int main()
{
	int select ;
	LNode headNode ; //建立头结点

	headNode.data = 0 ;//链表元素的个数
	headNode.pNext = &headNode ;

	while(1)
	{
		fflush(stdin) ;
		menu() ;
		scanf("%d", &select) ;

		switch(select)
		{
		case 0:
			return 0 ;
			
		case 1:
			Input_Back_L(&headNode) ;
			break ;

		case 2:
			Show_Link_L(&headNode) ;
			break ;

		case 3:
			Input_Set_Interface_L(&headNode) ;
			break ;

		case 4:
			Delet_Set_Interface_L(&headNode) ;
			break ;

		default:
			printf("\n无效输入，请从新输入\n") ;
			fflush(stdin) ;
			break ;
		}
	}
}

void menu(void)
{
	printf("\n*************************************************\n") ;
	printf(  "* 1 在尾部输入数据             2 显示线性表     *\n") ;
	printf(  "* 3 自定义位置输入             4 删除节点       *\n") ;
	printf(  "*                    0 退出                     *\n") ;
	printf(  "*************************************************\n") ;
	printf("请选择：") ;
}

void Input_Back_L(pLNode headNode)
{
	int data ;
	int i ;
	pLNode pPre = headNode ;
	pLNode nowNode ;

	for(i = 0; i < headNode->data; i++)
	{
		pPre = pPre->pNext ;
	}


	printf("请输入数据,以CTRL+E结束。 \n") ;

	while(scanf("%d", &data))
	{
		if( ( nowNode = (pLNode)malloc(sizeof(LNode))) == (pLNode)NULL)
		{
			printf("\n内存分配失败！") ;
			exit (1) ;
		}
		nowNode->data = data ;
		pPre->pNext = nowNode ;
		pPre = nowNode ;
		headNode->data++ ;
	}
	nowNode->pNext = headNode ;//最后一个的指针域为头指针
	fflush(stdin) ;
}

void Show_Link_L(pLNode headNode) 
{
	int i = headNode->data ;
	pLNode nowNode = headNode->pNext ; //初始为第一个节点
	
	if(headNode->data == 0)
	{
		printf("\n链表无数据\n") ;
	}
	else
	{
		printf("\n链表的大小为：%d\n", headNode->data) ;
		while(i > 0)
		{
			printf("%d ", nowNode->data) ;
			nowNode = nowNode->pNext ;
			i-- ;
		}
	}	
}


void Input_Set_Interface_L(pLNode headNode)
{
	int set, data ;

		printf("\n请输入插入位置：") ;
		scanf("%d", &set) ;
		while(set <= 0 || set > headNode->data+1)
		{
			printf("无效输入位置，请重新输入\n") ;
			scanf("%d" ,&set) ;
		}
		printf("请输入数值：\n") ;
		if(!scanf("%d", &data))
		{
			printf("\n输入流错误！\n") ;
			exit(1) ;
		}
		Input_Set_L(headNode, set, data) ;
}


int Input_Set_L(pLNode headNode, int set, int data)
{
	int i ;
	pLNode nowNode = headNode ;
	pLNode newNode ;

	if(set <= 0 || set >headNode->data+1)
	{
		printf("\n无效插入位置\n") ;
		return FAIL ;
	}
	else
	{
		for(i = 0; i < set-1; i++)
		{
			nowNode = nowNode->pNext ;
		}
		
		if( ( newNode = (pLNode)malloc(sizeof(LNode))) == (pLNode)NULL)
		{
			printf("\n内存分配失败！") ;
			exit (1) ;
		}
	
		newNode->pNext = nowNode->pNext ;
		newNode->data = data ;
		nowNode->pNext = newNode ;
		headNode->data++ ;
		return SUCCESS ;
	}
}


void Delet_Set_Interface_L(pLNode headNode) 
{
	int set ;

	if(headNode->data == 0)
	{
		printf("\n链表为空，无法删除\n") ;
	}
	else
	{
		printf("\n请输入删除位置：") ;
		scanf("%d", &set) ;
		while(set <= 0 || set > headNode->data)
		{
			printf("\n无效位置，请从新输入\n") ;
			if(!scanf("%d", set))
				;
		}
		Delet_Set_L(headNode, set) ;
		headNode->data-- ;
	}
}


int Delet_Set_L(pLNode headNode, int set) 
{
	int i ;
	pLNode delPreNode = headNode ;
	pLNode delNode ;
	
	if(set <= 0 || set > headNode->data)
	{
		printf("\n无效删除位置\n") ;
		return FAIL ;
	}
	else
	{
		for(i = 0; i < set-1; i++) //这个循环循环了(0~set-1) set次 (<) set-1 次
		{
			delPreNode = delPreNode->pNext ;
		}
		delNode = delPreNode->pNext ;
		delPreNode->pNext = delNode->pNext ;
		free(delNode) ;
		return SUCCESS ;
	}
}