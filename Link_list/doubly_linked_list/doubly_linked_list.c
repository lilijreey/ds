#include <stdio.h>
#include <stdlib.h>

enum status {FAIL, SUCCESS} ;

typedef struct linkPiece
{
	int data ;
	struct linkPiece *piror ;
	struct linkPiece *next ;
} DuLNode, *pDuLNode ;


void menu(void) ;
///////////////////双链表基本操作
//1 链表的第一个节点为1；

//2 尾部输入数据
void Input_Back_L(pDuLNode) ;

//3显示链表
void Show_Link_L(pDuLNode) ;

//4自定义位置输入
int Input_Set_L(pDuLNode, int, int) ;

//函数4的交互界面
void Input_Set_Interface_L(pDuLNode) ;

//5自定义位置删除
void Delet_Set_Interface_L(pDuLNode) ;

//函数5的交互界面
int Delet_Set_L(pDuLNode, int) ;


int main()
{
	int select ;
	DuLNode headNode ;
	headNode.next = (pDuLNode)NULL ;
	headNode.data = 0 ;

	while(1)
	{
		menu() ;
		fflush(stdin) ;
		scanf("%d", &select) ;

		switch(select)
		{
		case 0:
			return 0 ;

		case 1:
			Input_Back_L(&headNode) ;
			break ;

		case 2:
			Show_Link_L(&headNode) ;
			break ;

		case 3:
			Input_Set_Interface_L(&headNode) ;
			break ;
			
		case 4:
			Delet_Set_Interface_L(&headNode) ;
			break ;

		default :
			printf("无效选择，请重新输入") ;
			break ;
		}
		fflush(stdin) ;
	}
}



void menu(void)
{
	printf("\n*************************************************\n") ;
	printf(  "* 1 在尾部输入数据             2 显示线性表     *\n") ;
	printf(  "* 3 自定义位置输入             4 删除节点       *\n") ;
	printf(  "*                    0 退出                     *\n") ;
	printf(  "*************************************************\n") ;
	printf("请选择：") ;
}

void Input_Back_L(pDuLNode headNode) 
{
	int i ;
	int data ;
	pDuLNode newNode ;
	pDuLNode nowNode = headNode ;


	//使nowNode移动到最后一个节点
	for(i = 0; i < headNode->data; i++)
	{
		nowNode = nowNode->next ;
	}

	printf("请输入数据，以CTRL+E结束\n") ;
	while(scanf("%d", &data))
	{	
		if((newNode = (pDuLNode)malloc(sizeof(DuLNode))) == (pDuLNode)NULL)
		{
			printf("\n内存分配错误\n") ;
			exit(1) ;
		}
		newNode->data = data ;
		newNode->piror = nowNode ;
		nowNode = nowNode->next = newNode ;
		headNode->data++ ;
	}
	newNode->next = (pDuLNode)NULL ;
}

void Show_Link_L(pDuLNode headNode)
{
	pDuLNode nowNode = headNode->next ;

	if(headNode->data == 0)
	{
		printf("\n链表为空\n") ;
	}
	else
	{
		printf("链表的长度为：%d\n", headNode->data) ;
		while(nowNode != (pDuLNode)NULL)
		{
			printf("%d ", nowNode->data) ;
			nowNode = nowNode->next ;
		}
	}
}


void Input_Set_Interface_L(pDuLNode headNode)
{
	int set ;
	int data ;
	
	if(0 == headNode->data)
	{
		printf("\n链表为空,无法删除\n") ;
	}
	else
	{
		printf("\n请输入插入位置起始位为1: ") ;
		scanf("%d" ,&set) ;
		if(set > headNode->data+1 || set < 1)
		{
			printf("无效插入位置,请从新输入：") ;
			scanf("%d", &set) ;
		}
		printf("请输入数据：") ;
		if(!scanf("%d", &data))
		{
			fflush(stdin) ;
			printf("\n错误输入类型\n") ;
			exit(1) ;
		}
		Input_Set_L(headNode, set, data) ;
	}
}


int Input_Set_L(pDuLNode headNode, int set, int data) 
{
	int i ;
	pDuLNode nowNode = headNode ;
	pDuLNode newNode ;

	if(set > headNode->data+1 || set < 1)
	{
		printf("\n无效插入位置\n") ;
		return FAIL ;
	}
	else
	{
		for(i = 0; i < set-1; i++)
		{
			nowNode = nowNode->next ;
		}

		if((newNode = (pDuLNode)malloc(sizeof(DuLNode))) == (pDuLNode)NULL)
		{
			printf("\n内存分配错误\n") ;
			exit(1) ;
		}
		newNode->data = data ;
		if(nowNode->next == (pDuLNode)NULL)
		{
			newNode->piror = nowNode ;
			nowNode->next = newNode ;
			newNode->next = (pDuLNode)NULL ;
		}
		else
		{
			newNode->next = nowNode->next ;
			nowNode->next = newNode ;
			newNode->piror = nowNode ;
			newNode->next->piror = newNode ;			
		}
		headNode->data++ ;
		return SUCCESS ;
	}
}


void Delet_Set_Interface_L(pDuLNode headNode)
{
	int set ;

	printf("\n请输入删除位置起始位为1: ") ;
	scanf("%d" ,&set) ;
	if(set > headNode->data+1 || set < 1)
	{
		printf("无效删除位置,请从新输入：") ;
		scanf("%d", &set) ;
	}

	Delet_Set_L(headNode, set) ;

}


int Delet_Set_L(pDuLNode headNode, int set) 
{
	int i ;
	pDuLNode nowNode = headNode ;
	pDuLNode delNode ;
	
	if(set > headNode->data || set < 1)
	{
		printf("\n无效删除位置\n") ;
		return FAIL ;
	}
	
	for(i = 0; i < set-1; i++)
	{
		nowNode = nowNode->next ;
	}
	delNode = nowNode->next ;
	if(delNode->next == (pDuLNode)NULL)
	{
		nowNode->next =(pDuLNode)NULL ;
	//	printf("\nDELET\n") ;
	}
	else
	{
		nowNode->next = delNode->next ;
		delNode->piror = nowNode ;
	}
	free(delNode) ;
	headNode->data-- ;
	return SUCCESS ;
}

/*

int main()
{
	int num ;
    piece *head ;

	piece *temp, *now ;//temp->next = now

	//crate head piot
	if( (head = (piece *)malloc(sizeof(piece))) == (piece *)NULL)
	{
		printf("\n动态内存错误") ;
		exit(1) ;
	}
	temp = head ;


	printf("输入节点数据, 以CTRL+E结束") ;

	while(scanf("%d", &num))
	{	
		if( (now = (piece *)malloc(sizeof(piece))) == (piece *)NULL)
		{
			printf("\n动态内存错误") ;
			exit(1) ;
		}
		now->data = num ;
		temp->next = now ;
		now->pare = temp ;
		temp = now ;
	}
	now->next = (piece *)NULL ;
	head->next->pare = (piece *)NULL ;	

	//read
	now = head->next ;
	while(1)
	{
		printf("%d, ", now->data) ;
		if(now->next == (piece *)NULL)
			break ;
			now = now->next ;
	}

	return 0 ;
}
*/